<?php

/**
 * 应聘工作
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package page
 */
require('page.php');

//引入并声明candadites类
require(DIR_LIB . DS . 'sys-candadites.php');
$sysCandadites = new SysCandadites($coreDB, $sysLog, CANDADITES_TIME_LIMIT);

//限制提交时间输出
$limitTime = CANDADITES_TIME_LIMIT;
$limitStr = '';
if ($limitTime <= 60) {
    $limitStr = $limitTime . '秒';
} elseif ($limitTime > 60 && $limitTime <= 3600) {
    $limitStr = ceil($limitTime / 60) . '分钟';
} elseif ($limitTime > 3600 && $limitTime <= 86400) {
    $limitStr = ceil($limitTime / 60 / 60) . '小时';
} elseif ($limitTime > 86400) {
    $limitStr = ceil($limitTime / 60 / 60 / 24) . '天';
}

//是否可以提交简历
$candaditesAddOn = 0;
if ($sysCandadites->checkAdd($coreIP->ip) == true) {
    $candaditesAddOn = 1;
}

$postAdd = 0;
if (isset($_GET['m']) == true) {
    if ($_GET['m'] == 'add-success') {
        $postAdd = 1;
    } else {
        $postAdd = 2;
    }
}

//注册变量
$smarty->assign('limitTimeStr', $limitStr);
$smarty->assign('postAdd', $postAdd);
$smarty->assign('candaditesAddOn', $candaditesAddOn);

//输出页面
$smarty->display('candadites.tpl');
?>
