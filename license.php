<?php

/**
 * 授权协议
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package page
 */
require('page.php');

//获取授权协议
$licenseSrc = DIR_LIB . DS . 'static' . DS . 'license.html';
$license = '';
if(CoreFile::isFile($licenseSrc) == true){
    $license = CoreFile::loadFile($licenseSrc);
}

//注册变量
$smarty->assign('license', $license);

//输出页面
$smarty->display('license.tpl');
?>
