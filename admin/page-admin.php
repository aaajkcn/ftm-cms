<?php

/**
 * 登录后页面引用
 * @author fotomxq <fotomxq.me>
 * @version 3
 * @package admin
 */
require_once('page-post.php');
require(DIR_LIB . DS . 'sys-user.php');
$sysUser = new SysUser($coreDB, $coreSession, $sysLog, $coreIP->ip);
$userRes = $sysUser->logged();
if ($userRes) {
    if ($sysUser->checkPower('VISITORS') == false) {
        $coreFeedback->output('url', 'error.php?e=power');
    }
} else {
    $coreFeedback->output('url', 'index.php?active=1&msg=3');
}
//如果提交动作，则自动清理缓冲
if (isset($_GET['action']) == true) {
    $coreCache->clear();
}
?>
