<?php

/**
 * 网站主题设定
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require('page-admin-page.php');

//判断权限
if($sysUser->checkPower('BASIC') == false){
    CoreHeader::toURL($errorPagePower);
}

//主题类
require(DIR_LIB . DS . 'sys-theme.php');
$sysTheme = new SysTheme($coreDB, $sysLog, $sysConfig);
$themeList = $sysTheme->getList();

//CSS
$includePage['css'][] = 'admin-theme';
?>
<?php require('page-admin-top.php'); ?>

<div class="row">
    <div class="span2">&nbsp;</div>
    <div class="span10">
        <h2>主题设置</h2>
    </div>
</div>

<div class="row">
    <div class="span2"></div>
    <div class="span10">
        <p>&nbsp;</p>
        <form class="form-horizontal" action="action-operate-basic.php" method="post">
            <p>当前选择的是：默认主题</p>
            <ul class="thumbnails" id="operate-theme-list">
                <?php if($themeList){ foreach($themeList as $v){ $imageSrc = WEB_URL.'/includes/theme/'.$v['te_name'].'/preview.png'; ?>
                <li class="span4">
                    <a href="#theme" class="thumbnail" data-toggle="tooltip" title="<?php echo $v['te_title']; ?> - <?php echo $v['te_description']; ?>">
                        <img data-src="<?php echo $imageSrc; ?>" src="<?php echo $imageSrc; ?>">
                    </a>
                </li>
                <?php } } ?>
            </ul>
        </form>
    </div>
</div>

<?php require('page-admin-footer.php'); ?>
