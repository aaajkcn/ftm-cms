$(document).ready(function() {
    $('#viewsLine').highcharts({
        chart: {
            type: chartType
        },
        title: {
            text: chartTitle,
            x: -20 //center
        },
        subtitle: {
            text: '来自: ' + chartURL,
            x: -20
        },
        xAxis: {
            categories: chartCategories
        },
        yAxis: {
            title: {
                text: '访问量 (IP)'
            },
            plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
        },
        tooltip: {
            valueSuffix: ''
        },
        plotOptions: {
            area: {
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
                name: '访问量',
                data: chartData
            }]
    });
});