$(function() {
    if ($(document).width() > 750) {
        $('.table-list tr').each(function(k, v) {
            $(this).children('td:last').children('a').hide();
        });
        $('.table-list tr td:last').css('width', '390px');
        $('.table-list tr').hover(function() {
            $(this).children('td:last').children('a').show();
        }, function() {
            $(this).children('td:last').children('a').hide();
        });
    }
});