$(document).ready(function() {
    if ($(document).width() > 750) {
        $('tr[name="table-data-tr"]').each(function(k, v) {
            $(this).children('td:last').children('a').hide();
        });
        $('tr[name="table-data-tr"] td:last').css('width','360px');
        $('tr[name="table-data-tr"]').hover(function() {
            $(this).children('td:last').children('a').show();
        }, function() {
            $(this).children('td:last').children('a').hide();
        });
    }
});
