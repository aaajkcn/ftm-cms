//刷新产品按钮组事件
function refreshProductClick() {
    $('a[href="#delete-product"]').die('click');
    $('a[href="#delete-product"]').click(function() {
        $(this).parent().parent().remove();
    });
}
//ready
$(function() {
    //初始化编辑框
    /*
     $('.textarea').wysihtml5({
     locale: 'zh-CN',
     'html': true,
     'color': true
     });
     */
    $('#inputContent').wysiwyg();
    //添加产品按钮
    $('body').data('product-html', $('#product-list-template').html());
    $('body').data('product-num', $('#product-list .product-list').length);
    if ($('body').data('product-num') > 0) {
        $('#product-list').show();
    }
    $('a[href="#show-product"]').click(function() {
        if ($('body').data('product-num') == 0) {
            $('#product-list').show();
        }
        $('body').data('product-num', $('body').data('product-num') + 1);
        $('#product-list').append($('body').data('product-html'));
        refreshProductClick();
    });
    if ($(document).width() > 750) {
        $('.table-list tbody tr').each(function(k, v) {
            $(this).children('td:last').children('a').hide();
        });
        $('.table-list tbody tr td:last').css('width', '390px');
        $('.table-list tbody tr').hover(function() {
            $(this).children('td:last').children('a').show();
        }, function() {
            $(this).children('td:last').children('a').hide();
        });
    }
    //form提交前处理
    $('#form-content').submit(function(){
        var postTitle = $('#inputTitle').val();
        if(!postTitle){
            return false;
        }
        $('#inputContent').html($('#editor').html());
        var postContent = $('#inputContent').html();
        if(!postContent){
            return false;
        }
        return true;
    });
    //wysiwyg
    function initToolbarBootstrapBindings() {
        var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier', 
            'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
            'Times New Roman', 'Verdana','宋体','仿宋'],
                fontTarget = $('#content-font').siblings('.dropdown-menu');
        $.each(fonts, function(idx, fontName) {
            fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
        });
        $('a[title]').tooltip({container: 'body'});
        $('.dropdown-menu input').click(function() {
            return false;
        })
                .change(function() {
            $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
        })
                .keydown('esc', function() {
            this.value = '';
            $(this).change();
        });

        $('[data-role=magic-overlay]').each(function() {
            var overlay = $(this), target = $(overlay.data('target'));
            overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
        });
        if ("onwebkitspeechchange"  in document.createElement("input")) {
            //var editorOffset = $('#editor').offset();
            //$('#voiceBtn').css('position', 'absolute').offset({top: editorOffset.top, left: editorOffset.left + $('#editor').innerWidth() - 35});
        } else {
            //$('#voiceBtn').hide();
        }
    }
    function showErrorAlert(reason, detail) {
        var msg = '';
        if (reason === 'unsupported-file-type') {
            msg = "Unsupported format " + detail;
        }
        else {
            console.log("error uploading file", reason, detail);
        }
        $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
    }
    ;
    initToolbarBootstrapBindings();
    $('#editor').wysiwyg({fileUploadError: showErrorAlert});
    //window.prettyPrint && prettyPrint();
});