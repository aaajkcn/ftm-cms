<?php
/**
 * 站点统计页面
 * <p>直接跳转到统计登陆页面。</p>
 * @author fotomxq <fotomxq.me>
 * @version 2
 * @package admin
 */
require('page-admin-page.php');
CoreHeader::toURL('http://ta.qq.com/');
die();

//判断权限
if ($sysUser->checkPower('STATISTICS') == false) {
    CoreHeader::toURL($errorPagePower);
}

//访客记录
require(DIR_LIB . DS . 'sys-views.php');
$sysViews = new SysViews($coreDB, $sysLog);

//引用字符串截取插件
require(DIR_LIB . DS . 'plug-substrutf8.php');

//分析动作
$action = isset($_GET['action']) == true ? $_GET['action'] : 'year';
$viewsURL = isset($_GET['url']) == true ? $_GET['url'] : '';
$viewsIP = isset($_GET['ip']) == true ? filter_var($_GET['ip'], FILTER_VALIDATE_IP) : '';

//获取列表
$page = isset($_GET['page']) == true ? (int) $_GET['page'] : 1;
$max = 30;
$sort = isset($_GET['sort']) == true ? (int) $_GET['sort'] : 0;
$desc = isset($_GET['desc']);
$where = '1';
$attrs = null;
if ($viewsURL) {
    $where .= ' and `v_url` = :url';
    $attrs[] = array(':url' => array($viewsURL, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT));
}
if ($viewsIP) {
    $where .= ' and `v_ip` = :ip';
    $attrs[] = array(':ip' => array($viewsIP, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT));
}
$viewsList = $sysViews->getList($where, $attrs, $page, $max, $sort, $desc);
$viewListRow = $sysViews->getListRow($where, $attrs);

/**
 * 时间递减函数
 * <p>最多仅输出 年-月-日 部分</p>
 * @param int $time 减去时间 ( 秒 )
 * @param string $frame 输出格式
 * @return string
 */
function getDateSince($time, $frame) {
    $nowTime = time();
    $sinceTime = $nowTime - $time;
    $timeArr = getdate($sinceTime);
    $str = str_replace('Y', $timeArr['year'], $frame);
    if (strlen($timeArr['mon']) < 2) {
        $timeArr['mon'] = '0' . $timeArr['mon'];
    }
    $str = str_replace('m', $timeArr['mon'], $str);
    if (strlen($timeArr['mday']) < 2) {
        $timeArr['mday'] = '0' . $timeArr['mday'];
    }
    $str = str_replace('d', $timeArr['mday'], $str);
    return $str;
}

//获取图表数据
$nocache = isset($_GET['nocache']);
$chartData = false;
if ($nocache == false) {
    $chartData = $coreCache->get('STATISTICS-CHART', true);
}
$chartType = 'area';
$chartTitle = '';
$chartURL = WEB_URL;
$chartCategories = '';
$dateY = date('Y');
$dateM = date('m');
$viewsWhere = '';
$viewsAttrs = null;
$viewsPageTopList = null;
if ($viewsIP) {
    $viewsWhere = '`v_ip` LIKE :ip and ';
    $viewsAttrs = array(':ip' => array($viewsIP, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT));
}
if ($viewsURL) {
    $viewsWhere .= '`v_url` LIKE :url and ';
    $viewsAttrs[':url'] = array($viewsURL, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT);
}
if ($chartData === false) {
    $chartData = '[]';
    $data = null;
    //最近一年数据
    $data['year'] = null;
    for ($t = 31536000; $t >= 0; $t-=2592000) {
        $where = $viewsWhere . '`v_date` LIKE \'' . getDateSince($t, 'Y-m') . '%\'';
        $res = $sysViews->getListRow($where, $viewsAttrs);
        if ($res > 0) {
            $data['year'][] = $res;
        }
    }
    //最近30天数据
    $data['month'] = null;
    for ($t = 2592000; $t >= 0; $t-=86400) {
        $where = $viewsWhere . '`v_date` LIKE \'' . getDateSince($t, 'Y-m-d') . '%\'';
        $res = $sysViews->getListRow($where, $viewsAttrs);
        if ($res) {
            $data['month'][] = $res;
        }
    }
    //最近7天数据
    $data['week'] = null;
    for ($t = 604800; $t >= 0; $t-=86400) {
        $where = $viewsWhere . '`v_date` LIKE \'' . getDateSince($t, 'Y-m-d') . '%\'';
        $res = $sysViews->getListRow($where, $viewsAttrs);
        if ($res) {
            $data['week'][] = $res;
        }
    }
    //页面访问排名
    $data['top'] = null;
    $where = $viewsWhere . ' 1';
    $res = $sysViews->getPageTop($where, $viewsAttrs, 10);
    if ($res) {
        foreach ($res as $v) {
            $data['top'][] = $v['num'];
            $chartCategories[] = $v['v_url'];
        }
    }
    //保存缓冲
    if ($data) {
        $coreCache->save('STATISTICS-CHART', $data);
    }
    //更替数据
    if (isset($data[$action]) == true && $data[$action]) {
        $chartData = '[' . implode(',', $data[$action]) . ']';
    }
} else {
    if ($chartData) {
        if (isset($chartData[$action]) == true && $chartData[$action]) {
            $chartData = '[' . implode(',', $chartData[$action]) . ']';
        } else {
            $chartData = '[]';
        }
    } else {
        $chartData = '[]';
    }
}
if ($action == 'year') {
    $chartType = 'column';
    $chartTitle = '最近一年访问量';
    $chartCategories = '["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"]';
} elseif ($action == 'month') {
    $chartType = 'area';
    $chartTitle = '最近30天访问量';
    $monthDayNum = cal_days_in_month(CAL_GREGORIAN, $dateM, $dateY);
    for ($i = 1; $i <= $monthDayNum; $i++) {
        $chartCategories .= ',"' . $i . '"';
    }
    $chartCategories = '[' . substr($chartCategories, 1) . ']';
} elseif ($action == 'week') {
    $chartType = 'area';
    $chartTitle = '最近7天访问量';
    $chartCategories = '["周一", "周二", "周三", "周四", "周五", "周六", "周日"]';
} elseif ($action == 'top') {
    $chartType = 'column';
    $chartTitle = '页面访问量排名 (前10个)';
    if ($chartCategories) {
        $chartCategories = '["' . implode('","', $chartCategories) . '"]';
    }
}
//再次重置图表类型
$chartTypes = array('line', 'area', 'column');
$chartTypeKey = isset($_GET['type']) == true ? $_GET['type'] : -1;
$chartType = isset($chartTypes[$chartTypeKey]) == true ? $chartTypes[$chartTypeKey] : $chartType;

//页码生成器
require(DIR_LIB . DS . 'page-pagination.php');

//提示信息
$msgArr = array('无法执行操作，请重试。', '数据清理成功，现在所有访问数据重新开始记录！', '清理了图表缓冲。');
$msgTypeArr = array(array('错误！', 'error'), array('成功！', 'success'), array('成功！', 'success'));

//包含页面
$includePage['css'][] = 'admin-statistics';
$includePage['js'][] = 'highcharts';
$includePage['js'][] = 'exporting';
$includePage['js'][] = 'admin-statistics';

require('page-admin-top.php');
?>
<div class="row">&nbsp;</div>
<div class="row">
    <div class="span12">
        <a href="admin-statistics.php?active=3" class="btn btn-info"><i class="icon-refresh icon-white"></i> 重置筛选</a>
        <div class="btn-group">
            <a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#">
                选择图表数据类型
                <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li><a href="admin-statistics.php?active=3&action=year&type=<?php echo $chartTypeKey; ?>">最近一年访问量</a></li>
                <li><a href="admin-statistics.php?active=3&action=month&type=<?php echo $chartTypeKey; ?>">最近30天访问量</a></li>
                <li><a href="admin-statistics.php?active=3&action=week&type=<?php echo $chartTypeKey; ?>">最近7天访问量</a></li>
                <li><a href="admin-statistics.php?active=3&action=top&nocache=1&type=<?php echo $chartTypeKey; ?>">页面访问排名</a></li>
            </ul>
        </div>
        <div class="btn-group">
            <a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#">
                选择图表显示类型
                <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li><a href="admin-statistics.php?active=3&action=<?php echo $action; ?><?php if($nocache) echo '&nocache=1'; ?>&type=0">线性</a></li>
                <li><a href="admin-statistics.php?active=3&action=<?php echo $action; ?><?php if($nocache) echo '&nocache=1'; ?>&type=1">图形</a></li>
                <li><a href="admin-statistics.php?active=3&action=<?php echo $action; ?><?php if($nocache) echo '&nocache=1'; ?>&type=2">条状</a></li>
            </ul>
        </div>
        <a href="action-statistics.php?action=refresh" class="btn btn-info"><i class="icon-refresh icon-white"></i> 刷新图表(清理缓冲)</a>
        <a href="action-statistics.php?action=clear" class="btn btn-warning"><i class="icon-trash icon-white"></i> 清空所有数据</a>
    </div>
</div>
<div class="row">&nbsp;</div>
<h2>访问数据</h2>
<hr>
<div id="viewsLine" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
<div class="row">&nbsp;</div>
<h2>访客记录列表</h2>
<hr>
<?php if($viewsList){ ?>
<table class="table table-bordered table-hover table-list">
    <thead>
        <tr>
            <th><a href="admin-statistics.php?active=3&page=<?php echo $page; ?>&sort=3<?php if($desc == false) echo '&desc=1'; ?>" target="_self">时间</a></th>
            <th><a href="admin-statistics.php?active=3&page=<?php echo $page; ?>&sort=1<?php if($desc == false) echo '&desc=1'; ?>" target="_self">IP</a></th>
            <th><a href="admin-statistics.php?active=3&page=<?php echo $page; ?>&sort=2<?php if($desc == false) echo '&desc=1'; ?>" target="_self">URL</a></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($viewsList as $v){ ?>
        <tr>
            <td><?php echo $v['v_date']; ?></td>
        <td><a href="admin-statistics.php?active=3&action=<?php echo $action; ?>&ip=<?php echo $v['v_ip']; ?><?php if($viewsURL) echo '&url='.$viewsURL; ?><?php if($nocache == true) echo '&nocache=1'; ?>&type=<?php echo $chartTypeKey; ?>" target="_self"><?php echo $v['v_ip']; ?></a></td>
            <td><a href="admin-statistics.php?active=3&action=<?php echo $action; ?>&url=<?php echo $v['v_url']; ?><?php if($viewsIP) echo '&ip='.$viewsIP; ?><?php if($nocache == true) echo '&nocache=1'; ?>&type=<?php echo $chartTypeKey; ?>" target="_self"><?php echo $v['v_url']; ?></a></td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<div class="text-center">
    <?php echo PagePaginationQuick('admin-statistics.php?active=3&page=', $page, $viewListRow,$max); ?>
</div>
<?php } ?>
<script>
    var chartType = '<?php echo $chartType; ?>';
    var chartURL = '<?php echo $chartURL; ?>';
    var chartTitle = '<?php echo $chartTitle; ?>';
    var chartCategories = <?php echo $chartCategories; ?>;
    var chartData = <?php echo $chartData; ?>;
</script>
<?php require('page-admin-footer.php'); ?>
