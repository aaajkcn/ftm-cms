<?php

/**
 * 应聘操作处理器
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require_once('page-admin.php');
$msg = 0;
if (isset($_GET['action']) == true && $sysUser->checkPower('CANDIDATES') == true) {
    //引用并声明candadites对象
    require(DIR_LIB . DS . 'sys-candadites.php');
    $sysCandadites = new SysCandadites($coreDB, $sysLog, CANDADITES_TIME_LIMIT);
    if ($_GET['action'] == 'check' && isset($_GET['id']) == true && isset($_GET['pass']) == true && $sysUser->checkPower('CANDIDATES-CHECK') == true) {
        $id = (int) $_GET['id'];
        $status = $_GET['pass'] ? $sysCandadites->status[1] : $sysCandadites->status[2];
        $res = $sysCandadites->changeStatus($id, $status);
        $msg = $res == true ? 1 : 2;
    } elseif ($_GET['action'] == 'delete' && isset($_GET['id']) == true) {
        $id = (int) $_GET['id'];
        $res = $sysCandadites->del($id);
        $msg = $res == true ? 3 : 4;
    }
}
$coreFeedback->output('url', 'admin-candadites.php?active=2&msg=' . $msg);
?>
