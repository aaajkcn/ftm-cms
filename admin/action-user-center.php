<?php

/**
 * 提交修改用户信息
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require_once('page-admin.php');
$msg = 0;
if (isset($_POST['name']) == true && isset($_POST['email']) == true && $sysUser->checkPower('USER-SELF') == true) {
    $passwdBool = false;
    $passwd = $userRes['user_password'];
    if (isset($_POST['password']) == true && isset($_POST['newPassword']) == true && isset($_POST['newPassword2']) == true && $_POST['password'] && $_POST['newPassword'] && $_POST['newPassword2']) {
        $passwordEncrypt = $sysUser->getEncrypt($_POST['password']);
        if ($passwordEncrypt == $userRes['user_password'] && $_POST['newPassword'] == $_POST['newPassword2']) {
            $passwdBool = true;
            $passwd = $_POST['newPassword'];
        }
    } else {
        $passwdBool = true;
    }
    if ($passwdBool == true) {
        $res = $sysUser->editUser($userRes['id'], $userRes['user_username'], $passwd, $_POST['name'], $_POST['email']);
        $msg = $res == true ? 1 : 2;
    } else {
        $msg = 3;
    }
}
$coreFeedback->output('url', 'admin-user-center.php?active=1&msg=' . $msg);
?>
