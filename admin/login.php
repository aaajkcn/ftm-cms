<?php

/**
 * 登陆提交页面
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require('page-post.php');
require(DIR_LIB . DS . 'sys-user.php');

//验证登录
$sysUser = new SysUser($coreDB, $coreSession, $sysLog, $coreIP->ip);
$remember = isset($_POST['remember']);
if (isset($_POST['user']) == true && isset($_POST['passwd']) == true && isset($_POST['vcode']) == true) {
    $vcode = $coreSession->get('vcode');
    if (strtolower($_POST['vcode']) === strtolower($vcode) || DEBUG_ON == true) {
        if ($sysUser->login($_POST['user'], $_POST['passwd'], $remember) == true) {
            $coreFeedback->output('url', 'admin-center.php');
        } else {
            $coreFeedback->output('url', 'index.php?msg=1');
        }
    } else {
        $coreFeedback->output('url', 'index.php?msg=0');
    }
} else {
    $coreFeedback->output('url', 'index.php?msg=2');
}
?>
