<?php
/**
 * 用户中心
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require('page-admin-page.php');

//判断权限
if($sysUser->checkPower('USER-SELF') == false){
    CoreHeader::toURL($errorPagePower);
}

//提示信息
$msgArr = array('无法修改信息，请重试。', '修改成功！', '修改失败，请检查相关信息是否正确。', '修改失败，请输入正确的密码。');
$msgTypeArr = array(array('错误！', 'error'), array('成功！', 'success'), array('错误！', 'info'), array('错误！', 'info'));
require('page-admin-top.php');
?>
<div class="row">
    <div class="span3">&nbsp;</div>
    <div class="span9">
        <h2>用户个人中心</h2>
    </div>
</div>
<div class="row">
    <div class="span2"></div>
    <div class="span10">
        <p>&nbsp;</p>
        <form class="form-horizontal" action="action-user-center.php" method="post">
            <div class="control-group">
                <label class="control-label">登录时间 : </label>
                <div class="controls">
                    <p><?php echo $userRes['user_lasttime']; ?></p>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">登录IP : </label>
                <div class="controls">
                    <p><?php echo $userRes['user_ip']; ?></p>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputUsername">用户名</label>
                <div class="controls">
                    <input type="text" name="username" id="inputUsername" placeholder="用户名 (不修改则留空)" value="<?php echo $userRes['user_username']; ?>" disabled="disabled">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputName">昵称</label>
                <div class="controls">
                    <input type="text" name="name" id="inputName" placeholder="昵称 (必须)" value="<?php echo $userRes['user_name']; ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputEmail">邮箱</label>
                <div class="controls">
                    <input type="text" name="email" id="inputEmail" placeholder="邮箱 (必须)" value="<?php echo $userRes['user_email']; ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputPassword">原密码</label>
                <div class="controls">
                    <input type="password" name="password" id="inputPassword" placeholder="原密码 (如果修改密码,则必填)">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputnNwPassword">新密码</label>
                <div class="controls">
                    <input type="password" name="newPassword" id="inputnNwPassword" placeholder="新密码 (不修改则留空)">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputnNwPassword2">新密码重复</label>
                <div class="controls">
                    <input type="password" name="newPassword2" id="inputnNwPassword2" placeholder="新密码重复 (不修改则留空)">
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> 修改</button>
                    <button type="reset" class="btn"><i class="icon-share-alt icon-white"></i> 重置</button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php require('page-admin-footer.php'); ?>