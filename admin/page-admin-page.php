<?php

/**
 * 登陆后页面引用
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require('page-admin.php');
require('page.php');

//权限不足页面
$errorPagePower = 'error.php?e=power';
?>
