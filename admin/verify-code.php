<?php

/**
 * 验证码页面
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require('config.php');
require(DIR_LIB . DS . 'plug-vcode.php');
CoreHeader::noCache();
PlugVCode(4, 20, 150, 35);
?>
