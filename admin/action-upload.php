<?php

/**
 * 上传文件处理器
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require_once('page-admin.php');
$msg = 0;
if (isset($_GET['action']) == true && $sysUser->checkPower('FILE') == true) {
    require(DIR_LIB . DS . 'sys-post.php');
    require(DIR_LIB . DS . 'sys-upload.php');
    $sysPost = new SysPost($coreDB, $sysLog);
    if ($_GET['action'] == 'upload' && isset($_FILES['upload']) == true && isset($_GET['parent']) == true) {
        $parent = (int) $_GET['parent'];
        $sysUpload = new SysUpload($sysPost, $sysLog, DIR_UPLOAD, $userRes['id']);
        $fileID = $sysUpload->upload($_FILES['upload'], $parent, false);
        $msg = $fileID > 0 ? 1 : 2;
    } elseif ($_GET['action'] == 'edit' && isset($_GET['id']) == true && isset($_POST['title']) == true && isset($_POST['content']) == true) {
        $id = (int) $_GET['id'];
        $editRes = $sysPost->get($id, null);
        if ($editRes) {
            $res = $sysPost->edit($editRes['id'], $editRes['post_user'], $_POST['title'], $_POST['content'], $editRes['post_url'], $editRes['post_status'], $editRes['post_parent'], $editRes['post_order'], $editRes['post_password']);
            $msg = $res == true ? 3 : 4;
        }
    } elseif ($_GET['action'] == 'delete' && isset($_GET['id']) == true) {
        $id = (int) $_GET['id'];
        $sysUpload = new SysUpload($sysPost, $sysLog, DIR_UPLOAD, $userRes['id']);
        $res = $sysUpload->del($id);
        $msg = $res == true ? 5 : 6;
    }
}
$coreFeedback->output('url', 'admin-file.php?active=2&msg=' . $msg);
?>
