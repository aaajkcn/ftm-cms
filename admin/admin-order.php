<?php

/**
 * 订单管理页面
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require('page-admin-page.php');

//判断权限
if(ORDER_ON == true && $sysUser->checkPower('ORDER') == false){
    CoreHeader::toURL($errorPagePower);
}

//页码生成器
require(DIR_LIB . DS . 'page-pagination.php');

//当前动作
$action = isset($_GET['action']) == true ? $_GET['action'] : 'check';

//引用并声明product对象
require(DIR_LIB . DS . 'sys-product.php');
$sysProduct = new SysProduct($coreDB, $sysLog);

//引用并声明order对象
require(DIR_LIB . DS . 'sys-order.php');
$sysOrder = new SysOrder($coreDB, $sysLog, $coreSession, $sysProduct);

//引用字符串截取插件
require(DIR_LIB . DS . 'plug-substrutf8.php');

//获取列表
$status = isset($_GET['status']) == true && in_array($_GET['status'], $sysOrder->status) == true ? $_GET['status'] : $sysOrder->status[0];
$where = '`order_status` = \'' . $status . '\' and `order_parent` = 0';
$attrs = null;
$page = isset($_GET['page']) == true ? (int) $_GET['page'] : 1;
$max = 10;
$sort = isset($_GET['sort']) == true ? (int) $_GET['sort'] : 3;
$desc = isset($_GET['desc']) == true ? true : false;
$tableList = $sysOrder->getList($where, $attrs, $page, $max, $sort, $desc);
$tableListRow = $sysOrder->getListRow($where, $attrs);

//获取当前焦点
$orderRes = null;
if(isset($_GET['id']) == true){
    $id = (int) $_GET['id'];
    $orderRes = $sysOrder->get($id);
}

//如果是审核模式
if ($action == 'check') {
    $orderRes = null;
    $where = '`order_status` = \'' . $sysOrder->status[0] . '\' and `order_parent` = 0';
    $tableList = $sysOrder->getList($where, null, 1, 1, 0, false);
    if ($tableList) {
        $orderRes = $tableList[0];
    }
}

//提示信息
$msgArr = array('无法执行操作，请重试。', '标记成功！', '标记失败，请稍后重试。', '删除成功！', '无法删除，请稍后重试。');
$msgTypeArr = array(array('错误！', 'error'), array('成功！', 'success'), array('错误！', 'info'), array('成功！', 'success'), array('错误！', 'info'));

require('page-admin-top.php');
?>
<div class="row">&nbsp;</div>
<div class="row">
    <div class="span12">
        <a href="admin-order.php?active=2&action=check&status=<?php echo $sysOrder->status[0]; ?>" target="_self" class="btn btn-primary"><i class="icon-th-list icon-white"></i> 开始处理订单</a>
        <a href="admin-order.php?active=2&action=list&status=<?php echo $sysOrder->status[0]; ?>" target="_self" class="btn btn-info"><i class="icon-tasks icon-white"></i> 查看未处理</a>
        <a href="admin-order.php?active=2&action=list&status=<?php echo $sysOrder->status[1]; ?>" target="_self" class="btn btn-info"><i class="icon-thumbs-up icon-white"></i> 查看已完成</a>
        <a href="admin-order.php?active=2&action=list&status=<?php echo $sysOrder->status[2]; ?>" target="_self" class="btn btn-warning"><i class="icon-trash icon-white"></i> 查看回收站</a>
    </div>
</div>
<div class="row">&nbsp;</div>

<?php if($action == 'list'){ ?>
<h2>订单列表</h2>
<hr>
<?php if($tableList){ ?>
<table class="table table-hover table-list">
    <thead>
        <tr>
            <th>创建时间</th>
            <th>IP</th>
            <th>联系人</th>
            <th>联系方式</th>
            <th>订单金额</th>
            <th>操作</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($tableList as $k=>$v){ ?>
        <tr>
            <td><?php echo $v['order_date']; ?></td>
            <td><?php echo $v['order_ip']; ?></td>
            <td><?php echo $v['order_name']; ?></td>
            <td><?php echo $v['order_contact']; ?></td>
            <td><?php echo $v['order_price']; ?> 元</td>
            <td>
                <a href="admin-order.php?active=2&action=view&id=<?php echo $v['id']; ?>" class="btn btn-info"><i class="icon-search icon-white"></i> 查看详情</a>
                <?php if($status != $sysOrder->status[1]){ ?>
                <a href="action-order.php?action=check&id=<?php echo $v['id']; ?>&pass=1" class="btn btn-primary"><i class="icon-ok icon-white"></i> 完成交易</a> 
                <?php } ?>
                <?php if($status != $sysOrder->status[2]){ ?>
                <a href="action-order.php?action=check&id=<?php echo $v['id']; ?>&pass=0" class="btn btn-warning"><i class="icon-trash icon-white"></i> 删除</a> 
                <?php } ?>
                <?php if($status == $sysOrder->status[2]){ ?>
                <a href="action-order.php?action=delete&id=<?php echo $v['id']; ?>" class="btn btn-warning"><i class="icon-trash icon-white"></i> 彻底删除</a></td>
                <?php } ?>
        </tr>
        <?php } ?>
    </tbody>
</table>
<div class="text-center">
    <p><?php echo PagePaginationQuick('admin-order.php?active=2&action=list&status='.$status.'&page=', $page, $tableListRow); ?></p>
</div>
<?php }else{ ?>
<div class="text-center">
    <p>&nbsp;</p>
    <p>没有任何订单。</p>
</div>
<?php } ?>
<?php } ?>

<?php if(($action == 'view' || $action == 'check') && $orderRes){ $orderProductList = $sysOrder->getList('`order_parent` = :parent', array(':parent'=>array($orderRes['id'],PDO::PARAM_INT)), 1, 9999, 0, false); ?>
<h2>查阅订单</h2>
<hr>
<div class="hero-unit">
    <div class="page-header">
        <h3><?php echo $orderRes['id']; ?><small>&nbsp;&nbsp;&nbsp;<?php echo $orderRes['order_date']; ?> - <?php echo $orderRes['order_ip']; ?></small></h3>
    </div>
    <p>联系人 : <?php echo $orderRes['order_name']; ?></p>
    <p>联系方式 : <?php echo $orderRes['order_contact']; ?></p>
    <p>订单备注 : <?php echo $orderRes['order_note']; ?></p>
    <p>订单状态 : <?php if($orderRes['order_status'] == $sysOrder->status[0]){ echo '等待处理'; }else{ echo '处理完成'; } ?></p>
    <?php if($orderProductList){ ?>
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>产品名称</th>
                <th>产品描述</th>
                <th>费用</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($orderProductList as $v){ ?>
            <tr>
                <td><?php echo $v['order_name']; ?></td>
                <td><?php echo $v['order_note']; ?></td>
                <td><?php echo $v['order_price']; ?> 元</td>
            </tr>
            <?php } ?>
        </tbody>
        <tfoot>
            <tr>
                <th>总金额</th>
                <th></th>
                <th><?php echo $orderRes['order_price']; ?> 元</th>
            </tr>
        </tfoot>
    </table>
    <?php } ?>
    <p>&nbsp;</p>
    <p>
        <a href="action-order.php?action=check&id=<?php echo $orderRes['id']; ?>&pass=1" class="btn btn-primary"><i class="icon-ok icon-white"></i> 完成交易</a> 
        <a href="action-order.php?action=check&id=<?php echo $orderRes['id']; ?>&pass=0" class="btn btn-warning"><i class="icon-trash icon-white"></i> 删除订单</a> 
    </p>
</div>
<?php } ?>
<?php if($action == 'check' && !$orderRes){ ?>
<p>没有需要处理的订单。</p>
<?php } ?>

<?php require('page-admin-footer.php'); ?>
