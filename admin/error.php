<?php
/**
 * 错误页面
 * @author fotomxq <fotomxq.me>
 * @version 2
 * @package admin
 */
require('page.php');

//错误列
$errors = array('un' => '未知错误！', 'ip-ban' => 'IP禁止访问！', 'power' => '您没有权限访问该页面！请联系管理员，让您的用户获取该部分权限。');
$errorKey = 'un';

//找到对应错误
if (isset($_GET['e']) == true && isset($errors[$_GET['e']]) == true) {
    $errorKey = $_GET['e'];
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>错误</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="assets/bootstrap.css" rel="stylesheet">
        <link href="assets/bootstrap-responsive.css" rel="stylesheet">
        <link href="assets/glob.css" rel="stylesheet">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="assets/html5shiv.js"></script>
        <![endif]-->
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/logo-144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/logo-114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/logo-72.png">
        <link rel="apple-touch-icon-precomposed" href="assets/logo-57.png">
        <link rel="shortcut icon" href="assets/favicon.ico">
    </head>
    <body>
        <div id="wrap">
            <div id="push"></div>
            <div class="container">
                <div class="alert alert-block">
                    <h4>错误 !</h4>
                    <p>&nbsp;</p>
                    <?php echo $errors[$errorKey]; ?>
                    <p><a class="btn btn-inverse" href="index.php">返回首页</a></p>
                </div>
            </div>
            <div id="push"></div>
        </div>
        <?php require('footer.php'); ?>
        <script src="assets/jquery.js"></script>
        <script src="assets/bootstrap.js"></script>
    </body>
</html>