<?php

/**
 * 备份操作
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require_once('page-admin.php');
$msg = 0;
if (isset($_GET['action']) == true && $sysUser->checkPower('BACKUP') == true) {
    //引用并声明backup对象
    require(DIR_LIB . DS . 'sys-backup.php');
    $sysBackup = new SysBackup($coreDB, DIR_BACKUP . DS . 'ls', DIR_BACKUP);
    $dirs = array(DIR_UPLOAD);
    if ($_GET['action'] == 'backup') {
        $res = $sysBackup->backup($dirs);
        $msg = $res == true ? 1 : 2;
    } elseif ($_GET['action'] == 'restore' && isset($_GET['file']) == true && $sysUser->checkPower('RESTORE') == true) {
        $backupFile = DIR_BACKUP . DS . $_GET['file'];
        if (CoreFile::isFile($backupFile) == true) {
            $res = $sysBackup->restore($backupFile, $dirs);
            $msg = $res == true ? 3 : 4;
        }
    } elseif ($_GET['action'] == 'download' && isset($_GET['file']) == true) {
        $backupFile = DIR_BACKUP . DS . $_GET['file'];
        if (CoreFile::isFile($backupFile) == true) {
            $fileSize = filesize($backupFile);
            $fileName = basename($backupFile);
            CoreHeader::downloadFile($fileSize, $fileName, $backupFile);
        }
    } elseif ($_GET['action'] == 'delete' && isset($_GET['file']) == true) {
        $backupFile = DIR_BACKUP . DS . $_GET['file'];
        if (CoreFile::isFile($backupFile) == true) {
            $res = CoreFile::deleteFile($backupFile);
            $msg = $res == true ? 5 : 6;
        }
    }
}
$coreFeedback->output('url', 'admin-backup.php?active=3&msg=' . $msg);
?>
