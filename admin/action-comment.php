<?php

/**
 * 评论处理器
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require_once('page-admin.php');
$msg = 0;
if (isset($_GET['action']) == true && $sysUser->checkPower('COMMENT') == true) {
    //引用并声明comment对象
    require(DIR_LIB . DS . 'sys-comment.php');
    $sysComment = new SysComment($coreDB, $sysLog);
    if ($_GET['action'] == 'check' && isset($_GET['id']) == true && isset($_GET['pass']) == true) {
        $id = (int) $_GET['id'];
        $status = $_GET['pass'] ? $sysComment->status[1] : $sysComment->status[2];
        $res = $sysComment->changeStatus($id, $status);
        $msg = $res == true ? 1 : 2;
    } elseif ($_GET['action'] == 'delete' && isset($_GET['id']) == true) {
        $id = (int) $_GET['id'];
        $res = $sysComment->del($id);
        $msg = $res == true ? 3 : 4;
    }
}
$coreFeedback->output('url', 'admin-comment.php?active=2&msg=' . $msg);
?>
