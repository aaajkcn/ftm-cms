<?php

/**
 * 备份和还原中心
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require('page-admin-page.php');

//判断权限
if($sysUser->checkPower('BACKUP') == false || $sysUser->checkPower('RESTORE') == false){
    CoreHeader::toURL($errorPagePower);
}

//引入并初始化备份操作
require(DIR_LIB . DS . 'sys-backup.php');
$sysBackup = new SysBackup($coreDB, DIR_BACKUP . DS . 'ls', DIR_BACKUP, DS);

//页码生成器
require(DIR_LIB . DS . 'page-pagination.php');

//获取列表
$page = isset($_GET['page']) == true ? (int) $_GET['page'] : 1;
$max = 10;
$backList = $sysBackup->getList($page, $max);
$backListRow = $sysBackup->row;

//提示信息
$msgArr = array('无法执行操作，请重试。', '备份成功！', '备份失败，请稍后重试。', '还原成功！', '还原失败，可能是备份文件损坏，请检查文件完整性后重试。', '删除成功！', '无法删除备份文件，请稍后重试。', '备份和还原数据库需要一些时间处理，在此过程中请耐心等待...');
$msgTypeArr = array(array('错误！', 'error'), array('成功！', 'success'), array('失败！', 'error'), array('成功！', 'success'), array('失败！', 'error'), array('成功！', 'success'), array('失败！', 'error'), array('提示！', 'info'));
$_GET['msg'] = isset($_GET['msg']) == true ? $_GET['msg'] : 7;

require('page-admin-top.php');
?>
<h2>备份操作</h2>
<hr>
<p><a href="action-backup.php?&action=backup" class="btn btn-primary"><i class="icon-retweet icon-white"></i> 开始备份</a></p>
<p>&nbsp;</p>
<h2>备份文件列表</h2>
<hr>
<?php if($backList){ ?>
<table class="table table-hover">
    <thead>
        <tr>
            <th>文件名称</th>
            <th>创建时间</th>
            <th>文件大小</th>
            <th>操作</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($backList as $v){ ?>
        <tr>
            <td><?php echo $v[0]; ?></td>
            <td><?php echo $v[1]; ?></td>
            <td><?php echo $v[2]; ?></td>
            <td>
                <a href="action-backup.php?action=download&file=<?php echo $v[0]; ?>" class="btn btn-info"><i class="icon-download icon-white"></i> 下载</a>
                <a href="action-backup.php?action=restore&file=<?php echo $v[0]; ?>" class="btn btn-danger"><i class="icon-retweet icon-white"></i> 还原</a>
                <a href="action-backup.php?action=delete&file=<?php echo $v[0]; ?>" class="btn btn-danger"><i class="icon-trash icon-white"></i> 删除</a>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<div class="text-center"><?php echo PagePaginationQuick('admin-backup.php?active=3&page=', $page, $backListRow, $max); ?></div>
<?php }else{ ?>
<p>没有备份文件，请先尝试备份。</p>
<?php } ?>
<?php require('page-admin-footer.php'); ?>
