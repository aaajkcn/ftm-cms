<?php
/**
 * 后台中心
 * @author fotomxq <fotomxq.me>
 * @version 2
 * @package admin
 */
require('page-admin-page.php');

//待办事项
$taskList = null;

//引用并声明post对象
require(DIR_LIB . DS . 'sys-post.php');
$sysPost = new SysPost($coreDB, $sysLog);

//引入并初始化备份操作
require(DIR_LIB . DS . 'sys-backup.php');
$sysBackup = new SysBackup($coreDB, DIR_BACKUP . DS . 'ls', DIR_BACKUP, DS);

//遍历所有事件，查询是否有待办事件
//如果有用户数据
$taskPostRow = $sysPost->getListRow();
if ($taskPostRow > 0) {
    //任务-尚未进行备份
    $taskBackupList = $sysBackup->getList(1,999);
    if (!$taskBackupList) {
        $taskList[] = array('尚未备份', '系统发现您已经创建了部分数据，但是尚未进行任何备份。为了确保系统数据的安全，建议备份数据库。', 'admin-backup.php?active=3', '开始备份数据库');
    }
    //任务-最后一次备份时间超过15天
    $backupFileLastRes = DIR_BACKUP . DS . $taskBackupList[0][0];
    $backupFileLastTime = @fileatime($backupFileLastRes);
    if ($backupFileLastTime) {
        $time15Time = time() - 1296000;
        if ($backupFileLastTime < $time15Time) {
            $taskList[] = array('需要备份', '您已经有很久没有备份系统了，请进行备份，以确保数据安全。', 'admin-backup.php?active=3', '开始备份数据库');
        }
    }
    //任务-备份文件超过10个，提示删除
    if (count($taskBackupList) > 10) {
        $taskList[] = array('删除备份', '系统保存了太多的备份文件，请删除一些备份文件。', 'admin-backup.php?active=3', '删除备份文件');
    }
    //任务-备份文件平均太大，建议下载到本次存储
    $backupFileLastRes = DIR_BACKUP . DS . $taskBackupList[0][0];
    $backupFileLastSize = filesize($backupFileLastRes) / 1024 / 1024;
    if($backupFileLastSize > 500){
        $taskList[] = array('下载备份', '保存的备份文件过大，建议下载到本地存储，以节约服务器资源。', 'admin-backup.php?active=3', '下载备份文件');
    }
}

//引入并声明candadites类
require(DIR_LIB . DS . 'sys-candadites.php');
$sysCandadites = new SysCandadites($coreDB, $sysLog, CANDADITES_TIME_LIMIT);

//任务-如果存在待审核应聘
$where = '`cs_status` = \'' . $sysCandadites->status[0] . '\'';
$candaditesNum = $sysCandadites->getListRow($where);
if ($candaditesNum > 0) {
    $taskList[] = array('人力资源部', '有 <b>' . $candaditesNum . '</b> 位应聘者前来应聘，请您处理！', 'admin-candadites.php?active=2&action=check', '开展面试工作');
}

//引入并声明product类
require(DIR_LIB . DS . 'sys-product.php');
$sysProduct = new SysProduct($coreDB, $sysLog);

//引入并声明order类
require(DIR_LIB . DS . 'sys-order.php');
$sysOrder = new SysOrder($coreDB, $sysLog, $coreSession, $sysProduct);

//任务-如果存在待审核订单
$orderNum = $sysOrder->getListRow('`order_status` = \'' . $sysOrder->status[0] . '\'', null);
if($orderNum > 0){
    $taskList[] = array('销售部', '有 <b>' . $orderNum . '</b> 个订单需要处理！', 'admin-order.php?active=2&action=check', '开始交易');
}

//引入并声明comment类
require(DIR_LIB . DS . 'sys-comment.php');
$sysComment = new SysComment($coreDB, $sysLog);

//任务-如果存在待审核评论
$commentNum = $sysComment->getListRow('`ct_status` = \'' . $sysComment->status[0] . '\'', null);
if($commentNum > 0){
    $taskList[] = array('新的评论', '有 <b>' . $commentNum . '</b> 个评论需要处理！', 'admin-comment.php?active=2&action=check', '审核评论');
}

//引入头
require('page-admin-top.php');
?>
<h2>欢迎您，<?php echo $userRes['user_name']; ?>！</h2>
<hr>
<p>登陆时间：<?php echo $userRes['user_lasttime']; ?></p>
<p>登陆IP：<?php echo $userRes['user_ip']; ?></p>
<h2>待办事项</h2>
<hr>
<?php if($taskList){ foreach($taskList as $v){ ?>
<div class="hero-unit">
    <h3><?php echo $v[0]; ?></h3>
    <p><?php echo $v[1]; ?></p>
    <p><a href="<?php echo $v[2]; ?>" class="btn btn-primary"><?php echo $v[3]; ?></a></p>
</div>
<?php } }else{ ?>
<div class="hero-unit">
    <h3>恭喜</h3>
    <p>您已经完成了所有待办事项。</p>
    <p><a href="#" class="btn btn-primary">我明白了</a></p>
</div>
<?php } ?>
<?php require('page-admin-footer.php'); ?>
