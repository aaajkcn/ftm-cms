<?php
/**
 * 用户组管理
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require('page-admin-page.php');

//判断权限
if($sysUser->checkPower('USER-GROUP') == false){
    CoreHeader::toURL($errorPagePower);
}

require(DIR_LIB . DS . 'page-pagination.php');

//当前动作
$action = isset($_GET['action']) == true ? $_GET['action'] : 'add';

//获取用户组列表
$page = isset($_GET['page']) == true ? (int) $_GET['page'] : 1;
$where = '1';
$attrs = null;
$groupList = $sysUser->getGroupList($where, $attrs, $page);
if ($groupList) {
    foreach ($groupList as $k => $v) {
        $powerStr = '';
        if ($v['group_power']) {
            $powers = explode(',', $v['group_power']);
            foreach ($powers as $vP) {
                if (isset($sysUser->powers[$vP]) == true) {
                    $powerStr .= ',' . $sysUser->powers[$vP];
                }
            }
            $powerStr = substr($powerStr, 1);
        }
        $groupList[$k]['group_power'] = $powerStr;
    }
}

//记录数
$groupListRow = $sysUser->getGroupListRow($where, $attrs);

//提示信息
$msgArr = array('无法执行操作，请重试。', '添加成功！', '添加失败，请检查相关信息是否正确。', '修改成功！', '修改失败，请检查相关信息是否正确。', '删除成功！', '无法删除该用户组，请稍后重试。');
$msgTypeArr = array(array('错误！', 'error'), array('成功！', 'success'), array('错误！', 'info'), array('成功！', 'success'), array('错误！', 'info'), array('成功！', 'success'), array('错误！', 'info'));

//获取编辑用户信息
$editRes;
if($action == 'edit' && isset($_GET['id']) == true) {
    $id = (int) $_GET['id'];
    $editRes = $sysUser->getGroup($id);
    if ($editRes && $editRes['group_power']) {
        $editRes['group_power'] = explode(',', $editRes['group_power']);
    }
}

//包含页面
$includePage['js'][] = 'admin-user-group';
$includePage['css'][] = 'admin-user-group';

require('page-admin-top.php');
?>
<p>&nbsp;</p>
<p>注意，访客用户组不能删除或修改，同时您无法删除您当前的用户组。</p>
<h2>用户组管理</h2>
<table class="table table-hover table-list">
    <thead>
        <tr>
            <th>名称</th>
            <th>权限</th>
            <th>操作</th>
        </tr>
    </thead>
    <tbody>
        <?php if($groupList){ foreach($groupList as $v){ ?>
        <tr>
            <td><?php echo $v['group_name']; ?></td>
            <td><?php echo $v['group_power']; ?></td>
            <td><a href="admin-user-group.php?active=3&action=edit&id=<?php echo $v['id']; ?>" class="btn"><i class="icon-pencil icon-white"></i> 修改</a> <a href="action-user-group.php?action=delete&id=<?php echo $v['id']; ?>" class="btn btn-warning"><i class="icon-trash icon-white"></i> 删除</a></td>
        </tr>
        <?php } } ?>
    </tbody>
</table>
<div class="text-center">
    <p><?php echo PagePaginationQuick('admin-user-group.php?active=3&page=', $page, $groupListRow); ?></p>
</div>
<?php if($action != 'edit'){ ?>
<h2>添加用户组</h2>
<hr>
<form class="form-horizontal" action="action-user-group.php?action=add" method="post">
    <div class="control-group">
        <label class="control-label" for="inputName">用户组名称</label>
        <div class="controls">
            <input type="text" name="name" id="inputName" placeholder="用户组名称 (必填)">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputPower">权限</label>
        <div class="controls">
            <?php foreach($sysUser->powers as $k=>$v){ ?>
            <label class="checkbox inline">
                <input name="power[]" type="checkbox" value="<?php echo $k; ?>"> <?php echo $v; ?>
            </label>
            <?php } ?>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> 添加</button>
            <a href="#select-power-all" class="btn">全部权限</a>
            <a href="#select-power-reverse" class="btn">反选权限</a>
            <a href="#select-power-cancel" class="btn">取消选择权限</a>
        </div>
    </div>
</form>
<?php } ?>
<?php if($action == 'edit' && $editRes){ ?>
<h2>修改用户组</h2>
<hr>
<form class="form-horizontal" action="action-user-group.php?action=edit&id=<?php echo $editRes['id']; ?>" method="post">
    <div class="control-group">
        <label class="control-label" for="inputName">用户组名称</label>
        <div class="controls">
            <input type="text" name="name" id="inputName" placeholder="用户组名称 (必填)" value="<?php echo $editRes['group_name']; ?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputPower">权限</label>
        <div class="controls">
            <?php foreach($sysUser->powers as $k=>$v){ $checkStr = ''; if($editRes['group_power'] && in_array($k, $editRes['group_power']) == true){ $checkStr = 'checked="checked"'; } ?>
            <label class="checkbox inline">
                <input name="power[]" type="checkbox" value="<?php echo $k; ?>"<?php echo $checkStr; ?>> <?php echo $v; ?>
            </label>
            <?php } ?>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> 修改</button>
            <a href="#select-power-all" class="btn">全部权限</a>
            <a href="#select-power-reverse" class="btn">反选权限</a>
            <a href="#select-power-cancel" class="btn">取消选择权限</a>
        </div>
    </div>
</form>
<?php } ?>
<?php require('page-admin-footer.php'); ?>