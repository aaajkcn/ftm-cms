<?php
/**
 * 登录后顶部页面引用
 * @author fotomxq <fotomxq.me>
 * @version 2
 * @package admin
 */
if (isset($pageIncludeBool) == false) {
    die();
}
$menuKey = isset($_GET['active']) == true ? $_GET['active'] : 0;
//提示消息
$msgArr = isset($msgArr) == true ? $msgArr : array('发生未知错误，请重试。');
$msgTypeArr = isset($msgTypeArr) == true ? $msgTypeArr : array(array('错误！', 'error'));
$msgKey = isset($_GET['msg']) == true && isset($msgArr[$_GET['msg']]) == true ? $_GET['msg'] : false;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo $webTitle; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="assets/bootstrap.css" rel="stylesheet">
        <link href="assets/bootstrap-responsive.css" rel="stylesheet">
        <link href="assets/glob.css" rel="stylesheet">
        <link href="assets/glob-admin.css" rel="stylesheet">
        <?php if($includePage['css'] !== null){ foreach($includePage['css'] as $v){ ?>
        <link href="assets/<?php echo $v; ?>.css" rel="stylesheet">
        <?php } } ?>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="assets/html5shiv.js"></script>
        <![endif]-->
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/logo-144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/logo-114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/logo-72.png">
        <link rel="apple-touch-icon-precomposed" href="assets/logo-57.png">
        <link rel="shortcut icon" href="assets/favicon.ico">
    </head>
    <body>
        <div id="wrap">
            <div class="navbar navbar-inverse navbar-fixed-top">
                <div class="navbar-inner">
                    <div class="container-fluid">
                        <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="brand brand-fixed" href="admin-center.php"><img class="logo" src="assets/logo-144.png" /><?php echo $webTitle; ?></a>
                        <div class="nav-collapse collapse">
                            <p class="navbar-text pull-right">
                                欢迎您，<a href="admin-user-center.php?active=1" class="navbar-link"><?php echo $userRes['user_name']; ?></a> <a href="logout.php" class="navbar-link">退出登录</a>
                            </p>
                            <ul id="top-menu" class="nav">
                                <li class="<?php if(!$menuKey){ echo 'active'; } ?>"><a href="admin-center.php">首页</a></li>
                                <?php if($sysUser->checkPower('USER-SELF') == true){ ?>
                                <li class="<?php if($menuKey == 1){ echo 'active'; } ?>"><a href="admin-user-center.php?active=1">个人中心</a></li>
                                <?php } ?>
                                <?php if($sysUser->checkPower('NEWS') == true || $sysUser->checkPower('PRO') == true || $sysUser->checkPower('SORT') == true || $sysUser->checkPower('FILE') == true || $sysUser->checkPower('ORDER') == true || $sysUser->checkPower('ORDER-CHECK') == true || $sysUser->checkPower('CANDIDATES') == true || $sysUser->checkPower('CANDIDATES-CHECK') == true || $sysUser->checkPower('COMMENT') == true){ ?>
                                <li class="dropdown<?php if($menuKey == 2){ echo ' active'; } ?>">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">信息管理 <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <?php if($sysUser->checkPower('NEWS') == true || $sysUser->checkPower('PRO') == true || $sysUser->checkPower('SORT') == true){ ?>
                                        <li class="nav-header">信息</li>
                                        <?php if($sysUser->checkPower('NEWS') == true || $sysUser->checkPower('PRO') == true){ ?>
                                        <li><a href="admin-infos.php?active=2">文章管理</a></li>
                                        <?php } ?>
                                        <?php if($sysUser->checkPower('SORT') == true){ ?>
                                        <li><a href="admin-sort.php?active=2">分类管理</a></li>
                                        <?php } ?>
                                        <li class="divider"></li>
                                        <?php } ?>
                                        <?php if($sysUser->checkPower('FILE') == true){ ?>
                                        <li class="nav-header">媒体</li>
                                        <li><a href="admin-file.php?active=2">文件管理</a></li>
                                        <li class="divider"></li>
                                        <?php } ?>
                                        <?php if(ORDER_ON == true && ($sysUser->checkPower('ORDER') == true || $sysUser->checkPower('ORDER-CHECK') == true)){ ?>
                                        <li class="nav-header">订单</li>
                                        <?php if($sysUser->checkPower('ORDER-CHECK') == true){ ?>
                                        <li><a href="admin-order.php?active=2&action=check">审阅订单</a></li>
                                        <?php } ?>
                                        <?php if($sysUser->checkPower('ORDER') == true){ ?>
                                        <li><a href="admin-order.php?active=2&action=list">管理订单</a></li>
                                        <?php } ?>
                                        <li class="divider"></li>
                                        <?php } ?>
                                        <?php if(CANDADITES_ON == true && ($sysUser->checkPower('CANDIDATES') == true || $sysUser->checkPower('CANDIDATES-CHECK') == true)){ ?>
                                        <li class="nav-header">招聘</li>
                                        <?php if($sysUser->checkPower('CANDIDATES-CHECK') == true){ ?>
                                        <li><a href="admin-candadites.php?active=2&action=check">审阅应聘</a></li>
                                        <?php } ?>
                                        <?php if($sysUser->checkPower('CANDIDATES') == true){ ?>
                                        <li><a href="admin-candadites.php?active=2&action=list">管理应聘</a></li>
                                        <?php } ?>
                                        <li class="divider"></li>
                                        <?php } ?>
                                        <?php if(COMMENT_ON == true && $sysUser->checkPower('COMMENT') == true){ ?>
                                        <li class="nav-header">评论</li>
                                        <li><a href="admin-comment.php?active=2&action=check" target="_self">审核评论</a></li>
                                        <li><a href="admin-comment.php?active=2&action=list" target="_self">管理评论</a></li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                <?php } ?>
                                <li class="dropdown<?php if($menuKey == 3){ echo ' active'; } ?>">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">高级 <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <?php if($sysUser->checkPower('BASIC') == true || $sysUser->checkPower('USER') == true || $sysUser->checkPower('USER-GROUP') == true || $sysUser->checkPower('BACKUP') == true || $sysUser->checkPower('RESTORE') == true || $sysUser->checkPower('LOG') == true){ ?>
                                        <?php if($sysUser->checkPower('BASIC') == true){ ?>
                                        <li class="nav-header">基本操作</li>
                                        <li><a href="admin-operate-basic.php?active=3#basic">基本设定</a></li>
                                        <li><a href="admin-operate-index.php?active=3">首页设置</a></li>
                                        <li><a href="admin-theme.php?active=3">网站主题</a></li>
                                        <?php } ?>
                                        <?php if($sysUser->checkPower('USER') == true || $sysUser->checkPower('USER-GROUP') == true || $sysUser->checkPower('BACKUP') == true || $sysUser->checkPower('RESTORE') == true || $sysUser->checkPower('LOG') == true){ ?>
                                        <li class="divider"></li>
                                        <li class="nav-header">高级操作</li>
                                        <?php if($sysUser->checkPower('USER') == true){ ?>
                                        <li><a href="admin-user.php?active=3">用户管理</a></li>
                                        <?php } ?>
                                        <?php if($sysUser->checkPower('USER-GROUP') == true){ ?>
                                        <li><a href="admin-user-group.php?active=3">用户组管理</a></li>
                                        <?php } ?>
                                        <?php if($sysUser->checkPower('BACKUP') == true || $sysUser->checkPower('RESTORE') == true){ ?>
                                        <li><a href="admin-backup.php?active=3">系统备份和还原</a></li>
                                        <?php } ?>
                                        <?php if($sysUser->checkPower('LOG') == true){ ?>
                                        <li><a href="admin-log.php?active=3">站点日志</a></li>
                                        <?php } ?>
                                        <?php } ?>
                                        <li class="divider"></li>
                                        <?php } ?>
                                        <li class="nav-header">关于信息</li>
                                        <?php if($sysUser->checkPower('STATISTICS') == true){ ?>
                                        <li><a href="admin-statistics.php?active=3">站点统计</a></li>
                                        <?php } ?>
                                        <li><a href="admin-license.php?active=3">关于协议</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div><!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <div id="push"></div>
            <div class="container">
                <?php if($msgKey !== false){ ?>
                <div id="msg" class="alert alert-<?php echo $msgTypeArr[$msgKey][1]; ?>">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong><?php echo $msgTypeArr[$msgKey][0]; ?></strong> <?php echo $msgArr[$msgKey]; ?>
                </div>
                <?php } ?>