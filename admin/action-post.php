<?php

/**
 * post处理器
 * <p>因特殊原因，该处理器部分作废。仅用于admin-sort.php页面处理。</p>
 * @author fotomxq <fotomxq.me>
 * @version 2
 * @package admin
 */
require_once('page-admin.php');
require(DIR_LIB . DS . 'sys-post.php');
$sysPost = new SysPost($coreDB, $sysLog);
$msg = 0;
$url = 'error.php?e=un&';
if (isset($_GET['action']) == true && isset($_GET['type']) == true && isset($_GET['parent']) == true) {
    $actionReady = false;
    //准备参数默认值
    $postTitle = '';
    $postContent = '';
    $postURL = null;
    $postStatus = $sysPost->status[0];
    $postParent = (int) $_GET['parent'];
    $postOrder = 0;
    $postPassword = null;
    $postType = 'text';
    $postMeta = 'text/html';
    //根据提交来源更换参数值
    switch ($_GET['type']) {
        case 'sort':
            if ($sysUser->checkPower('SORT') == false) {
                CoreHeader::toURL('error.php?e=power');
            }
            $postType = $sysPost->type[0];
            $url = 'admin-sort.php?active=2&parent=' . $postParent . '&msg=';
            //分析动作
            if ($_GET['action'] == 'add' && isset($_POST['title']) == true && isset($_POST['content']) == true) {
                $postTitle = $_POST['title'];
                $postContent = $_POST['content'];
                $actionReady = true;
            } elseif ($_GET['action'] == 'edit' && isset($_GET['id']) == true && isset($_POST['title']) == true && isset($_POST['content']) == true) {
                $id = $_GET['id'];
                $postRes = $sysPost->get($id, null);
                if ($postRes) {
                    $res = $sysPost->edit($postRes['id'], $postRes['post_user'], $_POST['title'], $_POST['content'], $postRes['post_url'], $postRes['post_status'], $postRes['post_parent'], $postRes['post_order'], $postRes['post_password']);
                    $msg = $res == true ? 3 : 4;
                }
                $actionReady = true;
            }
            break;
        case 'text':
            break;
        default:
            $actionReady = false;
            break;
    }
    //添加操作
    if ($_GET['action'] == 'add' && $actionReady == true) {
        $res = $sysPost->add($userRes['id'], $postTitle, $postContent, $postURL, $postStatus, $postParent, $postOrder, $postPassword, $postType, $postMeta);
        $msg = $res > 0 ? 1 : 2;
    }
    //编辑操作
    if ($_GET['action'] == 'edit' && isset($_GET['id']) == true && $actionReady == true) {
        //移动到上面
    }
    //对调排序
    if ($_GET['action'] == 'order' && isset($_GET['id']) == true && isset($_GET['dest']) == true) {
        $id = $_GET['id'];
        $dest = $_GET['dest'];
        $postSrcRes = $sysPost->get($id, null);
        $postDestRes = $sysPost->get($dest, null);
        if ($postSrcRes && $postDestRes) {
            $res = $sysPost->edit($postSrcRes['id'], $postSrcRes['post_user'], $postSrcRes['post_title'], $postSrcRes['post_content'], $postSrcRes['post_url'], $postSrcRes['post_status'], $postSrcRes['post_parent'], $postDestRes['post_order'], $postSrcRes['post_password']);
            if ($res == true) {
                $res = $sysPost->edit($postDestRes['id'], $postDestRes['post_user'], $postDestRes['post_title'], $postDestRes['post_content'], $postDestRes['post_url'], $postDestRes['post_status'], $postDestRes['post_parent'], $postSrcRes['post_order'], $postDestRes['post_password']);
                $msg = $res == true ? 7 : 8;
            }
        }
    }
    //删除操作
    if ($_GET['action'] == 'delete' && isset($_GET['id']) == true) {
        $id = $_GET['id'];
        $res = $sysPost->del($id);
        $msg = $res == true ? 5 : 6;
    }
}
$coreFeedback->output('url', $url . $msg);
?>
