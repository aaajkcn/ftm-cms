<?php

/**
 * 用户操作
 * @author fotomxq <fotomxq.me>
 * @version 2
 * @package admin
 */
require_once('page-admin.php');
$msg = 0;
if (isset($_GET['action']) == true && $sysUser->checkPower('USER') == true) {
    if ($_GET['action'] == 'add' && isset($_POST['group']) == true && isset($_POST['username']) == true && isset($_POST['password']) == true && isset($_POST['password2']) == true && isset($_POST['name']) == true && isset($_POST['email']) == true) {
        if ($_POST['password'] === $_POST['password2']) {
            $group = (int) $_POST['group'];
            $res = $sysUser->addUser($group, $_POST['username'], $_POST['password'], $_POST['name'], $_POST['email']);
            $msg = $res == true ? 1 : 2;
        }
    } elseif ($_GET['action'] == 'edit' && isset($_GET['id']) == true && isset($_POST['username']) == true && isset($_POST['password']) == true && isset($_POST['password2']) == true && isset($_POST['name']) == true && isset($_POST['email']) == true && $_GET['id'] != VISITOR_USER) {
        if ($_POST['password'] === $_POST['password2']) {
            $id = (int) $_GET['id'];
            $res = $sysUser->editUser($id, $_POST['username'], $_POST['password'], $_POST['name'], $_POST['email']);
            $msg = $res == true ? 3 : 4;
        }
    } elseif ($_GET['action'] == 'delete' && isset($_GET['id']) == true && (int) $_GET['id'] != VISITOR_USER) {
        if ($_GET['id'] != $userRes['id']) {
            $res = $sysUser->delUser($_GET['id']);
            $msg = $res == true ? 5 : 6;
        }
    }
}
$coreFeedback->output('url', 'admin-user.php?active=3&msg=' . $msg);
?>
