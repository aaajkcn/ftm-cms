<?php

/**
 * 订单处理器
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require_once('page-admin.php');
$msg = 0;
if (isset($_GET['action']) == true && $sysUser->checkPower('ORDER') == true) {
    //引用并声明product对象
    require(DIR_LIB . DS . 'sys-product.php');
    $sysProduct = new SysProduct($coreDB, $sysLog);
    //引用并声明order对象
    require(DIR_LIB . DS . 'sys-order.php');
    $sysOrder = new SysOrder($coreDB, $sysLog, $coreSession, $sysProduct);
    
    if ($_GET['action'] == 'check' && isset($_GET['id']) == true && isset($_GET['pass']) == true && $sysUser->checkPower('ORDER-CHECK') == true) {
        $id = (int) $_GET['id'];
        $status = $_GET['pass'] ? $sysOrder->status[1] : $sysOrder->status[2];
        $res = $sysOrder->changeStatus($id, $status);
        $msg = $res == true ? 1 : 2;
    } elseif ($_GET['action'] == 'delete' && isset($_GET['id']) == true) {
        $id = (int) $_GET['id'];
        $res = $sysOrder->del($id);
        $msg = $res == true ? 3 : 4;
    }
}
$coreFeedback->output('url', 'admin-order.php?active=2&msg=' . $msg);
?>
