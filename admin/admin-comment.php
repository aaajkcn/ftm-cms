<?php

/**
 * 管理评论页面
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require('page-admin-page.php');

//判断权限
if(COMMENT_ON == true && $sysUser->checkPower('COMMENT') == false){
    CoreHeader::toURL($errorPagePower);
}

//页码生成器
require(DIR_LIB . DS . 'page-pagination.php');

//当前动作
$action = isset($_GET['action']) == true ? $_GET['action'] : 'check';

//引入并声明comment操作对象
require(DIR_LIB . DS . 'sys-comment.php');
$sysComment = new SysComment($coreDB, $sysLog);

//引用字符串截取插件
require(DIR_LIB . DS . 'plug-substrutf8.php');
//$sysComment->add(47, 0, '192.167.2.3', '拉拉', 'meial@a.com', 'wh.com', '评论测试内容1超长内容测试……………………………………………………………………');

//获取列表
$post = isset($_GET['post']) == true ? (int) $_GET['post'] : 0;
$status = isset($_GET['status']) == true && in_array($_GET['status'], $sysComment->status) == true ? $_GET['status'] : $sysComment->status[0];
$where = '';
$attrs = null;
if ($post > 0) {
    $where = '`ct_status` = \'' . $status . '\' and `post_id` = :post';
    $attrs = array(':post' => array($post, PDO::PARAM_INT));
} else {
    $where = '`ct_status` = \'' . $status . '\'';
}
$page = isset($_GET['page']) == true ? (int) $_GET['page'] : 1;
$max = 10;
$sort = isset($_GET['sort']) == true ? (int) $_GET['sort'] : 3;
$desc = isset($_GET['desc']) == true ? true : false;
$tableList = $sysComment->getList($where, $attrs, $page, $max, $sort, $desc);
$tableListRow = $sysComment->getListRow($where, $attrs);

//获取当前焦点
$commentRes = null;
if(isset($_GET['id']) == true){
    $id = (int) $_GET['id'];
    $commentRes = $sysComment->get($id);
}

//如果是审核模式
if ($action == 'check') {
    $commentRes = null;
    $where = '`ct_status` = \'' . $sysComment->status[0] . '\'';
    $tableList = $sysComment->getList($where, null, 1, 1, 0, false);
    if ($tableList) {
        $commentRes = $tableList[0];
    }
}

//提示信息
$msgArr = array('无法执行操作，请重试。', '标记成功！', '标记失败，请稍后重试。', '删除成功！', '无法删除，请稍后重试。');
$msgTypeArr = array(array('错误！', 'error'), array('成功！', 'success'), array('错误！', 'info'), array('成功！', 'success'), array('错误！', 'info'));

require('page-admin-top.php');
?>
<div class="row">&nbsp;</div>
<div class="row">
    <div class="span12">
        <a href="admin-comment.php?active=2&action=check&post=0&status=<?php echo $sysComment->status[0]; ?>" target="_self" class="btn btn-primary"><i class="icon-th-list icon-white"></i> 审核评论</a>
        <a href="admin-comment.php?active=2&action=list&post=0&status=<?php echo $sysComment->status[0]; ?>" target="_self" class="btn btn-info"><i class="icon-tasks icon-white"></i> 查看未审核评论</a>
        <a href="admin-comment.php?active=2&action=list&post=0&status=<?php echo $sysComment->status[1]; ?>" target="_self" class="btn btn-info"><i class="icon-thumbs-up icon-white"></i> 查看已通过评论</a>
        <a href="admin-comment.php?active=2&action=list&post=0&status=<?php echo $sysComment->status[2]; ?>" target="_self" class="btn btn-warning"><i class="icon-thumbs-down icon-white"></i> 查看垃圾评论</a>
    </div>
</div>
<div class="row">&nbsp;</div>

<?php if($action == 'list'){ ?>
<h2>评论列表</h2>
<hr>
<?php if($tableList){ ?>
<table class="table table-hover table-list">
    <thead>
        <tr>
            <th>创建时间</th>
            <th>IP</th>
            <th>作者</th>
            <th>内容</th>
            <th>操作</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($tableList as $k=>$v){ ?>
        <tr>
            <td><?php echo $v['ct_date']; ?></td>
            <td><?php echo $v['ct_ip']; ?></td>
            <td><?php echo $v['ct_name']; ?></td>
            <td><?php echo PlugSubstrUTF8($v['ct_content'], 25); ?></td>
            <td>
                <a href="admin-comment.php?active=2&action=view&id=<?php echo $v['id']; ?>" class="btn btn-info"><i class="icon-search icon-white"></i> 查看</a>
                <?php if($status != $sysComment->status[1]){ ?>
                <a href="action-comment.php?active=2&action=check&id=<?php echo $v['id']; ?>&pass=1" class="btn btn-primary"><i class="icon-ok icon-white"></i> 通过审核</a> 
                <?php } ?>
                <?php if($status != $sysComment->status[2]){ ?>
                <a href="action-comment.php?active=2&action=check&id=<?php echo $v['id']; ?>&pass=0" class="btn btn-inverse"><i class="icon-trash icon-white"></i> 垃圾评论</a> 
                <?php } ?>
                <?php if($status != $sysComment->status[0]){ ?>
                <a href="action-comment.php?action=delete&id=<?php echo $v['id']; ?>" class="btn btn-warning"><i class="icon-trash icon-white"></i> 删除</a></td>
                <?php } ?>
        </tr>
        <?php } ?>
    </tbody>
</table>
<div class="text-center">
    <p><?php echo PagePaginationQuick('admin-comment.php?active=2&action=list&status='.$status.'&page=', $page, $tableListRow); ?></p>
</div>
<?php }else{ ?>
<div class="text-center">
    <p>&nbsp;</p>
    <p>没有任何评论。</p>
</div>
<?php } ?>
<?php } ?>

<?php if(($action == 'view' || $action == 'check') && $commentRes){?>
<h2>查看评论</h2>
<hr>
<div class="hero-unit">
    <div class="page-header">
        <h3><?php echo $commentRes['ct_name']; ?><small>&nbsp;&nbsp;&nbsp;<?php echo $commentRes['ct_date']; ?> - <a href="<?php echo $commentRes['ct_email']; ?>" target="_blank"><?php echo $commentRes['ct_email']; ?></a> - <a href="<?php echo $commentRes['ct_url']; ?>" target="_blank"><?php echo $commentRes['ct_url']; ?></a></small></h3>
    </div>
    <?php echo $commentRes['ct_content']; ?>
    <p>&nbsp;</p>
    <p>
        <a href="action-comment.php?active=2&action=check&id=<?php echo $commentRes['id']; ?>&pass=1" class="btn btn-primary"><i class="icon-ok icon-white"></i> 通过审核</a> 
        <a href="action-comment.php?active=2&action=check&id=<?php echo $commentRes['id']; ?>&pass=0" class="btn btn-inverse"><i class="icon-trash icon-white"></i> 标记为垃圾评论</a> 
        <a href="action-comment.php?active=2&action=delete&id=<?php echo $commentRes['id']; ?>" class="btn btn-warning"><i class="icon-trash icon-white"></i> 删除该评论</a> 
    </p>
</div>
<?php } ?>
<?php if($action == 'check' && !$commentRes){ ?>
<p>没有需要审核的评论。</p>
<?php } ?>

<?php require('page-admin-footer.php'); ?>
