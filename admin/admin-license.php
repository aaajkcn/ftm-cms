<?php

/**
 * 许可证页面
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require('page-admin-page.php');
?>
<?php require('page-admin-top.php'); ?>
<?php require(DIR_LIB . DS . 'static' . DS . 'license.html'); ?>
<?php require('page-admin-footer.php'); ?>

