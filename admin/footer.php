<?php
/**
 * 页脚
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
?>
        <div id="footer" class="navbar navbar-inverse navbar-fixed-bottom">
            <div class="container">
                <p>&copy; 2013 DiNing Biosolutions Company , <a href="admin-license.php?active=3">Business License</a> .
                Code & Desgin by <a href="http://fotomxq.me">Liuzilu</a> . &nbsp;&nbsp;&nbsp;</p>
            </div>
        </div>