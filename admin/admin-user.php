<?php
/**
 * 用户管理
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require('page-admin-page.php');

//判断权限
if($sysUser->checkPower('USER') == false){
    CoreHeader::toURL($errorPagePower);
}

require(DIR_LIB . DS . 'page-pagination.php');

//当前动作
$action = isset($_GET['action']) == true ? $_GET['action'] : 'add';

//获取用户列表
$page = isset($_GET['page']) == true ? (int) $_GET['page'] : 1;
$where = '1';
$attrs = null;
if ($action == 'group' && isset($_GET['group']) == true) {
    $group = (int) $_GET['group'];
    $where = '`user_group` = :group';
    $attrs = array(':group' => array($group, PDO::PARAM_INT));
}
$userList = $sysUser->getUserList($where, $attrs, $page);

//记录数
$userListRow = $sysUser->getUserListRow($where, $attrs);

//提示信息
$msgArr = array('无法执行操作，请重试。', '添加成功！', '添加失败，请检查相关信息是否正确。', '修改成功！', '修改失败，请检查相关信息是否正确。', '删除成功！', '无法删除该用户，请稍后重试。');
$msgTypeArr = array(array('错误！', 'error'), array('成功！', 'success'), array('错误！', 'info'), array('成功！', 'success'), array('错误！', 'info'), array('成功！', 'success'), array('错误！', 'info'));

//获取用户组
$userGroupList = $sysUser->getGroupList('1', null, 1, 9999);

//获取编辑用户信息
$editRes;
if($action == 'edit' && isset($_GET['id']) == true){
    $id = (int) $_GET['id'];
    $editRes = $sysUser->getUser($id);
}

require('page-admin-top.php');
?>
<p>&nbsp;</p>
<p>注意，访客用户不能删除或修改，同时您无法删除您当前的用户。</p>
<h2>用户管理</h2>
<table class="table table-hover table-list">
    <thead>
        <tr>
            <th>用户名</th>
            <th>昵称</th>
            <th>邮箱</th>
            <th>用户组</th>
            <th>操作</th>
        </tr>
    </thead>
    <tbody>
        <?php if($userList){ foreach($userList as $v){ $vGroup = $sysUser->getGroup((int) $v['user_group']); ?>
        <tr>
            <td><?php echo $v['user_username']; ?></td>
            <td><?php echo $v['user_name']; ?></td>
            <td><?php echo $v['user_email']; ?></td>
            <td><a href="admin-user.php?active=3&action=edit&action=group&group=<?php echo $vGroup['id']; ?>"><?php echo $vGroup['group_name']; ?></a></td>
            <td><a href="admin-user.php?active=3&action=edit&id=<?php echo $v['id']; ?>" class="btn"><i class="icon-pencil icon-white"></i> 修改</a> <a href="action-user.php?action=delete&id=<?php echo $v['id']; ?>" class="btn btn-warning"><i class="icon-trash icon-white"></i> 删除</a></td>
        </tr>
        <?php } } ?>
    </tbody>
</table>
<div class="text-center">
    <p><?php echo PagePaginationQuick('admin-user.php?active=3&page=', $page, $userListRow); ?></p>
</div>
<?php if($action != 'edit'){ ?>
<h2>添加用户</h2>
<hr>
<form class="form-horizontal" action="action-user.php?action=add" method="post">
    <div class="control-group">
        <label class="control-label" for="inputGroup">用户组</label>
        <div class="controls">
            <select name="group">
                <?php if($userGroupList){ foreach($userGroupList as $v){ ?>
                <option value="<?php echo $v['id']; ?>"><?php echo $v['group_name']; ?></option>
                <?php } } ?>
            </select>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputUsername">用户名</label>
        <div class="controls">
            <input type="text" name="username" id="inputUsername" placeholder="登录用户名">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputPassword">密码</label>
        <div class="controls">
            <input type="password" name="password" id="inputPassword" placeholder="登录密码">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputPassword2">密码重复</label>
        <div class="controls">
            <input type="password" name="password2" id="inputPassword2" placeholder="登录密码重复">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputName">昵称</label>
        <div class="controls">
            <input type="text" name="name" id="inputName" placeholder="昵称">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">邮箱</label>
        <div class="controls">
            <input type="text" name="email" id="inputEmail" placeholder="邮箱@邮箱.com">
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> 添加</button>
        </div>
    </div>
</form>
<?php } ?>
<?php if($action == 'edit' && $editRes){ ?>
<h2>修改用户</h2>
<hr>
<form class="form-horizontal" action="action-user.php?action=edit&id=<?php echo $editRes['id']; ?>" method="post">
    <div class="control-group">
        <label class="control-label" for="inputUsername">用户名</label>
        <div class="controls">
            <input type="text" name="username" id="inputUsername" placeholder="登录用户名 (必填)" value="<?php echo $editRes['user_username']; ?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputPassword">密码</label>
        <div class="controls">
            <input type="password" name="password" id="inputPassword" placeholder="登录密码 (不修改则留空)">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputPassword2">密码重复</label>
        <div class="controls">
            <input type="password" name="password2" id="inputPassword2" placeholder="登录密码重复 (不修改则留空)">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputName">昵称</label>
        <div class="controls">
            <input type="text" name="name" id="inputName" placeholder="昵称 (必填)" value="<?php echo $editRes['user_name']; ?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">邮箱</label>
        <div class="controls">
            <input type="text" name="email" id="inputEmail" placeholder="邮箱@邮箱.com (必填)" value="<?php echo $editRes['user_email']; ?>">
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> 修改</button>
        </div>
    </div>
</form>
<?php } ?>
<?php require('page-admin-footer.php'); ?>