<?php

/**
 * 信息管理
 * @author fotomxq <fotomxq.me>
 * @version 4
 * @package admin
 */
require('page-admin-page.php');

//判断权限
if ($sysUser->checkPower('NEWS') == false || $sysUser->checkPower('PRO') == false) {
    CoreHeader::toURL($errorPagePower);
}

//当前页面标识
$pageName = 'text';
$pageType = 'text';

//页码生成器
require(DIR_LIB . DS . 'page-pagination.php');

//当前动作
$action = isset($_GET['action']) == true ? $_GET['action'] : 'add';

//引用并声明post对象
require(DIR_LIB . DS . 'sys-post.php');
$sysPost = new SysPost($coreDB, $sysLog);

//引入并声明product操作对象
require(DIR_LIB . DS . 'sys-product.php');
$sysProduct = new SysProduct($coreDB, $sysLog);

//引入并声明comment操作对象
require(DIR_LIB . DS . 'sys-comment.php');
$sysComment = new SysComment($coreDB, $sysLog);

//获取列表
$parent = isset($_GET['parent']) == true ? (int) $_GET['parent'] : 0;
$sortRes = null;
if ($parent > 0) {
    $sortRes = $sysPost->get($parent, null);
    if (!$sortRes) {
        CoreHeader::toURL('error.php?e=un');
    }
}
$where = '';
$attrs = null;
if ($parent > 0) {
    $where = '`post_status` = \'public\' and `post_type` = \'' . $sysPost->type[2] . '\' and `post_parent` = :parent';
    $attrs = array(':parent' => array($parent, PDO::PARAM_INT));
} else {
    $where = '`post_status` = \'public\' and `post_type` = \'' . $sysPost->type[2] . '\'';
}
$page = isset($_GET['page']) == true ? (int) $_GET['page'] : 1;
$max = 10;
$sort = isset($_GET['sort']) == true ? (int) $_GET['sort'] : 3;
$desc = isset($_GET['desc']) == true ? true : false;
$tableList = $sysPost->getList($where, $attrs, $page, $max, $sort, $desc);
$tableListRow = $sysPost->getListRow($where, $attrs);

//获取所有分类
$sortWhere = '`post_status` = \'' . $sysPost->status[0] . '\' and `post_type` = \'' . $sysPost->type[0] . '\'';
$sortAttrs = null;
$sortList = $sysPost->getList($sortWhere, $sortAttrs, 1, 9999, 3, false);

//获取当前焦点
$postRes = null;
if (isset($_GET['id']) == true) {
    $id = (int) $_GET['id'];
    $postRes = $sysPost->get($id, null);
    if ($postRes) {
        $postRes['parent'] = $sysPost->get($postRes['post_parent'], null);
    }
}

//提示信息
$msgArr = array('无法执行操作，请重试。', '添加成功！', '添加失败，请检查相关信息是否正确。', '修改成功！', '修改失败，请检查相关信息是否正确。', '删除成功！', '无法删除，请稍后重试。', '修改排序成功！', '无法修改位置，请稍后重试。');
$msgTypeArr = array(array('错误！', 'error'), array('成功！', 'success'), array('错误！', 'info'), array('成功！', 'success'), array('错误！', 'info'), array('成功！', 'success'), array('错误！', 'info'), array('成功！', 'success'), array('错误！', 'info'));

//包含文件
$includePage['css'][] = 'admin-infos';
$includePage['css'][] = 'font-awesome';
$includePage['js'][] = 'jquery.hotkeys';
$includePage['js'][] = 'bootstrap-wysiwyg';
$includePage['js'][] = 'admin-infos';

require('page-admin-top.php');
?>
<?php if($sortList){ ?>
<div class="row">&nbsp;</div>
<div class="row">
    <div class="span2">
        <a href="admin-infos.php?active=2&parent=<?php echo $parent; ?>&action=add" target="_self" class="btn btn-primary">添加文章</a>
    </div>
    <div class="span6">
        <form action="admin-infos.php" method="get" class="text-left">
            <select name="parent" id="inputParent">
                <option value="0">查看所有分类</option>
                <?php foreach($sortList as $v){ ?>
                <option value="<?php echo $v['id']; ?>"<?php if($v['id'] == $parent) echo ' selected="selected"'; ?>><?php echo $v['post_title']; ?></option>
                <?php } ?>
            </select>
            <button type="submit" class="btn btn-info"><i class="icon-chevron-right icon-white"></i> 筛选</button>
            <input type="text" name="active" value="2" class="hidden">
        </form>
    </div>
</div>

<h2>文章列表</h2>
<hr>
<?php if($tableList){ ?>
<table class="table table-hover table-list">
    <thead>
        <tr>
            <th><a href="admin-infos.php?active=2&sort=2<?php if($desc==false) echo '&desc=1'; ?>" target="_self">标题</a></th>
            <th><a href="admin-infos.php?active=2&sort=5<?php if($desc==false) echo '&desc=1'; ?>" target="_self">创建时间</a></th>
            <th><a href="admin-infos.php?active=2&sort=12<?php if($desc==false) echo '&desc=1'; ?>" target="_self">查看次数</a></th>
            <th>评论</th>
            <th>操作</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($tableList as $k=>$v){ $commentNumRes = $sysComment->getListRow('`post_id` = :id', array(':id'=>array($v['id'],PDO::PARAM_INT))); ?>
        <tr>
            <td><a href="admin-infos.php?active=2&action=view&parent=<?php echo $parent; ?>&id=<?php echo $v['id']; ?>" target="_self"><?php echo $v['post_title']; ?></a></td>
            <td><?php echo $v['post_date']; ?></td>
            <td><?php echo $v['post_view']; ?></td>
            <td><a href="admin-comment.php?active=2&action=view-post&post=<?php echo $v['id']; ?>" target="_self"><?php echo $commentNumRes; ?></a></td>
            <td>
                <?php if($k > 0){ ?><a href="action-infos.php?action=sort&id=<?php echo $v['id']; ?>&dest=<?php echo $tableList[$k-1]['id']; ?>" class="btn btn-info"><i class="icon-chevron-up icon-white"></i></a> <?php }else{ ?><a href="#" class="btn"><i class="icon-chevron-up icon-white"></i></a><?php } ?>
                <?php if(isset($tableList[$k+1]) == true){ ?><a href="action-infos.php?action=sort&id=<?php echo $v['id']; ?>&dest=<?php echo $tableList[$k+1]['id']; ?>" class="btn btn-info"><i class="icon-chevron-down icon-white"></i></a> <?php }else{ ?><a href="#" class="btn"><i class="icon-chevron-down icon-white"></i></a><?php } ?>
                <a href="admin-infos.php?active=2&action=view&parent=<?php echo $parent; ?>&id=<?php echo $v['id']; ?>" class="btn btn-info"><i class="icon-search icon-white"></i> 查看</a>
                <a href="admin-infos.php?active=2&action=edit&parent=<?php echo $parent; ?>&id=<?php echo $v['id']; ?>" class="btn btn-info"><i class="icon-pencil icon-white"></i> 修改</a> 
                <a href="action-infos.php?action=delete&id=<?php echo $v['id']; ?>" class="btn btn-warning"><i class="icon-trash icon-white"></i> 删除</a></td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<div class="text-center">
    <p><?php echo PagePaginationQuick('admin-infos.php?active=2&parent='.$parent.'&page=', $page, $tableListRow); ?></p>
</div>
<?php }else{ ?>
<div class="text-center">
    <p>&nbsp;</p>
    <p>没有任何文章。</p>
</div>
<?php } ?>

<?php if($action == 'view' && $postRes){ $productList = $sysProduct->getALL($postRes['id']); $commentNumRes = $sysComment->getListRow('`post_id` = :id', array(':id'=>array($postRes['id'],PDO::PARAM_INT))); ?>
<h2>查看</h2>
<hr>
<div class="hero-unit">
    <div class="page-header">
        <h3><?php echo $postRes['post_title']; ?><small>&nbsp;&nbsp;&nbsp;[ <?php if($postRes['parent']) echo $postRes['parent']['post_title']; ?> ] - <?php echo $postRes['post_date']; ?> - 查看 <?php echo $postRes['post_view']; ?> 次 - 评论 <?php echo $commentNumRes; ?> 个</small></h3>
    </div>
    <?php if($productList){ ?>
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>名称</th>
                <th>描述</th>
                <th>价格 ( 人民币 )</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($productList as $v){ ?>
            <tr>
                <td><?php echo $v['pt_name']; ?></td>
                <td><?php echo $v['pt_description']; ?></td>
                <td><?php echo $v['pt_price']; ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
<?php } ?>
    <?php echo $postRes['post_content']; ?>
</div>
<?php } ?>

<?php if($action == 'add'){ ?>
<h2>添加</h2>
<hr>
<form id="form-content" class="form-horizontal" action="action-infos.php?action=add&parent=<?php echo $parent; ?>" method="post">
    <div class="control-group">
        <label class="control-label" for="inputTitle">标题</label>
        <div class="controls">
            <input type="text" name="title" id="inputTitle" placeholder="标题" value="" class="input-xxlarge">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputParent">分类</label>
        <div class="controls">
            <select name="parent" id="inputParent">
                <?php if($sortList){ foreach($sortList as $v){ ?>
                <option value="<?php echo $v['id']; ?>"<?php if($v['id'] == $parent) echo ' selected="selected"'; ?>><?php echo $v['post_title']; ?></option>
                <?php } } ?>
            </select>
        </div>
    </div>
    <div id="product-list" class="hide"></div>
    <div class="controls">
        <div id="alerts"></div>
        <div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
            <div class="btn-group">
                <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" id="content-font" title="字体"><i class="icon-font"></i><b class="caret"></b></a>
                <ul class="dropdown-menu">
                </ul>
            </div>
            <div class="btn-group">
                <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" title="字体大小"><i class="icon-text-height"></i>&nbsp;<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a data-edit="fontSize 5"><font size="5">大</font></a></li>
                    <li><a data-edit="fontSize 3"><font size="3">正常</font></a></li>
                    <li><a data-edit="fontSize 1"><font size="1">小</font></a></li>
                </ul>
            </div>
            <div class="btn-group">
                <a class="btn btn-small" data-edit="bold" title="加粗 (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
                <a class="btn btn-small" data-edit="italic" title="斜体 (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
                <a class="btn btn-small" data-edit="strikethrough" title="删除线"><i class="icon-strikethrough"></i></a>
                <a class="btn btn-small" data-edit="underline" title="下划线 (Ctrl/Cmd+U)"><i class="icon-underline"></i></a>
            </div>
            <div class="btn-group">
                <a class="btn btn-small" data-edit="insertunorderedlist" title="符号编号"><i class="icon-list-ul"></i></a>
                <a class="btn btn-small" data-edit="insertorderedlist" title="数字编号"><i class="icon-list-ol"></i></a>
                <a class="btn btn-small" data-edit="outdent" title="左缩进 (Shift+Tab)"><i class="icon-indent-left"></i></a>
                <a class="btn btn-small" data-edit="indent" title="右缩进 (Tab)"><i class="icon-indent-right"></i></a>
            </div>
            <div class="btn-group">
                <a class="btn btn-small" data-edit="justifyleft" title="左对齐 (Ctrl/Cmd+L)"><i class="icon-align-left"></i></a>
                <a class="btn btn-small" data-edit="justifycenter" title="居中 (Ctrl/Cmd+E)"><i class="icon-align-center"></i></a>
                <a class="btn btn-small" data-edit="justifyright" title="右对齐 (Ctrl/Cmd+R)"><i class="icon-align-right"></i></a>
                <a class="btn btn-small" data-edit="justifyfull" title="两端对齐 (Ctrl/Cmd+J)"><i class="icon-align-justify"></i></a>
            </div>
            <div class="btn-group">
                <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" title="添加链接"><i class="icon-link"></i></a>
                <ul class="dropdown-menu">
                    <li class="input-append">
                        <input class="span2" placeholder="URL" type="text" data-edit="createLink"/>
                        <button class="btn" type="button">添加</button>
                    </li>
                </ul>
                <a class="btn btn-small" data-edit="unlink" title="删除链接"><i class="icon-cut"></i></a>
            </div>
            <div class="btn-group">
                <a class="btn btn-small" title="插入图片" id="pictureBtn"><i class="icon-picture"></i></a>
                <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
            </div>
            <div class="btn-group">
                <a class="btn btn-small" data-edit="undo" title="撤销 (Ctrl/Cmd+Z)"><i class="icon-undo"></i></a>
                <a class="btn btn-small" data-edit="redo" title="还原 (Ctrl/Cmd+Y)"><i class="icon-repeat"></i></a>
            </div>
        </div>
        <div id="editor"></div>
    </div>
    <textarea class="textarea span9 hide" name="content" id="inputContent"></textarea>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> 发表</button>
            <a href="#show-product" class="btn btn-info"><i class="icon-tasks icon-white"></i> 添加产品</a>
        </div>
    </div>
</form>
<?php } ?>

<?php if($action == 'edit' && $postRes){ $productList = $sysProduct->getALL($postRes['id']); ?>
<h2>修改</h2>
<hr>
<form id="form-content" class="form-horizontal" action="action-infos.php?action=edit&id=<?php echo $postRes['id']; ?>&parent=<?php echo $parent; ?>" method="post">
    <div class="control-group">
        <label class="control-label" for="inputTitle">标题</label>
        <div class="controls">
            <input type="text" name="title" id="inputTitle" placeholder="标题" class="input-xxlarge" value="<?php echo $postRes['post_title']; ?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputParent">分类</label>
        <div class="controls">
            <select name="parent" id="inputParent">
                <?php if($sortList){ foreach($sortList as $v){ ?>
                <option value="<?php echo $v['id']; ?>"<?php if($v['id'] == $postRes['post_parent']) echo ' selected="selected"'; ?>><?php echo $v['post_title']; ?></option>
                <?php } } ?>
            </select>
        </div>
    </div>
    <div id="product-list">
        <?php if($productList){ foreach($productList as $v){ ?>
        <div class="control-group product-list">
            <label class="control-label">产品</label>
            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-barcode"></i></span>
                    <input type="text" name="proName[]" placeholder="名称" class="input-medium" value="<?php echo $v['pt_name']; ?>">
                </div>
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-tag"></i></span>
                    <input type="text" name="proDescription[]" placeholder="描述" class="input-large" value="<?php echo $v['pt_description']; ?>">
                </div>
                <div class="input-prepend">
                    <span class="add-on">$</span>
                    <input type="text" name="proPrice[]" placeholder="价格" class="input-small" value="<?php echo $v['pt_price']; ?>">
                </div>
                <a href="#delete-product" class="btn"><i class="icon-remove"></i> </a>
            </div>
        </div>
        <?php } } ?>
    </div>
    <div class="controls">
        <div id="alerts"></div>
        <div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
            <div class="btn-group">
                <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" id="content-font" title="字体"><i class="icon-font"></i><b class="caret"></b></a>
                <ul class="dropdown-menu">
                </ul>
            </div>
            <div class="btn-group">
                <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" title="字体大小"><i class="icon-text-height"></i>&nbsp;<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a data-edit="fontSize 5"><font size="5">大</font></a></li>
                    <li><a data-edit="fontSize 3"><font size="3">正常</font></a></li>
                    <li><a data-edit="fontSize 1"><font size="1">小</font></a></li>
                </ul>
            </div>
            <div class="btn-group">
                <a class="btn btn-small" data-edit="bold" title="加粗 (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
                <a class="btn btn-small" data-edit="italic" title="斜体 (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
                <a class="btn btn-small" data-edit="strikethrough" title="删除线"><i class="icon-strikethrough"></i></a>
                <a class="btn btn-small" data-edit="underline" title="下划线 (Ctrl/Cmd+U)"><i class="icon-underline"></i></a>
            </div>
            <div class="btn-group">
                <a class="btn btn-small" data-edit="insertunorderedlist" title="符号编号"><i class="icon-list-ul"></i></a>
                <a class="btn btn-small" data-edit="insertorderedlist" title="数字编号"><i class="icon-list-ol"></i></a>
                <a class="btn btn-small" data-edit="outdent" title="左缩进 (Shift+Tab)"><i class="icon-indent-left"></i></a>
                <a class="btn btn-small" data-edit="indent" title="右缩进 (Tab)"><i class="icon-indent-right"></i></a>
            </div>
            <div class="btn-group">
                <a class="btn btn-small" data-edit="justifyleft" title="左对齐 (Ctrl/Cmd+L)"><i class="icon-align-left"></i></a>
                <a class="btn btn-small" data-edit="justifycenter" title="居中 (Ctrl/Cmd+E)"><i class="icon-align-center"></i></a>
                <a class="btn btn-small" data-edit="justifyright" title="右对齐 (Ctrl/Cmd+R)"><i class="icon-align-right"></i></a>
                <a class="btn btn-small" data-edit="justifyfull" title="两端对齐 (Ctrl/Cmd+J)"><i class="icon-align-justify"></i></a>
            </div>
            <div class="btn-group">
                <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" title="添加链接"><i class="icon-link"></i></a>
                <ul class="dropdown-menu">
                    <li class="input-append">
                        <input class="span2" placeholder="URL" type="text" data-edit="createLink"/>
                        <button class="btn" type="button">添加</button>
                    </li>
                </ul>
                <a class="btn btn-small" data-edit="unlink" title="删除链接"><i class="icon-cut"></i></a>
            </div>
            <div class="btn-group">
                <a class="btn btn-small" title="插入图片" id="pictureBtn"><i class="icon-picture"></i></a>
                <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
            </div>
            <div class="btn-group">
                <a class="btn btn-small" data-edit="undo" title="撤销 (Ctrl/Cmd+Z)"><i class="icon-undo"></i></a>
                <a class="btn btn-small" data-edit="redo" title="还原 (Ctrl/Cmd+Y)"><i class="icon-repeat"></i></a>
            </div>
        </div>
        <div id="editor">
            <?php echo $postRes['post_content']; ?>
        </div>
    </div>
    <textarea class="textarea span9 hide" name="content" id="inputContent"><?php echo $postRes['post_content']; ?></textarea>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> 保存修改</button>
            <a href="#show-product" class="btn btn-info"><i class="icon-tasks icon-white"></i> 添加产品</a>
        </div>
    </div>
</form>
<?php } ?>
<?php }else{ ?>
<p>&nbsp;</p>
<div class="text-center">
    <p>没有任何分类，请先<a href="admin-sort.php?active=2" target="_self">进入分类管理界面</a>，添加分类。</p>
</div>
<?php } ?>
<div id="product-list-template" class="hide">
    <div class="control-group product-list">
        <label class="control-label">产品</label>
        <div class="controls">
            <div class="input-prepend">
                <span class="add-on"><i class="icon-barcode"></i></span>
                <input type="text" name="proName[]" placeholder="名称" class="input-medium">
            </div>
            <div class="input-prepend">
                <span class="add-on"><i class="icon-tag"></i></span>
                <input type="text" name="proDescription[]" placeholder="描述" class="input-large">
            </div>
            <div class="input-prepend">
                <span class="add-on">$</span>
                <input type="text" name="proPrice[]" placeholder="价格" class="input-small">
            </div>
            <a href="#delete-product" class="btn"><i class="icon-remove"></i> </a>
        </div>
    </div>
</div>

<?php require('page-admin-footer.php'); ?>
