<?php
/**
 * 登录页面
 * @author fotomxq <fotomxq.me>
 * @version 3
 * @package admin
 */
//引入包
require('page.php');

//引入User
require(DIR_LIB . DS . 'sys-user.php');
$sysUser = new SysUser($coreDB, $coreSession, $sysLog, $coreIP->ip);

//判断是否已登陆
if ($sysUser->logged()) {
    CoreHeader::toURL('admin-center.php');
} else {
    $sysUser->logout();
}

//设定自动输入
$inputUserName = '';
$inputPassWord = '';

//如果为debug
if(DEBUG_ON == true){
    //自动输入登录信息
    $inputUserName = 'admin';
    $inputPassWord = 'adminadmin';
}

//清理session
$coreSession->clear();

//访客记录
require(DIR_LIB . DS . 'sys-views.php');
$sysViews = new SysViews($coreDB, $sysLog);
$sysViews->add($coreIP->ip);

//消息
$msgArr = array('验证码填写错误，请注意区分大小写！', '用户名或密码填写错误，请重新输入。', '请先填写相关信息，然后再登录。', '登录超时，请重新登录！');
$msgTypeArr = array(array('错误！', 'error'), array('错误！', 'error'), array('错误！', 'error'), array('超时！', 'error'));
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>登录 - <?php echo $webTitle; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="assets/bootstrap.css" rel="stylesheet">
        <link href="assets/bootstrap-responsive.css" rel="stylesheet">
        <link href="assets/glob.css" rel="stylesheet">
        <style>
            .form-signin {
                max-width: 300px;
                padding: 19px 29px 29px;
                margin: 0 auto 20px;
                background-color: #fff;
                border: 1px solid #e5e5e5;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
            }
            .form-signin .form-signin-heading,
            .form-signin .checkbox {
                margin-bottom: 10px;
            }
            .form-signin-heading img{
                max-width:300px;
                max-height: 159px;
            }
            .form-signin input[type="text"],
            .form-signin input[type="password"] {
                font-size: 16px;
                height: auto;
                margin-bottom: 15px;
                padding: 7px 9px;
            }
            #vcode{
                width:150px;
                height:35px;
                cursor: pointer;
            }
            .vcode-input{
                margin-top: 15px;
            }
        </style>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="assets/html5shiv.js"></script>
        <![endif]-->
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/logo-144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/logo-114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/logo-72.png">
        <link rel="apple-touch-icon-precomposed" href="assets/logo-57.png">
        <link rel="shortcut icon" href="assets/favicon.ico">
    </head>
    <body>
        <div id="wrap">
            <div id="push"></div>
            <div class="container">
                <?php if(isset($_GET['msg']) == true){ if(isset($msgArr[$_GET['msg']]) == true){ ?>
                <div id="msg" class="alert">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>错误!</strong> <?php echo $msgArr[$_GET['msg']]; ?>
                </div>
                <?php } } ?>
                <form class="form-signin" action="login.php" method="post">
                    <h2 class="form-signin-heading"><img src="assets/logo-text.png" /></h2>
                    <input name="user" type="text" class="input-block-level" placeholder="用户名" value="<?php echo $inputUserName; ?>">
                    <input name="passwd" type="password" class="input-block-level" placeholder="密码" value="<?php echo $inputPassWord; ?>">
                    <div class="input-block-level">
                        <img id="vcode" src="verify-code.php?" />
                        <input name="vcode" type="text" class="vcode-input input-small" placeholder="验证码" autocomplete="off">
                    </div>
                    <label class="checkbox">
                        <input name="remember" type="checkbox" value="1"> 记住我
                    </label>
                    <button class="btn btn-large btn-primary" type="submit"><i class="icon-lock icon-white"></i>&nbsp;登录</button>
                </form>
            </div>
            <div id="push"></div>
        </div>
        <?php require('footer.php'); ?>
        <script src="assets/jquery.js"></script>
        <script src="assets/bootstrap.js"></script>
        <script>
            function msgTimeOut(){
                $('#msg').fadeOut();
            }
            $(document).ready(function() {
                $('#vcode').data('url', $('#vcode').attr('src'));
                $('#vcode').click(function() {
                    var random = Math.random();
                    $('#vcode').attr('src', $('#vcode').data('url') + random);
                });
                <?php if(isset($_GET['msg']) == true){ if(isset($msgArr[$_GET['msg']]) == true){ ?>
                    var msgTime = window.setTimeout('msgTimeOut();',5000);
                <?php } } ?>
            });
        </script>
    </body>
</html>