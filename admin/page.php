<?php

/**
 * 页面引用
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require_once('config.php');
//网站名称
$webTitle = $sysConfig->load('WEB_TITLE');
//禁止缓冲页面
CoreHeader::noCache();
//定义包含页面
$includePage = array('css' => null, 'js' => null);
$pageIncludeBool = true;
?>
