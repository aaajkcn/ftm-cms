<?php

/**
 * 显示图片
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require_once('page-admin.php');
require(DIR_LIB . DS . 'sys-post.php');
require(DIR_LIB . DS . 'sys-upload.php');
$sysPost = new SysPost($coreDB, $sysLog);
$sysUpload = new SysUpload($sysPost, $sysLog, DIR_UPLOAD, $userRes['id']);
if (isset($_GET['id']) == true) {
    $id = (int) $_GET['id'];
    $width = isset($_GET['width']) == true ? $_GET['width'] : null;
    $height = isset($_GET['height']) == true ? $_GET['height'] : null;
    $passwd = isset($_GET['passwd']) == true ? $_GET['passwd'] : null;
    $sysUpload->showImage($id, $width, $height, $passwd, UPLOAD_DOWN_PHP);
}
?>
