<?php

/**
 * 修改网站设定
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require_once('page-admin.php');
$msg = 0;
if (isset($_GET['action']) == true && $sysUser->checkPower('BASIC') == true) {
    if ($_GET['action'] == 'set' &&
            isset($_POST['websiteTitle']) == true &&
            isset($_POST['webIndexTextM']) == true &&
            isset($_POST['pageFooter']) == true &&
            isset($_POST['IPBanOn']) == true &&
            isset($_POST['IPBanList']) == true &&
            isset($_POST['IPWhiteOn']) == true &&
            isset($_POST['IPWhiteList']) == true
    ) {
        $inputWebsiteTitle = $_POST['websiteTitle'];
        $inputIndexTextM = $_POST['webIndexTextM'];
        $inputPageFooter = $_POST['pageFooter'];
        $inputIPBanOn = $_POST['IPBanOn'] == '1' ? '1' : '0';
        $inputIPBanList = $_POST['IPBanList'];
        $inputIPWhiteOn = $_POST['IPWhiteOn'] == '1' ? '1' : '0';
        $inputIPWhiteList = $_POST['IPWhiteList'];
        $configSetArr = array(
            'WEB_TITLE' => $inputWebsiteTitle,
            'INDEX_TEXT_M' => $inputIndexTextM,
            'PAGE_FOOTER' => $inputPageFooter,
            'IP_BAN_ON' => $inputIPBanOn,
            'IP_BAN_LIST' => $inputIPBanList,
            'IP_WHITE_ON' => $inputIPWhiteOn,
            'IP_WHITE_LIST' => $inputIPWhiteList
        );
        $res;
        foreach ($configSetArr as $k => $v) {
            $res = $sysConfig->save(array($k => $v));
            if (!$res) {
                break;
            }
        }
        $msg = $res ? 1 : 2;
    } elseif ($_GET['action'] == 'clear-cache') {
        /* 手动删除
        $search = DIR_CACHE . DS . '*';
        $searchList = CoreFile::searchDir($search);
        if ($searchList) {
            foreach ($searchList as $v) {
                if (CoreFile::isFile($v) == true) {
                    CoreFile::deleteFile($v);
                } else {
                    CoreFile::deleteDir($v);
                }
            }
        }
         */
        //使用缓冲器删除
        $coreCache->clear();
        $msg = 5;
    } elseif ($_GET['action'] == 'restore') {
        $res = $sysConfig->restore();
        $msg = $res ? 3 : 4;
    }
}
$coreFeedback->output('url', 'admin-operate-basic.php?active=3&msg=' . $msg);
?>
