<?php

/**
 * 网站基本设定
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require('page-admin-page.php');

//判断权限
if ($sysUser->checkPower('BASIC') == false) {
    CoreHeader::toURL($errorPagePower);
}

//提示信息
$msgArr = array(
    '无法修改设置，请检查输入是否正确？',
    '修改设置成功！',
    '修改失败，请检查相关信息是否正确。',
    '还原成功！',
    '无法还原设置，请稍后重试。',
    '已经强制删除了所有缓冲文件，网站将自动重建相关缓冲信息。在此期间可能有卡顿，这是正常现象。'
);
$msgTypeArr = array(array('错误！', 'error'), array('成功！', 'success'), array('错误！', 'info'), array('成功！', 'success'), array('错误！', 'info'), array('完成！', 'info'));

require('page-admin-top.php');
?>

<div class="row">
    <div class="span2">&nbsp;</div>
    <div class="span10">
        <h2>网站设置</h2>
    </div>
</div>

<div class="row">
    <div class="span2"></div>
    <div class="span10">
        <p>&nbsp;</p>
        <form class="form-horizontal" action="action-operate-basic.php?action=set" method="post">
            <h4 id="basic">基本</h4>
            <hr>
            <div class="control-group">
                <label class="control-label" for="inputWebsiteTitle">网站名称</label>
                <div class="controls">
                    <input type="text" class="input-xxlarge" name="websiteTitle" id="inputWebsiteTitle" placeholder="网站名称" value="<?php echo $webTitle; ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputWebIndexTextM">首页介绍文字</label>
                <div class="controls">
                    <textarea rows="5" name="webIndexTextM" class="span5"><?php echo $sysConfig->load('INDEX_TEXT_M'); ?></textarea>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputFooter">底部版权和备案信息</label>
                <div class="controls">
                    <input type="text" class="input-xxlarge" name="pageFooter" id="inputFooter" placeholder="底部版权和备案信息" value="<?php echo $sysConfig->load('PAGE_FOOTER'); ?>">
                </div>
            </div>
            <h4>IP</h4>
            <hr>
            <div class="control-group">
                <label class="control-label" for="inputIPBanOn">黑名单（禁止访问）</label>
                <div class="controls">
                    <select name="IPBanOn">
                        <option<?php if($ipConfigs['IP_BAN_ON'] == '1') echo ' selected="selected"'; ?> value="1">打开</option>
                        <option<?php if($ipConfigs['IP_BAN_ON'] != '1') echo ' selected="selected"'; ?> value="0">关闭</option>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputIPBanList">黑名单列表</label>
                <div class="controls">
                    <textarea rows="3" name="IPBanList"><?php echo $ipConfigs['IP_BAN_LIST']; ?></textarea>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputIPWhiteOn">白名单（仅允许访问）</label>
                <div class="controls">
                    <select name="IPWhiteOn">
                        <option<?php if($ipConfigs['IP_WHITE_ON'] == '1') echo ' selected="selected"'; ?> value="1">打开</option>
                        <option<?php if($ipConfigs['IP_WHITE_ON'] != '1') echo ' selected="selected"'; ?> value="0">关闭</option>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputIPWhiteList">白名单列表</label>
                <div class="controls">
                    <textarea rows="3" name="IPWhiteList"><?php echo $ipConfigs['IP_WHITE_LIST']; ?></textarea>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> 修改</button>
                    <button type="reset" class="btn"><i class="icon-share-alt icon-white"></i> 重置</button>
                    <a href="action-operate-basic.php?action=clear-cache" class="btn btn-info"><i class="icon-fire icon-white"></i> 强制删除缓冲</a>
                    <a href="action-operate-basic.php?action=restore" class="btn btn-warning"><i class="icon-retweet icon-white"></i> 还原到最初设定</a>
                </div>
            </div>
        </form>
    </div>
</div>
<?php require('page-admin-footer.php'); ?>