<?php

/**
 * 站点统计处理器
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require_once('page-admin.php');
$msg = 0;
if (isset($_GET['action']) == true && $sysUser->checkPower('STATISTICS') == true) {
    //访客记录
    require(DIR_LIB . DS . 'sys-views.php');
    $sysViews = new SysViews($coreDB, $sysLog);
    //分析动作
    if ($_GET['action'] == 'clear') {
        $res = $sysViews->clear();
        $msg = $res == true ? 1 : 0;
    } elseif ($_GET['action'] == 'refresh') {
        $coreCache->clear('STATISTICS-CHART');
        $msg = 2;
    }
}
$coreFeedback->output('url', 'admin-statistics.php?active=3&msg=' . $msg);
?>
