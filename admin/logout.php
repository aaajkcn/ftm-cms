<?php

/**
 * 退出登陆页面
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require('page-post.php');
require(DIR_LIB . DS . 'sys-user.php');
$sysUser = new SysUser($coreDB, $coreSession, $sysLog, $coreIP->ip);
$sysUser->logout();
$coreFeedback->output('url', 'index.php');
?>
