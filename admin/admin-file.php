<?php

/**
 * 文件管理
 * @author fotomxq <fotomxq.me>
 * @version 2
 * @package admin
 */
require('page-admin-page.php');

//判断权限
if($sysUser->checkPower('FILE') == false){
    CoreHeader::toURL($errorPagePower);
}

//当前页面标识
$pageName = 'file';
$pageType = 'file';

//页码生成器
require(DIR_LIB . DS . 'page-pagination.php');

//当前动作
$action = isset($_GET['action']) == true ? $_GET['action'] : 'add';

//引用并声明post对象
require(DIR_LIB . DS . 'sys-post.php');
$sysPost = new SysPost($coreDB, $sysLog);

//获取列表
$where = '`post_status` = \'public\' and `post_type` = \'' . $sysPost->type[1] . '\'';
$attrs = null;
$page = isset($_GET['page']) == true ? (int) $_GET['page'] : 1;
$max = 10;
$sort = isset($_GET['sort']) == true ? (int) $_GET['sort'] : 3;
$desc = isset($_GET['desc']) == true ? false : true;
$tableList = $sysPost->getList($where, $attrs, $page, $max, $sort, $desc);
$tableListRow = $sysPost->getListRow($where, $attrs);

//获取当前焦点
$postRes = null;
if(isset($_GET['id']) == true){
    $id = (int) $_GET['id'];
    $postRes = $sysPost->get($id, null);
}

//提示信息
$msgArr = array('无法执行操作，请重试。', '上传成功！', '上传失败，请稍后重试。', '修改成功！', '修改失败，请检查相关信息是否正确。', '删除成功！', '无法删除，请稍后重试。', '修改排序成功！', '无法修改位置，请稍后重试。');
$msgTypeArr = array(array('错误！', 'error'), array('成功！', 'success'), array('错误！', 'info'), array('成功！', 'success'), array('错误！', 'info'), array('成功！', 'success'), array('错误！', 'info'), array('成功！', 'success'), array('错误！', 'info'));

//包含文件
$includePage['css'][] = 'admin-file';
$includePage['js'][] = 'admin-file';

require('page-admin-top.php');
?>
<?php if($action == 'add'){ ?>
<h2>上传文件</h2>
<form class="form-horizontal" id="edit-form" action="action-upload.php?action=upload&parent=0" method="post" enctype="multipart/form-data">
    <div class="control-group">
        <label class="control-label" for="inputFile"></label>
        <div class="controls">
            <input type="file" name="upload" />
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> 开始上传</button>
        </div>
    </div>
</form>
<hr>
<?php } ?>
<h2>文件列表</h2>
<hr>
<table class="table table-hover admin-file-table">
    <thead>
        <tr>
            <th>预览</th>
            <th>引用地址</th>
            <th><a href="admin-file.php?active=2&sort=3<?php if($desc == true) echo '&desc=1'; ?>" target="_self">上传时间</a></th>
            <th><a href="admin-file.php?active=2&sort=2<?php if($desc == true) echo '&desc=1'; ?>" target="_self">文件名</a></th>
            <th><a href="admin-file.php?active=2&sort=13<?php if($desc == true) echo '&desc=1'; ?>" target="_self">文件类型</a></th>
            <th><a href="admin-file.php?active=2&sort=12<?php if($desc == true) echo '&desc=1'; ?>" target="_self">访问次数</a></th>
            <th>操作</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($tableList as $v){ ?>
        <tr name="table-data-tr">
            <td><?php if($v['post_meta'] == 'image/jpeg' || $v['post_meta'] == 'image/png'){ ?><img data-src="holder.js/150x150" src="../content/uploads/<?php echo $v['post_url']; ?>"><?php } ?></td>
            <td><?php if($v['post_meta'] == 'image/jpeg' || $v['post_meta'] == 'image/png'){ echo 'image.php?id='.$v['id']; }else{ echo 'download.php?id='.$v['id']; } ?></td>
            <td><?php echo $v['post_date']; ?></td>
            <td><?php echo $v['post_title']; ?></td>
            <td><?php echo $v['post_meta']; ?></td>
            <td><?php echo $v['post_view']; ?></td>
            <td>
                <a href="download.php?id=<?php echo $v['id']; ?>" class="btn" target="_blank"><i class="icon-download icon-white"></i> 下载</a> 
                <a href="admin-file.php?active=2&action=edit&id=<?php echo $v['id']; ?>" class="btn btn-primary"><i class="icon-pencil icon-white"></i> 修改</a> 
                <a href="action-upload.php?action=delete&id=<?php echo $v['id']; ?>" class="btn btn-warning"><i class="icon-trash icon-white"></i> 删除</a></td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<div class="text-center">
    <p><?php echo PagePaginationQuick('admin-file.php?active=2&page=', $page, $tableListRow); ?></p>
</div>
<?php if($action == 'edit' && $postRes){ ?>
<h2>修改文件信息</h2>
<hr>
<form class="form-horizontal" id="edit-form" action="action-upload.php?action=edit&id=<?php echo $postRes['id']; ?>" method="post">
    <div class="control-group">
        <label class="control-label" for="inputTitle">文件名称</label>
        <div class="controls">
            <input type="text" name="title" id="inputTitle" placeholder="文件名称" value="<?php echo $postRes['post_title']; ?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputContent">文件描述</label>
        <div class="controls">
            <textarea name="content" rows="3" placeholder="文件描述..."><?php echo $postRes['post_content']; ?></textarea>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> 保存</button>
        </div>
    </div>
</form>
<?php } ?>
<?php require('page-admin-footer.php'); ?>
