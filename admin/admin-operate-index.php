<?php
/**
 * 网站首页设定
 * @author fotomxq <fotomxq.me>
 * @version 2
 * @package admin
 */
require('page-admin-page.php');

//判断权限
if($sysUser->checkPower('BASIC') == false){
    CoreHeader::toURL($errorPagePower);
}

//引用并声明post对象
require(DIR_LIB . DS . 'sys-post.php');
$sysPost = new SysPost($coreDB, $sysLog);

$wherePostMeta = '`post_meta` = \'image/jpeg\' or `post_meta` = \'image/png\'';

//获取首页幻灯片列表
$where = '`post_status` = \'public\' and `post_type` = \'' . $sysPost->type[6] . '\'';
$slideshowList = $sysPost->getList($where, null, 1, 9999, 3, false, true);
if ($slideshowList) {
    foreach ($slideshowList as $k => $v) {
        $where = '`post_status` = \'public\' and `post_type` = \'file\' and (' . $wherePostMeta . ') and `post_parent` = :id';
        $attrs = array(':id' => array($v['id'], PDO::PARAM_INT));
        $image = $sysPost->getList($where, $attrs);
        if ($image) {
            $slideshowList[$k]['image'] = $image[0]['id'];
        } else {
            $slideshowList[$k]['image'] = 0;
        }
    }
}

//获取首页产品列表
$where = '`post_status` = \'public\' and `post_type` = \'' . $sysPost->type[7] . '\'';
$proList = $sysPost->getList($where, null, 1, 9999, 3, false, true);
if ($proList) {
    foreach ($proList as $k => $v) {
        $where = '`post_status` = \'public\' and `post_type` = \'file\' and (' . $wherePostMeta . ') and `post_parent` = :id';
        $attrs = array(':id' => array($v['id'], PDO::PARAM_INT));
        $image = $sysPost->getList($where, $attrs);
        if ($image) {
            $proList[$k]['image'] = $image[0]['id'];
        } else {
            $proList[$k]['image'] = 0;
        }
    }
}

//获取底部合作商列
$where = '`post_status` = \'public\' and `post_type` = \'' . $sysPost->type[8] . '\'';
$pooList = $sysPost->getList($where, null, 1, 9999, 3, false, true);
if ($pooList) {
    foreach ($pooList as $k => $v) {
        $where = '`post_status` = \'public\' and `post_type` = \'file\' and (' . $wherePostMeta . ') and `post_parent` = :id';
        $attrs = array(':id' => array($v['id'], PDO::PARAM_INT));
        $image = $sysPost->getList($where, $attrs);
        if ($image) {
            $pooList[$k]['image'] = $image[0]['id'];
        } else {
            $pooList[$k]['image'] = 0;
        }
    }
}

//提示信息
$msgArr = array('操作失败，请稍后重试。', '建立了新的幻灯片。', '文件上传失败，请稍后重试。', '建立了新的产品目录。', '无法建立新的幻灯片，请检查输入并重试。', '无法建立新的产品目录，请检查输入后重试。', '修改成功！', '无法修改，请稍后重试。', '删除成功！', '无法删除，请稍后重试。', '成功置换了位置。', '无法修改排序，请稍后重试。');
$msgTypeArr = array(array('错误！', 'error'), array('成功！', 'success'), array('失败！', 'error'), array('成功！', 'success'), array('失败！', 'error'), array('失败！', 'error'), array('成功！', 'success'), array('失败！', 'error'), array('成功！', 'success'), array('失败！', 'error'), array('成功！', 'success'), array('失败！', 'error'));

//includes
$includePage['css'][] = 'admin-operate-index';
?>
<?php require('page-admin-top.php'); ?>

<div class="row">
    <div class="span2">&nbsp;</div>
    <div class="span10">
        <h2>首页设置</h2>
    </div>
</div>

<div class="row">
    <div class="span2"></div>
    <div class="span10">
        <p>上传文件格式必须为 : <?php echo UPLOAD_IMG_TYPE; ?> , 尺寸必须在 <?php echo UPLOAD_IMG_SIZE_W . 'x' . UPLOAD_IMG_SIZE_H; ?> 以内 , 文件大小必须在 <?php echo UPLOAD_SIZE_MAX / 1024; ?> MB以内。</p>
        <p>修改时，如果不修改图片则无需上传文件。</p>
        <!-- slide show -->
        <h4>首页幻灯片</h4>
        <hr>
        <ul class="thumbnails">
            <?php if($slideshowList){ foreach($slideshowList as $k=>$v){ ?>
            <li class="span3 operate-list">
                <div class="thumbnail">
                    <img data-src="holder.js/300x200" src="image.php?id=<?php echo $v['image']; ?>">
                    <div class="caption">
                        <form action="action-operate-index.php?action=edit&id=<?php echo $v['id']; ?>" method="post" enctype="multipart/form-data">
                            <input type="file" name="upload" />
                            <input name="title" type="text" placeholder="标题" value="<?php echo $v['post_title']; ?>">
                            <input name="url" type="text" placeholder="URL" value="<?php echo $v['post_url']; ?>">
                            <textarea name="content" rows="3" placeholder="描述"><?php echo $v['post_content']; ?></textarea>
                            <p>
                                <button type="submit" class="btn btn-primary"><i class="icon-pencil icon-white"></i></button> 
                                <a href="action-operate-index.php?action=delete&id=<?php echo $v['id']; ?>" class="btn btn-warning"><i class="icon-trash icon-white"></i></a>
                                <?php if(isset($slideshowList[$k-1])==true){ ?><a href="action-operate-index.php?action=sort&id=<?php echo $v['id']; ?>&dest=<?php echo $slideshowList[$k-1]['id']; ?>" class="btn btn-info"><i class="icon-arrow-left icon-white"></i></a><?php } ?>
                                <?php if(isset($slideshowList[$k+1])==true){ ?><a href="action-operate-index.php?action=sort&id=<?php echo $v['id']; ?>&dest=<?php echo $slideshowList[$k+1]['id']; ?>" class="btn btn-info"><i class="icon-arrow-right icon-white"></i></a><?php } ?>
                            </p>
                        </form>
                    </div>
                </div>
            </li>
            <?php } } ?>
            <li class="span4">
                <div class="thumbnail">
                    <div class="caption">
                        <form action="action-operate-index.php?action=add-slideshow" method="post" enctype="multipart/form-data">
                            <input type="file" name="upload" />
                            <p>建议尺寸 : 1500 x 500 px</p>
                            <input name="title" type="text" placeholder="标题">
                            <input name="url" type="text" placeholder="URL">
                            <textarea name="content" rows="3" placeholder="描述"></textarea>
                            <p><button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> 添加</button></p>
                        </form>
                    </div>
                </div>
            </li>
        </ul>
        <!-- product list -->
        <h4>首页产品目录</h4>
        <hr>
        <ul class="thumbnails">
            <?php if($proList){ foreach($proList as $k=>$v){ ?>
            <li class="span3 operate-list">
                <div class="thumbnail">
                    <img data-src="holder.js/140x140" src="image.php?id=<?php echo $v['image']; ?>">
                    <div class="caption">
                        <form action="action-operate-index.php?action=edit&id=<?php echo $v['id']; ?>" method="post" enctype="multipart/form-data">
                            <input type="file" name="upload" />
                            <input name="title" type="text" placeholder="产品名称" value="<?php echo $v['post_title']; ?>">
                            <input name="url" type="text" placeholder="URL" value="<?php echo $v['post_url']; ?>">
                            <textarea name="content" rows="3" placeholder="产品简介"><?php echo $v['post_content']; ?></textarea>
                            <p>
                                <button type="submit" class="btn btn-primary"><i class="icon-pencil icon-white"></i></button> 
                                <a href="action-operate-index.php?action=delete&id=<?php echo $v['id']; ?>" class="btn btn-warning"><i class="icon-trash icon-white"></i></a>
                                <?php if(isset($proList[$k-1])==true){ ?><a href="action-operate-index.php?action=sort&id=<?php echo $v['id']; ?>&dest=<?php echo $proList[$k-1]['id']; ?>" class="btn btn-info"><i class="icon-arrow-left icon-white"></i></a><?php } ?>
                                <?php if(isset($proList[$k+1])==true){ ?><a href="action-operate-index.php?action=sort&id=<?php echo $v['id']; ?>&dest=<?php echo $proList[$k+1]['id']; ?>" class="btn btn-info"><i class="icon-arrow-right icon-white"></i></a><?php } ?>
                            </p>
                        </form>
                    </div>
                </div>
            </li>
            <?php } } ?>
            <li class="span4">
                <div class="thumbnail">
                    <div class="caption">
                        <form action="action-operate-index.php?action=add-prolist" method="post" enctype="multipart/form-data">
                            <input type="file" name="upload" />
                            <p>建议尺寸 : 140 x 140 px</p>
                            <input name="title" type="text" placeholder="产品名称">
                            <input name="url" type="text" placeholder="URL">
                            <textarea name="content" rows="3" placeholder="产品简介"></textarea>
                            <p><button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> 添加</button></p>
                        </form>
                    </div>
                </div>
            </li>
        </ul>
        <!-- cooperation list -->
        <h4>底部滚动条</h4>
        <hr>
        <ul class="thumbnails">
            <?php if($pooList){ foreach($pooList as $k=>$v){ ?>
            <li class="span3 operate-list">
                <div class="thumbnail">
                    <img data-src="holder.js/140x140" src="image.php?id=<?php echo $v['image']; ?>">
                    <div class="caption">
                        <form action="action-operate-index.php?action=edit&id=<?php echo $v['id']; ?>" method="post" enctype="multipart/form-data">
                            <input type="file" name="upload" />
                            <input name="title" type="text" placeholder="名称" value="<?php echo $v['post_title']; ?>">
                            <input name="url" type="text" placeholder="URL" value="<?php echo $v['post_url']; ?>">
                            <textarea name="content" class="hidden"></textarea>
                            <p>
                                <button type="submit" class="btn btn-primary"><i class="icon-pencil icon-white"></i></button> 
                                <a href="action-operate-index.php?action=delete&id=<?php echo $v['id']; ?>" class="btn btn-warning"><i class="icon-trash icon-white"></i></a>
                                <?php if(isset($pooList[$k-1])==true){ ?><a href="action-operate-index.php?action=sort&id=<?php echo $v['id']; ?>&dest=<?php echo $pooList[$k-1]['id']; ?>" class="btn btn-info"><i class="icon-arrow-left icon-white"></i></a><?php } ?>
                                <?php if(isset($pooList[$k+1])==true){ ?><a href="action-operate-index.php?action=sort&id=<?php echo $v['id']; ?>&dest=<?php echo $pooList[$k+1]['id']; ?>" class="btn btn-info"><i class="icon-arrow-right icon-white"></i></a><?php } ?>
                            </p>
                        </form>
                    </div>
                </div>
            </li>
            <?php } } ?>
            <li class="span4">
                <div class="thumbnail">
                    <div class="caption">
                        <form action="action-operate-index.php?action=add-poolist" method="post" enctype="multipart/form-data">
                            <input type="file" name="upload" />
                            <p>建议尺寸 : 140 x 140 px</p>
                            <input name="title" type="text" placeholder="名称">
                            <input name="url" type="text" placeholder="URL">
                            <p><button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> 添加</button></p>
                        </form>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>

<?php require('page-admin-footer.php'); ?>