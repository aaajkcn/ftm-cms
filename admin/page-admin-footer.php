<?php

/**
 * 登录后页脚
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
if (isset($pageIncludeBool) == false) {
    die();
}
?>
            </div>
            <div id="push"></div>
        </div>
<?php require('footer.php'); ?>
        <script src="assets/jquery.js"></script>
        <script src="assets/bootstrap.js"></script>
        <?php if($includePage['js'] !== null){ foreach($includePage['js'] as $v){ ?>
        <script src="assets/<?php echo $v; ?>.js"></script>
        <?php } } ?>
        <script>
            //全局变量定义
            var topMenu = new Object();
            topMenu.id = '#top-menu';
            topMenu.childId = topMenu.id+' li';
            //消息消失处理
            function msgTimeOut(){
                $('#msg').fadeOut();
            }
            $(document).ready(function() {
                <?php if(isset($_GET['msg']) == true){ if(isset($msgArr[$_GET['msg']]) == true){ ?>
                    //消息处理
                    var msgTime = window.setTimeout('msgTimeOut();',5000);
                    $('#msg').click(function(){
                        msgTimeOut();
                    });
                <?php } } ?>
                //顶部菜单处理
                $(topMenu.childId).hover(function(){
                    var attrClass = "";
                    if($(this).attr('class')){
                        attrClass = $(this).attr('class');
                    }
                    $(this).data('class',attrClass);
                    $(this).attr('class',attrClass+' active');
                },function(){
                    $(this).attr('class',$(this).data('class'));
                });
                //主菜单栏显示
                $('#top-menu li').hover(function() {
                    $(this).addClass('open');
                }, function() {
                    $(this).removeClass('open');
                });
            });
        </script>
    </body>
</html>
