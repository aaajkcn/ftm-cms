<?php

/**
 * 用户组操作
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require_once('page-admin.php');
$msg = 0;
if (isset($_GET['action']) == true && $sysUser->checkPower('USER-GROUP') == true) {
    if ($_GET['action'] == 'add' && isset($_POST['name']) == true && isset($_POST['power']) == true) {
        $res = $sysUser->addGroup($_POST['name'], $_POST['power']);
        $msg = $res == true ? 1 : 2;
    } elseif ($_GET['action'] == 'edit' && isset($_GET['id']) == true && isset($_POST['name']) == true && isset($_POST['power']) == true && (int) $_GET['id'] != VISITOR_USER_GROUP) {
        $id = (int) $_GET['id'];
        $res = $sysUser->editGroup($id, $_POST['name'], $_POST['power']);
        $msg = $res == true ? 3 : 4;
    } elseif ($_GET['action'] == 'delete' && isset($_GET['id']) == true && (int) $_GET['id'] != VISITOR_USER_GROUP) {
        if ($_GET['id'] != $userRes['user_group']) {
            $res = $sysUser->delGroup($_GET['id']);
            $msg = $res == true ? 5 : 6;
        }
    }
}
$coreFeedback->output('url', 'admin-user-group.php?active=3&msg=' . $msg);
?>
