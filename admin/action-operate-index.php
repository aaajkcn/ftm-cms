<?php

/**
 * 首页操作
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require_once('page-admin.php');
require(DIR_LIB . DS . 'sys-post.php');
require(DIR_LIB . DS . 'sys-upload.php');
$sysPost = new SysPost($coreDB, $sysLog);
$msg = 0;
if (isset($_GET['action']) == true && $sysUser->checkPower('BASIC') == true) {
    if ($_GET['action'] == 'add-slideshow' && isset($_POST['title']) == true && isset($_POST['url']) == true && isset($_POST['content']) == true && isset($_FILES['upload']) == true) {
        $postID = $sysPost->add($userRes['id'], $_POST['title'], $_POST['content'], $_POST['url'], 'public', 0, 0, null, $sysPost->type[6]);
        if ($postID > 0) {
            $sysUpload = new SysUpload($sysPost, $sysLog, DIR_UPLOAD, $userRes['id']);
            $fileID = $sysUpload->upload($_FILES['upload'], $postID, true);
            if ($fileID < 1) {
                $sysPost->del($postID);
            }
            $msg = $fileID > 0 ? 1 : 2;
        } else {
            $msg = 3;
        }
    } elseif ($_GET['action'] == 'add-prolist' && isset($_POST['title']) == true && isset($_POST['url']) == true && isset($_POST['content']) == true && isset($_FILES['upload']) == true) {
        $postID = $sysPost->add($userRes['id'], $_POST['title'], $_POST['content'], $_POST['url'], 'public', 0, 0, null, $sysPost->type[7]);
        if ($postID > 0) {
            $sysUpload = new SysUpload($sysPost, $sysLog, DIR_UPLOAD, $userRes['id']);
            $fileID = $sysUpload->upload($_FILES['upload'], $postID, true);
            if ($fileID < 1) {
                $sysPost->del($postID);
            }
            $msg = $fileID > 0 ? 1 : 2;
        } else {
            $msg = 3;
        }
    } elseif ($_GET['action'] == 'add-poolist' && isset($_POST['title']) == true && isset($_POST['url']) == true && isset($_FILES['upload']) == true) {
        $postID = $sysPost->add($userRes['id'], $_POST['title'], '', $_POST['url'], 'public', 0, 0, null, $sysPost->type[8]);
        if ($postID > 0) {
            $sysUpload = new SysUpload($sysPost, $sysLog, DIR_UPLOAD, $userRes['id']);
            $fileID = $sysUpload->upload($_FILES['upload'], $postID, true);
            if ($fileID < 1) {
                $sysPost->del($postID);
            }
            $msg = $fileID > 0 ? 1 : 2;
        } else {
            $msg = 3;
        }
    } elseif ($_GET['action'] == 'edit' && isset($_GET['id']) == true && isset($_POST['title']) == true && isset($_POST['url']) == true && isset($_POST['content']) == true) {
        $res = $sysPost->get($_GET['id'], null);
        if ($res) {
            $resR = $sysPost->edit($res['id'], $res['post_user'], $_POST['title'], $_POST['content'], $_POST['url'], $res['post_status'], $res['post_parent'], $res['post_order'], $res['post_password']);
            $msg = $resR > 0 ? 6 : 7;
            if (isset($_FILES['upload']) == true && $_FILES['upload']['tmp_name']) {
                $where = '`post_status` = \'public\' and `post_type` = \'file\' and `post_meta` = \'image/jpeg\' and `post_parent` = :id';
                $attrs = array(':id' => array($res['id'], PDO::PARAM_INT));
                $resImage = $sysPost->getList($where, $attrs);
                $sysUpload = new SysUpload($sysPost, $sysLog, DIR_UPLOAD, $userRes['id']);
                //存在则删除旧的图片
                if ($resImage) {
                    $sysUpload->del($resImage[0]['id']);
                }
                $resR = $sysUpload->upload($_FILES['upload'], $res['id'], true);
                $msg = $resR > 0 ? 6 : 2;
            }
        }
    } elseif ($_GET['action'] == 'sort' && isset($_GET['id']) == true && isset($_GET['dest']) == true) {
        $id = (int) $_GET['id'];
        $dest = (int) $_GET['dest'];
        $res = $sysPost->get($id);
        $destRes = $sysPost->get($dest);
        $msg = 11;
        if ($res && $destRes) {
            if($sysPost->edit($res['id'], $res['post_user'], $res['post_title'], $res['post_content'], $res['post_url'], $res['post_status'], $res['post_parent'], $destRes['post_order'], $res['post_password']) == true && $sysPost->edit($destRes['id'], $destRes['post_user'], $destRes['post_title'], $destRes['post_content'], $destRes['post_url'], $destRes['post_status'], $destRes['post_parent'], $res['post_order'], $destRes['post_password']) == true){
                $msg = 10;
            }
        }
    } elseif ($_GET['action'] == 'delete' && isset($_GET['id']) == true) {
        $postID = (int) $_GET['id'];
        $passwd = null;
        $res = $sysPost->get($_GET['id'], $passwd);
        if ($res) {
            $where = '`post_status` = \'public\' and `post_type` = \'file\' and `post_meta` = \'image/jpeg\' and `post_parent` = :id';
            $attrs = array(':id' => array($res['id'], PDO::PARAM_INT));
            $resImage = $sysPost->getList($where, $attrs);
            if ($resImage) {
                $sysUpload = new SysUpload($sysPost, $sysLog, DIR_UPLOAD, $userRes['id']);
                $sysUpload->del($resImage[0]['id'], $passwd);
            }
            $res = $sysPost->del($postID);
            $msg = $res == true ? 8 : 9;
        }
    }
}
$coreFeedback->output('url', 'admin-operate-index.php?active=3&msg=' . $msg);
?>
