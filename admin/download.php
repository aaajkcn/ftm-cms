<?php

/**
 * 下载文件
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require_once('page-admin.php');
require(DIR_LIB . DS . 'sys-post.php');
require(DIR_LIB . DS . 'sys-upload.php');
$sysPost = new SysPost($coreDB, $sysLog);
$sysUpload = new SysUpload($sysPost, $sysLog, DIR_UPLOAD, $userRes['id']);
if (isset($_GET['id']) == true) {
    $id = (int) $_GET['id'];
    $passwd = isset($_GET['passwd']) == true ? $_GET['passwd'] : null;
    $sysUpload->downloadFile($id, $passwd, UPLOAD_DOWN_PHP);
}
?>
