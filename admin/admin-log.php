<?php
/**
 * 网站日志
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require('page-admin-page.php');

//判断权限
if($sysUser->checkPower('LOG') == false){
    CoreHeader::toURL($errorPagePower);
}

//页码模块
require(DIR_LIB . DS . 'page-pagination.php');

//提示信息
$msgArr = array('无法归档日志，请稍后重试。', '归档成功！', '无法归档日志，请稍后重试。');
$msgTypeArr = array(array('错误！', 'error'), array('成功！', 'success'), array('错误！', 'error'));

//日志列表
$page = isset($_GET['page']) == true ? (int) $_GET['page'] : 1;
$max = 20;
$sort = isset($_GET['sort']) == true ? (int) $_GET['sort'] : 0;
$desc = isset($_GET['desc']) == true ? false : true;
$logList = $sysLog->getList($page, $max, $sort, $desc);
$logListRow = $sysLog->getListRow();
$pagePagination = new PagePagination('admin-log.php?active=3&page=');
$pagePagination->setMax($max);
?>
<?php require('page-admin-top.php'); ?>
<div class="row">
    <div class="span2">&nbsp;</div>
    <div class="span10">
        <h2>网站日志</h2>
        <p><a href="action-log.php?action=file" class="btn btn-warning" target="_self"><i class="icon-retweet icon-white"></i> 归档日志记录</a></p>
    </div>
</div>

<div class="row">
    <div class="span2"></div>
    <div class="span10">
        <p>&nbsp;</p>
        <div class="accordion" id="accordion2">
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                        尚未归档日志
                    </a>
                </div>
                <div id="collapseOne" class="accordion-body collapse in">
                    <div class="accordion-inner">
                        <?php if ($logList) { ?>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th><a href="admin-log.php?active=3&sort=0<?php if($desc == true) echo '&desc=1'; ?>" target="_self">#</a></th>
                                        <th><a href="admin-log.php?active=3&sort=1<?php if($desc == true) echo '&desc=1'; ?>" target="_self">时间</a></th>
                                        <th><a href="admin-log.php?active=3&sort=2<?php if($desc == true) echo '&desc=1'; ?>" target="_self">IP</a></th>
                                        <th><a href="admin-log.php?active=3&sort=3<?php if($desc == true) echo '&desc=1'; ?>" target="_self">消息</a></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($logList as $v) { ?>
                                        <tr>
                                            <td><?php echo $v['id']; ?></td>
                                            <td><?php echo $v['log_time']; ?></td>
                                            <td><?php echo $v['log_ip']; ?></td>
                                            <td style="max-width: 150px;"><?php echo $v['log_msg']; ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <div class="text-center">
                                <p><?php echo $pagePagination->output($page, $logListRow); ?></p>
                            </div>
                        <?php }else{ ?>
                        <p>没有任何日志数据。</p>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                        归档的日志
                    </a>
                </div>
                <div id="collapseTwo" class="accordion-body collapse">
                    <div class="accordion-inner">
                        考虑到安全性，请通过FTP或其他方式访问<?php echo DIR_DATA.DS.'log'; ?>目录。
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require('page-admin-footer.php'); ?>
