<?php

/**
 * 文章处理器
 * @author fotomxq <fotomxq.me>
 * @version 3
 * @package admin
 */
require_once('page-admin.php');

//引入并声明post类
require(DIR_LIB . DS . 'sys-post.php');
$sysPost = new SysPost($coreDB, $sysLog);

//引入并声明product操作对象
require(DIR_LIB . DS . 'sys-product.php');
$sysProduct = new SysProduct($coreDB, $sysLog);

//引用并声明comment对象
require(DIR_LIB . DS . 'sys-comment.php');
$sysComment = new SysComment($coreDB, $sysLog);

$msg = 0;
$parent = isset($_GET['parent']) == true ? (int) $_GET['parent'] : 0;

//分析动作
if (isset($_GET['action']) == true && $sysUser->checkPower('NEWS') == true) {
    if ($_GET['action'] == 'add' && isset($_POST['parent']) == true && isset($_POST['title']) == true && isset($_POST['content']) == true && $_POST['parent'] > 0 && $_POST['title'] && $_POST['content']) {
        //添加操作
        //组合product
        $productInsertList = null;
        if (isset($_POST['proName']) == true && isset($_POST['proPrice']) == true && isset($_POST['proDescription']) == true && is_array($_POST['proName']) == true && is_array($_POST['proPrice']) == true && is_array($_POST['proDescription']) == true) {
            foreach ($_POST['proName'] as $k => $v) {
                if ($v && isset($_POST['proPrice'][$k]) == true && isset($_POST['proDescription'][$k]) == true) {
                    $productInsertList[$k]['name'] = $v;
                    $productInsertList[$k]['price'] = (int) $_POST['proPrice'][$k];
                    $productInsertList[$k]['des'] = $_POST['proDescription'][$k];
                }
            }
        }
        //准备参数默认值
        $postTitle = $_POST['title'];
        $postContent = $_POST['content'];
        $postURL = null;
        $postStatus = $sysPost->status[0];
        $postParent = (int) $_POST['parent'];
        $postOrder = 0;
        $postPassword = null;
        $postType = 'text';
        $postMeta = 'text/html';
        $postID = $sysPost->add($userRes['id'], $postTitle, $postContent, $postURL, $postStatus, $postParent, $postOrder, $postPassword, $postType, $postMeta);
        $res = true;
        if ($postID > 0 && $productInsertList) {
            $res = $sysProduct->setProducts($postID, $productInsertList);
        }
        $msg = $res == true && $postID > 0 ? 1 : 2;
    } elseif ($_GET['action'] == 'edit' && isset($_POST['parent']) == true && isset($_GET['id']) == true && isset($_POST['title']) == true && isset($_POST['content']) == true && $_POST['parent'] > 0 && $_GET['id'] > 0 && $_POST['title'] && $_POST['content']) {
        //编辑操作
        //组合product
        $productInsertList = null;
        if (isset($_POST['proName']) == true && isset($_POST['proPrice']) == true && isset($_POST['proDescription']) == true && is_array($_POST['proName']) == true && is_array($_POST['proPrice']) == true && is_array($_POST['proDescription']) == true) {
            foreach ($_POST['proName'] as $k => $v) {
                if ($v && isset($_POST['proPrice'][$k]) == true && isset($_POST['proDescription'][$k]) == true) {
                    $productInsertList[$k]['name'] = $v;
                    $productInsertList[$k]['price'] = (int) $_POST['proPrice'][$k];
                    $productInsertList[$k]['des'] = $_POST['proDescription'][$k];
                }
            }
        }
        //编辑post
        $id = (int) $_GET['id'];
        $postRes = $sysPost->get($id, null);
        if ($postRes) {
            $res = $sysPost->add($postRes['post_user'], $postRes['post_title'], $postRes['post_content'], $postRes['post_url'], $sysPost->status[3], $id, $postRes['post_order'], $postRes['post_password'], $postRes['post_type'], $postRes['post_meta']);
            if ($res == true) {
                //准备参数默认值
                $postTitle = $_POST['title'];
                $postContent = $_POST['content'];
                $postURL = $postRes['post_url'];
                $postStatus = $sysPost->status[0];
                $postParent = (int) $_POST['parent'];
                $postOrder = $postRes['post_order'];
                $postPassword = $postRes['post_password'];
                $res = $sysPost->edit($id, $userRes['id'], $postTitle, $postContent, $postURL, $postStatus, $postParent, $postOrder, $postPassword);
                $res = true;
                if ($postRes['id'] > 0 && $productInsertList) {
                    $res = $sysProduct->setProducts($postRes['id'], $productInsertList);
                }
                $msg = $res == true ? 3 : 4;
            }
        }
    } elseif ($_GET['action'] == 'sort' && isset($_GET['id']) == true && isset($_GET['dest']) == true) {
        $id = (int) $_GET['id'];
        $dest = (int) $_GET['dest'];
        $postSrcRes = $sysPost->get($id, null);
        $postDestRes = $sysPost->get($dest, null);
        if ($postSrcRes && $postDestRes) {
            $res = $sysPost->edit($postSrcRes['id'], $postSrcRes['post_user'], $postSrcRes['post_title'], $postSrcRes['post_content'], $postSrcRes['post_url'], $postSrcRes['post_status'], $postSrcRes['post_parent'], $postDestRes['post_order'], $postSrcRes['post_password']);
            if ($res == true) {
                $res = $sysPost->edit($postDestRes['id'], $postDestRes['post_user'], $postDestRes['post_title'], $postDestRes['post_content'], $postDestRes['post_url'], $postDestRes['post_status'], $postDestRes['post_parent'], $postSrcRes['post_order'], $postDestRes['post_password']);
                $msg = $res == true ? 7 : 8;
            }
        }
    } elseif ($_GET['action'] == 'delete' && isset($_GET['id']) == true) {
        //删除操作
        $id = (int) $_GET['id'];
        $sysProduct->delAll($id);
        $sysComment->delAll($id);
        $res = $sysPost->del($id);
        $msg = $res == true ? 5 : 6;
    }
}
$coreFeedback->output('url', 'admin-infos.php?active=2&parent=' . $parent . '&msg=' . $msg);
?>
