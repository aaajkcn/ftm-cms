<?php

/**
 * 日志操作
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require_once('page-admin.php');
$msg = 0;
if (isset($_GET['action']) == true && $sysUser->checkPower('LOG') == true) {
    if ($_GET['action'] == 'file') {
        $res = $sysLog->file();
        $msg = $res ? 1 : 2;
    }
}
$coreFeedback->output('url', 'admin-log.php?active=3&msg=' . $msg);
?>
