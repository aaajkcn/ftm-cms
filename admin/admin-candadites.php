<?php

/**
 * 应聘管理
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require('page-admin-page.php');

//判断权限
if(CANDADITES_ON == true && $sysUser->checkPower('CANDIDATES') == false){
    CoreHeader::toURL($errorPagePower);
}

//页码生成器
require(DIR_LIB . DS . 'page-pagination.php');

//当前动作
$action = isset($_GET['action']) == true ? $_GET['action'] : 'check';

//引用并声明post对象
require(DIR_LIB . DS . 'sys-post.php');
$sysPost = new SysPost($coreDB, $sysLog);

//引用并声明candadites对象
require(DIR_LIB . DS . 'sys-candadites.php');
$sysCandadites = new SysCandadites($coreDB, $sysLog, CANDADITES_TIME_LIMIT);

//获取状态
$status = isset($_GET['status']) == true ? $_GET['status'] : $sysCandadites->status[0];
if(in_array($status, $sysCandadites->status) == false){
    $status = $sysCandadites->status[0];
}

//获取列表
$tableList = null;
$tableListRow = 0;
$page = isset($_GET['page']) == true ? (int) $_GET['page'] : 1;
$sort = isset($_GET['sort']) == true ? (int) $_GET['sort'] : 3;
$desc = isset($_GET['desc']) == true ? false : true;
if ($action == 'list') {
    $where = '`cs_status` = \'' . $status . '\'';
    $attrs = null;
    $max = 10;
    $tableList = $sysCandadites->getList($where, $attrs, $page, $max, $sort, $desc);
    $tableListRow = $sysCandadites->getListRow($where, $attrs);
} else {
    $where = '`cs_status` = \'' . $status . '\'';
    $attrs = null;
    $tableList = $sysCandadites->getList($where, $attrs, 1, 1, 0, false);
}

//提示信息
$msgArr = array('无法执行操作，请重试。', '审核成功！', '无法审核该应聘，请稍后重试。', '删除成功！', '删除失败，请稍后重试。');
$msgTypeArr = array(array('错误！', 'error'), array('成功！', 'success'), array('错误！', 'info'), array('成功！', 'success'), array('错误！', 'info'));

//包含文件
$includePage['css'][] = 'admin-candadites';
$includePage['js'][] = 'admin-candadites';

require('page-admin-top.php');
?>
<p>&nbsp;</p>
<div class="btn-group">
    <a href="admin-candadites.php?active=2&action=check" class="btn">逐个审核应聘</a>
</div>
<div class="btn-group">
    <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">查看列表 <span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li><a href="admin-candadites.php?active=2&action=list&status=<?php echo $sysCandadites->status[0]; ?>">未审核列表</a></li>
        <li><a href="admin-candadites.php?active=2&action=list&status=<?php echo $sysCandadites->status[1]; ?>">已通过列表</a></li>
        <li><a href="admin-candadites.php?active=2&action=list&status=<?php echo $sysCandadites->status[2]; ?>">未通过列表</a></li>
    </ul>
</div>
<?php if($action == 'check'){ ?>
<p>&nbsp;</p>
<h2>审核应聘</h2>
<hr>
<?php if($tableList){ ?>
<div class="row">
    <div class="span2">&nbsp;</div>
    <div class="span1"><b>时间</b></div>
    <div class="span4"><?php echo $tableList[0]['cs_date']; ?></div>
    <div class="span1"><b>IP</b></div>
    <div class="span4"><?php echo $tableList[0]['cs_ip']; ?></div>
</div>
<p>&nbsp;</p>
<div class="row">
    <div class="span2">&nbsp;</div>
    <div class="span1"><b>姓名</b></div>
    <div class="span4"><?php echo $tableList[0]['cs_name']; ?></div>
    <div class="span1"><b>联系方式</b></div>
    <div class="span4"><?php echo $tableList[0]['cs_contact']; ?></div>
</div>
<div class="row">&nbsp;</div>
<div class="row">
    <div class="span2">&nbsp;</div>
    <div class="span10"><?php echo $tableList[0]['cs_resume']; ?></div>
</div>
<p>&nbsp;</p>
<div class="row">
</div>
<p>&nbsp;</p>
<div class="row">
    <div class="span2">&nbsp;</div>
    <div class="span10">
        <a href="action-candadites.php?action=check&id=<?php echo $tableList[0]['id']; ?>&pass=1" class="btn btn-primary">通过</a>
        <a href="action-candadites.php?action=check&id=<?php echo $tableList[0]['id']; ?>&pass=0" class="btn btn-warning">拒绝</a>
    </div>
</div>
<?php }else{ ?>
<div class="span12">
    <p>&nbsp;</p>
    <p>您已经审核完所有的应聘书。</p>
</div>
<?php } ?>
<?php } ?>
<?php if($action == 'list'){ ?>
<p>&nbsp;</p>
<h2>应聘列表</h2>
<hr>
<?php if($tableList){ ?>
<table class="table table-hover admin-table">
    <thead>
        <tr>
            <th>时间</th>
            <th>姓名</th>
            <th>联系方式</th>
            <th>IP地址</th>
            <th>操作</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($tableList as $v){ ?>
        <tr name="table-data-tr">
            <td><?php echo $v['cs_date']; ?></td>
            <td><?php echo $v['cs_name']; ?></td>
            <td><?php echo $v['cs_contact']; ?></td>
            <td><?php echo $v['cs_ip']; ?></td>
            <td>
                <?php if($status == 'audit'){ ?>
                <a href="action-candadites.php?action=check&id=<?php echo $v['id']; ?>&pass=1" class="btn btn-primary"><i class="icon-pencil icon-white"></i> 通过</a> 
                <a href="action-candadites.php?action=check&id=<?php echo $v['id']; ?>&pass=0" class="btn btn-warning"><i class="icon-trash icon-white"></i> 不通过</a></td>
                <?php }else{ ?>
                <a href="action-candadites.php?action=delete&id=<?php echo $v['id']; ?>" class="btn btn-warning"><i class="icon-trash icon-white"></i> 删除</a></td>
                <?php } ?>
        </tr>
        <?php } ?>
    </tbody>
</table>
<div class="text-center">
    <p><?php echo PagePaginationQuick('admin-candadites.php?active=2&page=', $page, $tableListRow); ?></p>
</div>
<?php }else{ ?>
<div class="span12">
    <p>&nbsp;</p>
    <p>尚未有人参与我公司面试。</p>
</div>
<?php } ?>
<?php } ?>
<?php require('page-admin-footer.php'); ?>
