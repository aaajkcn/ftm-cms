<?php

/**
 * 提交引用页面
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require('config.php');
//禁止页面缓存
CoreHeader::noCache();
//引用并声明反馈处理器
require(DIR_LIB . DS . 'core-feedback.php');
$coreFeedback = new CoreFeedback();
?>
