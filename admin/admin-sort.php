<?php

/**
 * 分类管理
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package admin
 */
require('page-admin-page.php');

//判断权限
if($sysUser->checkPower('SORT') == false){
    CoreHeader::toURL($errorPagePower);
}

//当前页面标识
$pageName = 'sort';
$pageType = 'sort';

//页码生成器
require(DIR_LIB . DS . 'page-pagination.php');

//当前动作
$action = isset($_GET['action']) == true ? $_GET['action'] : 'add';

//引用并声明post对象
require(DIR_LIB . DS . 'sys-post.php');
$sysPost = new SysPost($coreDB, $sysLog);

//获取列表
$parent = isset($_GET['parent']) == true ? (int) $_GET['parent'] : 0;
$sortRes = null;
if ($parent > 0) {
    $sortRes = $sysPost->get($parent, null);
    if (!$sortRes) {
        CoreHeader::toURL('error.php?e=un');
    }
}
$where = '`post_status` = \'public\' and `post_type` = \'' . $sysPost->type[0] . '\' and `post_parent` = :parent';
$attrs = array(':parent' => array($parent, PDO::PARAM_INT));
$page = isset($_GET['page']) == true ? (int) $_GET['page'] : 1;
$max = 10;
$sort = isset($_GET['sort']) == true ? (int) $_GET['sort'] : 3;
$desc = isset($_GET['desc']) == true ? true : false;
$tableList = $sysPost->getList($where, $attrs, $page, $max, $sort, $desc);
$tableListRow = $sysPost->getListRow($where, $attrs);

//获取当前焦点
$postRes = null;
if(isset($_GET['id']) == true){
    $id = (int) $_GET['id'];
    $postRes = $sysPost->get($id, null);
}

//包含文件
$includePage['js'][] = 'admin-sort';
$includePage['css'][] = 'admin-sort';

//提示信息
$msgArr = array('无法执行操作，请重试。', '添加成功！', '添加失败，请检查相关信息是否正确。', '修改成功！', '修改失败，请检查相关信息是否正确。', '删除成功！', '无法删除，请稍后重试。', '修改排序成功！', '无法修改位置，请稍后重试。');
$msgTypeArr = array(array('错误！', 'error'), array('成功！', 'success'), array('错误！', 'info'), array('成功！', 'success'), array('错误！', 'info'), array('成功！', 'success'), array('错误！', 'info'), array('成功！', 'success'), array('错误！', 'info'));

require('page-admin-top.php');
?>
<h2>分类列表</h2>
<hr>
<p><?php if($parent > 0){ ?><a href="admin-sort.php?active=2&parent=<?php echo $sortRes['post_parent']; ?>" class="btn">返回上一级</a><?php } ?></p>
<table class="table table-hover">
    <thead>
        <tr>
            <th>名称</th>
            <th>操作</th>
        </tr>
    </thead>
    <tbody class="table-list">
        <?php foreach($tableList as $k=>$v){ ?>
        <tr>
            <td><a href="admin-sort.php?active=2&parent=<?php echo $v['id']; ?>" target="_self"><?php echo $v['post_title']; ?></a></td>
            <td>
                <?php if($k > 0){ ?><a href="action-post.php?action=order&type=<?php echo $pageType; ?>&parent=<?php echo $parent; ?>&id=<?php echo $v['id']; ?>&dest=<?php echo $tableList[$k-1]['id']; ?>" class="btn btn-info"><i class="icon-chevron-up icon-white"></i></a> <?php }else{ ?><a href="#" class="btn"><i class="icon-chevron-up icon-white"></i></a><?php } ?>
                <?php if(isset($tableList[$k+1]) == true){ ?><a href="action-post.php?action=order&type=<?php echo $pageType; ?>&parent=<?php echo $parent; ?>&id=<?php echo $v['id']; ?>&dest=<?php echo $tableList[$k+1]['id']; ?>" class="btn btn-info"><i class="icon-chevron-down icon-white"></i></a> <?php }else{ ?><a href="#" class="btn"><i class="icon-chevron-down icon-white"></i></a><?php } ?>
                <a href="admin-infos.php?active=2&action=add&parent=<?php echo $v['id']; ?>" class="btn btn-info"><i class="icon-th-large icon-white"></i> 添加文章</a> 
                <a href="admin-sort.php?active=2&action=edit&parent=<?php echo $parent; ?>&id=<?php echo $v['id']; ?>" class="btn btn-info"><i class="icon-pencil icon-white"></i></a> 
                <a href="action-post.php?action=delete&type=<?php echo $pageType; ?>&parent=<?php echo $parent; ?>&id=<?php echo $v['id']; ?>" class="btn btn-warning"><i class="icon-trash icon-white"></i></a></td>
        </tr>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <form class="form-horizontal" action="action-post.php?action=add&type=<?php echo $pageType; ?>&parent=<?php echo $parent; ?>" method="post">
                <td><input type="text" name="title" placeholder="分类名称"><input type="text" name="content" placeholder="描述"></td>
                <td><button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> 添加</button></td>
            </form>
        </tr>
    </tfoot>
</table>
<div class="text-center">
    <p><?php echo PagePaginationQuick('admin-sort.php?active=2&parent='.$parent.'&page=', $page, $tableListRow); ?></p>
</div>
<?php if($action == 'edit' && $postRes){ ?>
<h2>修改分类</h2>
<hr>
<form class="form-horizontal" id="edit-form" action="action-post.php?action=edit&type=<?php echo $pageType; ?>&parent=<?php echo $parent; ?>&id=<?php echo $postRes['id']; ?>" method="post">
    <div class="control-group">
        <label class="control-label" for="inputTitle">分类名称</label>
        <div class="controls">
            <input type="text" name="title" id="inputTitle" placeholder="分类名称" value="<?php echo $postRes['post_title']; ?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputContent">分类描述</label>
        <div class="controls">
            <input type="text" name="content" id="inputContent" placeholder="分类描述" value="<?php echo $postRes['post_content']; ?>">
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> 修改</button>
        </div>
    </div>
</form>
<?php } ?>
<?php require('page-admin-footer.php'); ?>
