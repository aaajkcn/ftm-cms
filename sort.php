<?php

/**
 * 分类页面
 * @author fotomxq <fotomxq.me>
 * @version 2
 * @package page
 */
require('page.php');

//强制不生成缓冲
$smarty->caching = false;

//初始化变量
$showType = 'none';
$sortRes = null;
$sortList = null;
$textList = null;
$pageStr = '';

if (isset($_GET['id']) == true) {
    //页码模块
    require(DIR_LIB . DS . 'page-pagination.php');
    //获取上一级ID
    $parent = (int) $_GET['id'];
    //获取主体sort信息
    $sortRes = $sysPost->get($parent, null, true);
    //分类分页
    $sortPage = isset($_GET['page']) == true ? (int) $_GET['page'] : 1;
    $sortMax = 20;
    $sortSort = 3;
    $sortDesc = false;
    //获取分类列
    $sortWhere = '`post_parent` = :parent and `post_status` = \'' . $sysPost->status[0] . '\' and `post_type` = \'' . $sysPost->type[0] . '\'';
    $sortAttrs = array(':parent' => array($parent, PDO::PARAM_INT));
    $sortList = $sysPost->getList($sortWhere, $sortAttrs, $sortPage, $sortMax, $sortSort, $sortDesc, true);
    if ($sortList) {
        $showType = 'sort';
        $sortListRow = $sysPost->getListRow($sortWhere, $sortAttrs);
        $url = 'sort.php?id=' . $parent . '&page=';
        $pageStr = PagePaginationQuick($url, $sortPage, $sortListRow, $sortMax);
    }
    //文章分页
    $textPage = 1;
    //如果分类没有内容，则主体显示文章列，否则默认第一页为准
    if (!$sortList) {
        $textPage = isset($_GET['page']) == true ? (int) $_GET['page'] : 1;
    }
    $textMax = 20;
    $textSort = 3;
    $textDesc = false;
    //获取文章列
    $textWhere = '`post_status` = \'' . $sysPost->status[0] . '\' and `post_type` = \'' . $sysPost->type[2] . '\' and (`post_parent` = :parent';
    if ($sortList) {
        foreach ($sortList as $v) {
            $textWhere .= ' or `post_parent` = ' . $v['id'];
        }
        $textSort = 0;
        $textDesc = true;
    }
    $textWhere .= ')';
    $textAttrs = array(':parent' => array($parent, PDO::PARAM_INT));
    $textList = $sysPost->getList($textWhere, $textAttrs, $textPage, $textMax, $textSort, $textDesc);
    if ($textList) {
        if (!$sortList) {
            $showType = 'text';
            $textListRow = $sysPost->getListRow($textWhere, $textAttrs);
            $url = 'sort.php?id=' . $parent . '&page=';
            $pageStr = PagePaginationQuick($url, $textPage, $textListRow, $textMax);
            //如果只有一篇，则判定为直接显示文本，跳转页面
            if (count($textList) === 1) {
                $url = 'text.php?id=' . $textList[0]['id'];
                CoreHeader::toURL($url);
                die();
            }
        }
    }
    //获取全网站最新消息
    $textTopWhere = '`post_status` = \'' . $sysPost->status[0] . '\' and `post_type` = \'' . $sysPost->type[2] . '\'';
    $textTopList = $sysPost->getList($textTopWhere, null, 1, 10, 0, true);
}

//注册变量
$smarty->assign('showType', $showType);
$smarty->assign('sortRes', $sortRes);
$smarty->assign('sortList', $sortList);
$smarty->assign('textList', $textList);
$smarty->assign('pageStr', $pageStr);
$smarty->assign('textTopList', $textTopList);

//输出页面
$smarty->display('sort.tpl');
?>
