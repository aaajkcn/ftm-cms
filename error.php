<?php
/**
 * 错误页面
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package page
 */
//错误消息数组
$errorMessage = array(
    0 => '发生了错误！',
    'login-info' => '登录用户名或密码错误，请尝试重新输入。',
    'login-vcode' => '验证码输入错误，请尝试重新输入。'
);
//错误标识
$errorKey = isset($_GET['e']) == true && isset($errorMessage[$_GET['e']]) ? $_GET['e'] : 0;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>出错了</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="includes/assets/bootstrap.css" rel="stylesheet">
        <link href="includes/assets/bootstrap-responsive.css" rel="stylesheet">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="includes/assets/html5shiv.js"></script>
        <![endif]-->
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="includes/assets/logo-144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="includes/assets/logo-114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="includes/assets/logo-72.png">
        <link rel="apple-touch-icon-precomposed" href="includes/assets/logo-57.png">
        <link rel="shortcut icon" href="includes/assets/logo.png">
        <style type="text/css">
            .errorMessage{
                margin: 80px auto;
                text-align: center;
            }
            .errorMessage h1{
                line-height: 100px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="errorMessage">
                <h1><?php echo $errorMessage[$errorKey]; ?></h1>
                <p>对不起！您查找的页面不存在，请访问其他页面或<a href="index.php">返回首页</a>。</p>
            </div>
        </div>
    </body>
</html>