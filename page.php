<?php

/**
 * 页面引用
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package page
 */
require('config.php');

//修正编码
CoreHeader::toPage();

//访客记录
require(DIR_LIB . DS . 'sys-views.php');
$sysViews = new SysViews($coreDB, $sysLog);
$sysViews->add($coreIP->ip);

//主题类
require(DIR_LIB . DS . 'sys-theme.php');
$sysTheme = new SysTheme($coreDB, $sysLog, $sysConfig);
$nowTheme = $sysTheme->getNow();
$nowThemeDir = DIR_LIB . DS . 'theme' . DS . $nowTheme;

//引入相关包
require(DIR_LIB . DS . 'sys-post.php');
$sysPost = new SysPost($coreDB, $sysLog);

//Smarty
require(DIR_LIB . DS . 'smarty' . DS . 'Smarty.class.php');
$smarty = new Smarty();
$smarty->template_dir = $nowThemeDir;
$smarty->compile_dir = DIR_CACHE;
$smarty->cache_dir = DIR_CACHE;
$smarty->left_delimiter = '{';
$smarty->right_delimiter = '}';
$smarty->cache_lifetime = CACHE_LIMIT_TIME;
$smarty->debugging = DEBUG_ON;
$smarty->caching = CACHE_ON;
if(DEBUG_ON === true){
    $smarty->caching = false;
    $smarty->cache_lifetime = 0;
}

//定义包含页面
$includePage = array('css' => null, 'js' => null);

//获取菜单列
$menuWhere = '`post_status` = \'public\' and `post_type` = \'' . $sysPost->type[0] . '\' and `post_parent` = :parent';
$menuAttrs = array(':parent' => array(0, PDO::PARAM_INT));
$menuList = $sysPost->getList($menuWhere, $menuAttrs, 1, 9999, 3, false);
if($menuList){
    foreach($menuList as $k=>$v){
        $menuAttrs = array(':parent' => array($v['id'], PDO::PARAM_INT));
        $vMenuList = $sysPost->getList($menuWhere, $menuAttrs, 1, 9999, 3, false);
        if($vMenuList){
            $menuList[$k]['child'] = $vMenuList;
        }
    }
}

//幻灯片数据
$slideShowWhere = '`post_status` = \'public\' and `post_type` = \'' . $sysPost->type[6] . '\'';
$slideShowList = $sysPost->getList($slideShowWhere, null, 1, 9999, 0, false, true);
if ($slideShowList) {
    foreach ($slideShowList as $k => $v) {
        $where = '`post_status` = \'public\' and `post_type` = \'file\' and (`post_meta` = \'image/jpeg\' or `post_meta` = \'image/png\') and `post_parent` = :id';
        $attrs = array(':id' => array($v['id'], PDO::PARAM_INT));
        $image = $sysPost->getList($where, $attrs);
        if ($image) {
            $slideShowList[$k]['image'] = $image[0]['id'];
        } else {
            $slideShowList[$k]['image'] = 0;
        }
    }
}

//输出变量
$smarty->assign('webTitle', $sysConfig->load('WEB_TITLE'));
$smarty->assign('webFooterStr', $sysConfig->load('PAGE_FOOTER'));
$smarty->assign('webURL', WEB_URL);
$smarty->assign('menu', $menuList);
$smarty->assign('slideShowList', $slideShowList);
$smarty->assign('postRand', $plugVistorPost->get());
?>
