<?php

/**
 * 文章页面
 * @author fotomxq <fotomxq.me>
 * @version 2
 * @package page
 */
require('page.php');

//强制不生成缓冲
$smarty->caching = false;

//初始化变量
$textRes = null;
$sortRes = null;
$textList = null;
$productList = null;
$commentList = null;
$commentListRow = 0;

//引入产品类
require(DIR_LIB . DS . 'sys-product.php');
$sysProduct = new SysProduct($coreDB, $sysLog);

//引入评论类
require(DIR_LIB . DS . 'sys-comment.php');
$sysComment = new SysComment($coreDB, $sysLog);

//执行动作
if (isset($_GET['id']) == true) {
    //获取文章主体
    $id = (int) $_GET['id'];
    $textRes = $sysPost->get($id, false, true);
    if ($textRes) {
        $productList = $sysProduct->getALL($textRes['id']);
        $commentWhere = '`post_id` = :post and `ct_status` = \'' . $sysComment->status[1] . '\'';
        $commentAttrs = array(':post' => array($textRes['id'], PDO::PARAM_INT));
        $commentList = $sysComment->getList($commentWhere, $commentAttrs, 1, 9999, 0, false);
        $commentListRow = $sysComment->getListRow($commentWhere, $commentAttrs);
        //获取文章所属栏目
        $sortRes = $sysPost->get($textRes['post_parent'], null, true);
        if ($sortRes) {
            //获取文章所属栏目最新信息列
            $textListWhere = '`post_status` = \'' . $sysPost->status[0] . '\' and `post_type` = \'' . $sysPost->type[2] . '\' and `post_parent` = \'' . $sortRes['id'] . '\'';
            $textList = $sysPost->getList($textListWhere, null, 1, 10, 0, true);
        }
    }
}

//获取全网站最新信息列
$textTopWhere = '`post_status` = \'' . $sysPost->status[0] . '\' and `post_type` = \'' . $sysPost->type[2] . '\'';
$textTopList = $sysPost->getList($textTopWhere, null, 1, 10, 0, true);

//判定消息
$messageKey = 0;
if (isset($_GET['msg']) == true) {
    if ($_GET['msg'] == 'buy-success') {
        $messageKey = 1;
    } elseif ($_GET['msg'] == 'buy-failed') {
        $messageKey = 2;
    } elseif ($_GET['msg'] == 'comment-success') {
        $messageKey = 3;
    } elseif ($_GET['msg'] == 'comment-failed') {
        $messageKey = 4;
    }
}

//注册变量
$smarty->assign('textRes', $textRes);
$smarty->assign('sortRes', $sortRes);
$smarty->assign('textList', $textList);
$smarty->assign('productList', $productList);
$smarty->assign('commentList', $commentList);
$smarty->assign('commentListRow', $commentListRow);
$smarty->assign('textTopList', $textTopList);
$smarty->assign('messageKey', $messageKey);

//输出页面
$smarty->display('text.tpl');
?>
