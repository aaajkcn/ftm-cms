--
-- 数据库: `diningbio`
--

-- --------------------------------------------------------

--
-- 表的结构 `sys_candadites`
--

CREATE TABLE IF NOT EXISTS `sys_candadites` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `cs_resume` text COLLATE utf8_bin NOT NULL COMMENT '简历内容',
  `cs_contact` varchar(300) COLLATE utf8_bin NOT NULL COMMENT '联系方式',
  `cs_name` varchar(300) COLLATE utf8_bin NOT NULL COMMENT '姓名',
  `cs_date` datetime NOT NULL COMMENT '创建时间',
  `cs_status` varchar(30) COLLATE utf8_bin NOT NULL COMMENT '状态',
  `cs_ip` varchar(39) COLLATE utf8_bin NOT NULL COMMENT '创建IP',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `sys_comment`
--

CREATE TABLE IF NOT EXISTS `sys_comment` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `post_id` bigint(20) NOT NULL COMMENT 'post-id',
  `ct_status` varchar(30) COLLATE utf8_bin NOT NULL,
  `ct_parent` bigint(20) NOT NULL COMMENT '回复ID',
  `ct_date` datetime NOT NULL,
  `ct_ip` varchar(39) COLLATE utf8_bin NOT NULL COMMENT '创建IP',
  `ct_name` varchar(300) COLLATE utf8_bin DEFAULT NULL COMMENT '姓名',
  `ct_email` varchar(300) COLLATE utf8_bin DEFAULT NULL COMMENT '邮件',
  `ct_url` varchar(300) COLLATE utf8_bin DEFAULT NULL COMMENT '地址',
  `ct_content` text COLLATE utf8_bin NOT NULL COMMENT '评论内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `sys_config`
--

CREATE TABLE IF NOT EXISTS `sys_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `cg_name` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '配置名称',
  `cg_value` text COLLATE utf8_bin COMMENT '配置值',
  `cg_default` text COLLATE utf8_bin COMMENT '默认值',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `cg_name` (`cg_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=9 ;

--
-- 转存表中的数据 `sys_config`
--

INSERT INTO `sys_config` (`id`, `cg_name`, `cg_value`, `cg_default`) VALUES
(1, 'WEB_TITLE', '北京迪宁生物科技有限公司', '网站名称'),
(2, 'IP_BAN_ON', '1', '1'),
(3, 'IP_BAN_LIST', '192.11.15.3,192.11.15.2', NULL),
(4, 'IP_WHITE_ON', '0', '0'),
(5, 'IP_WHITE_LIST', '192.11.15.3,192.11.15.1', NULL),
(6, 'THEME_SELECTED', '1', '1'),
(7, 'INDEX_TEXT_M', '作为时代的骄傲,我们的科学家们正以前所未有的激情努力探索着生命的奥秘。人类美好的未来很大程度上也寄托在生命科学的不断进步之中。迪宁生物愿积极求索,不断创新,与科学家们一起,共同寻找和开发尖端科技,不断开启生命科学激动人心的新篇章。 迪宁生物致力于成为每一位生命科研工作者的忠实伙伴，用我们领先的科技，高品质的产品，丰富的经验和悉心的服务,来帮助推动科学的进步，成为联系人类与科学的纽带。我们高质量的产品和服务已经帮助诸多学者更快更好地完成了他们的工作，结出了累累硕果；而这些产品和服务作为载体，也帮助我们与更多的学术英才搭建了良好合作的桥梁，让我们能够同他们一同寻求更为尖端的技术,携手建设属于我们自己的生命科学产业，共同发现更为激动人心的科学答案。 我们知道，成功源于激情、求知、与奋斗。我们将与您同行，不懈追求，一起播撒、浇灌生命科学的种子，共同创造明天更加健康美好的生活。 迪宁生物，伴您生命科学成功之路！', '公司介绍……'),
(8, 'PAGE_FOOTER', '© 2013 DiNing Biosolutions Company', '© 2013 DiNing Biosolutions Company');

-- --------------------------------------------------------

--
-- 表的结构 `sys_log`
--

CREATE TABLE IF NOT EXISTS `sys_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `log_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `log_ip` varchar(39) COLLATE utf8_bin NOT NULL COMMENT 'Ip地址',
  `log_msg` text COLLATE utf8_bin NOT NULL COMMENT '消息',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=79 ;
-- --------------------------------------------------------

--
-- 表的结构 `sys_order`
--

CREATE TABLE IF NOT EXISTS `sys_order` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_parent` bigint(20) NOT NULL COMMENT '上一级ID',
  `product_id` bigint(20) NOT NULL COMMENT '产品ID',
  `order_ip` varchar(39) COLLATE utf8_bin NOT NULL,
  `order_price` int(11) NOT NULL COMMENT '价格',
  `order_date` datetime NOT NULL,
  `order_status` varchar(30) COLLATE utf8_bin DEFAULT NULL COMMENT '状态',
  `order_name` varchar(300) COLLATE utf8_bin DEFAULT NULL COMMENT '联系人姓名或产品名称',
  `order_contact` varchar(300) COLLATE utf8_bin DEFAULT NULL COMMENT '联系方式',
  `order_note` text COLLATE utf8_bin COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `sys_post`
--

CREATE TABLE IF NOT EXISTS `sys_post` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `post_parent` bigint(20) DEFAULT NULL COMMENT '上一级ID',
  `post_title` varchar(300) COLLATE utf8_bin NOT NULL COMMENT '标题',
  `post_order` bigint(20) NOT NULL COMMENT '排序',
  `post_url` varchar(300) COLLATE utf8_bin DEFAULT NULL COMMENT 'URL',
  `post_date` datetime NOT NULL COMMENT '创建时间',
  `post_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `post_password` varchar(60) COLLATE utf8_bin DEFAULT NULL COMMENT '访问密码',
  `post_content` longtext COLLATE utf8_bin COMMENT '内容',
  `post_status` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT 'public' COMMENT '状态',
  `post_user` bigint(20) NOT NULL COMMENT '用户',
  `post_type` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '类型',
  `post_view` int(11) NOT NULL DEFAULT '0' COMMENT '查看次数',
  `post_meta` varchar(300) COLLATE utf8_bin DEFAULT NULL COMMENT '媒体',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=250 ;

-- --------------------------------------------------------

--
-- 表的结构 `sys_product`
--

CREATE TABLE IF NOT EXISTS `sys_product` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `post_id` bigint(20) NOT NULL COMMENT 'post-id',
  `pt_price` int(11) NOT NULL COMMENT '价格',
  `pt_name` varchar(300) COLLATE utf8_bin NOT NULL COMMENT '名称',
  `pt_description` text COLLATE utf8_bin COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `sys_tag`
--

CREATE TABLE IF NOT EXISTS `sys_tag` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tag_name` varchar(300) COLLATE utf8_bin NOT NULL COMMENT '标签名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `sys_tag_value`
--

CREATE TABLE IF NOT EXISTS `sys_tag_value` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tag_id` bigint(20) NOT NULL COMMENT '标签ID',
  `post_id` bigint(20) NOT NULL COMMENT '数据ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `sys_theme`
--

CREATE TABLE IF NOT EXISTS `sys_theme` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `te_name` varchar(300) COLLATE utf8_bin NOT NULL COMMENT '主题文件夹名',
  `te_title` varchar(300) COLLATE utf8_bin NOT NULL COMMENT '主题名称',
  `te_description` text COLLATE utf8_bin NOT NULL COMMENT '主题描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `sys_theme`
--

INSERT INTO `sys_theme` (`id`, `te_name`, `te_title`, `te_description`) VALUES
(1, 'default', '默认主题', '网站默认经典主题。');

-- --------------------------------------------------------

--
-- 表的结构 `sys_user`
--

CREATE TABLE IF NOT EXISTS `sys_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_group` bigint(20) NOT NULL COMMENT '用户组外键',
  `user_username` varchar(30) COLLATE utf8_bin NOT NULL COMMENT '用户名',
  `user_password` char(40) COLLATE utf8_bin NOT NULL COMMENT '加密密码',
  `user_name` varchar(60) COLLATE utf8_bin NOT NULL COMMENT '名称',
  `user_email` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '邮箱',
  `user_lasttime` datetime NOT NULL COMMENT '最后一次登陆时间',
  `user_ip` varchar(39) COLLATE utf8_bin NOT NULL COMMENT '登陆IP',
  `user_remember` tinyint(4) NOT NULL COMMENT '记住我',
  `user_session` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '用户登陆session_id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `user_group` (`user_group`),
  KEY `user_group_2` (`user_group`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `sys_user`
--

INSERT INTO `sys_user` (`id`, `user_group`, `user_username`, `user_password`, `user_name`, `user_email`, `user_lasttime`, `user_ip`, `user_remember`, `user_session`) VALUES
(1, 1, 'visitor', 'dd94709528bb1c83d08f3088d4043f4742891f4f', '访客', 'visit@visitor.com', '2013-08-01 09:49:28', '::1', 0, NULL),
(2, 2, 'admin', 'dd94709528bb1c83d08f3088d4043f4742891f4f', '管理员', 'admin@admin.com', '2013-08-02 15:44:30', '::1', 0, '');

-- --------------------------------------------------------

--
-- 表的结构 `sys_user_group`
--

CREATE TABLE IF NOT EXISTS `sys_user_group` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `group_name` varchar(300) COLLATE utf8_bin NOT NULL COMMENT '名称',
  `group_power` text COLLATE utf8_bin COMMENT '权限',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `sys_user_group`
--

INSERT INTO `sys_user_group` (`id`, `group_name`, `group_power`) VALUES
(1, '访客', NULL),
(2, '管理员', 'VISITORS,USER-SELF,USER,USER-GROUP,LOG,BACKUP,RESTORE,BASIC,NEWS,PRO,SORT,FILE,ORDER,ORDER-CHECK,CANDIDATES,CANDIDATES-CHECK,COMMENT,STATISTICS');

-- --------------------------------------------------------

--
-- 表的结构 `sys_views`
--

CREATE TABLE IF NOT EXISTS `sys_views` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `v_ip` varchar(39) COLLATE utf8_bin NOT NULL COMMENT 'IP地址',
  `v_url` varchar(300) COLLATE utf8_bin NOT NULL COMMENT 'URL',
  `v_date` datetime NOT NULL COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=60 ;
