<?php

/**
 * 访客动作处理器
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package page
 */
//引入配置文件
require('config.php');

//跳转初始化URL
$url = 'error.php?e=0';

//判定动作
if (isset($_GET['action']) == true && isset($_GET['rand']) == true) {
    //判定vistor-rand
    if ($plugVistorPost->isRand($_GET['rand']) == true) {
        //访客记录
        require(DIR_LIB . DS . 'sys-views.php');
        $sysViews = new SysViews($coreDB, $sysLog);
        $sysViews->add($coreIP->ip);
        //引入filter处理类
        require(DIR_LIB . DS . 'core-filter.php');
        $coreFilter = new CoreFilter();
        //引入post处理类
        require(DIR_LIB . DS . 'sys-post.php');
        $sysPost = new SysPost($coreDB, $sysLog);
        switch ($_GET['action']) {
            case 'candadites':
                //引入post处理类
                require(DIR_LIB . DS . 'sys-candadites.php');
                $sysCandadites = new SysCandadites($coreDB, $sysLog, CANDADITES_TIME_LIMIT);
                //验证和过滤数据
                $postName = isset($_POST['name']) ? $_POST['name'] : '';
                $postContact = isset($_POST['contact']) ? $_POST['contact'] : 0;
                $postResume = isset($_POST['resume']) ? $_POST['resume'] : '';
                $postName = $coreFilter->getString($postName, 10, 1, true, false);
                $postContact = $coreFilter->getString($postContact, 200, 0, true);
                $postResume = $coreFilter->getString($postResume, 3000, 0, true);
                $url = 'candadites.php?m=add-failed';
                if ($postName && $postContact && $postResume) {
                    $postRes = $sysCandadites->add($postResume, $postContact, $postName, $coreIP->ip);
                    if ($postRes > 0) {
                        $url = 'candadites.php?m=add-success';
                    }
                }
                break;
            case 'comment':
                $url = 'text.php?id=' . $textID . '&msg=comment-failed#comment';
                if (isset($_GET['text']) == true && isset($_POST['name']) == true && isset($_POST['email']) == true && isset($_POST['url']) == true && isset($_POST['content']) == true) {
                    $textID = (int) $_GET['text'];
                    if ($sysPost->get($textID)) {
                        //引入comment处理类
                        require(DIR_LIB . DS . 'sys-comment.php');
                        $sysComment = new SysComment($coreDB, $sysLog);
                        //添加评论
                        $name = $coreFilter->getString($_POST['name'], 10, 1, true);
                        $email = $coreFilter->getEmail($_POST['email'], 100);
                        $commentURL = $coreFilter->getURL($_POST['url'], 100);
                        $content = $coreFilter->getString($_POST['content'], 200, 0, true);
                        if ($name && $email && $commentURL && $content) {
                            $res = $sysComment->add($textID, 0, $coreIP->ip, $name, $email, $commentURL, $content);
                            if ($res > 0) {
                                $url = 'text.php?id=' . $textID . '&msg=comment-success#comment';
                            }
                        }
                    }
                }
                break;
            case 'order':
                $url = 'shop.php?msg=order-failed';
                if (isset($_POST['name']) == true && isset($_POST['contact']) == true && isset($_POST['note']) == true) {
                    //引入product处理类
                    require(DIR_LIB . DS . 'sys-product.php');
                    $sysProduct = new SysProduct($coreDB, $sysLog);
                    //引入order处理类
                    require(DIR_LIB . DS . 'sys-order.php');
                    $sysOrder = new SysOrder($coreDB, $sysLog, $coreSession, $sysProduct);
                    //执行订单
                    $name = $coreFilter->getString($_POST['name'], 10, 1, true);
                    $contact = $coreFilter->getString($_POST['contact'], 200, 0, true);
                    $note = $coreFilter->getString($_POST['note'], 500, 0, true);
                    if ($name && $contact && $note) {
                        $res = $sysOrder->order($coreIP->ip, $name, $contact, $note);
                        if ($res) {
                            $sysOrder->clearBuy();
                            $url = 'shop.php?msg=order-success';
                        }
                    }
                }
                break;
            case 'buy':
                if (isset($_GET['product']) == true && isset($_GET['text']) == true) {
                    $productID = $coreFilter->getInt($_GET['product']);
                    $textID = $coreFilter->getInt($_GET['text']);
                    //引入product处理类
                    require(DIR_LIB . DS . 'sys-product.php');
                    $sysProduct = new SysProduct($coreDB, $sysLog);
                    //引入order处理类
                    require(DIR_LIB . DS . 'sys-order.php');
                    $sysOrder = new SysOrder($coreDB, $sysLog, $coreSession, $sysProduct);
                    //添加购物车
                    $sysOrder->addBuy($productID);
                    $url = 'text.php?id=' . $textID . '&msg=buy-success';
                }
                break;
            case 'clear-buy':
                //引入product处理类
                require(DIR_LIB . DS . 'sys-product.php');
                $sysProduct = new SysProduct($coreDB, $sysLog);
                //引入order处理类
                require(DIR_LIB . DS . 'sys-order.php');
                $sysOrder = new SysOrder($coreDB, $sysLog, $coreSession, $sysProduct);
                if (isset($_GET['product']) == true) {
                    //删除购物车某一项
                    $productID = $coreFilter->getInt($_GET['product']);
                    $sysOrder->clearBuy($productID);
                    $url = 'shop.php';
                } else {
                    //清空购物车
                    $sysOrder->clearBuy();
                    $url = 'shop.php';
                }
                break;
        }
    }
}
CoreHeader::toURL($url);
?>
