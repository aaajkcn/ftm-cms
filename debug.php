<?php

/**
 * debug
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package debug
 */
require('config.php');

//输出测试session
print_r($_SESSION);
$_SESSION = null;

//清理session
session_unset();
session_destroy();
session_regenerate_id(true);
?>
