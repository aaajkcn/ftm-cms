<?php

/**
 * 全局配置文件
 * @author fotomxq <fotomxq.me>
 * @version 2
 * @package sys
 */
//////////////////
//路径定义
//////////////////
//路径分隔符
define('DS', DIRECTORY_SEPARATOR);
//绝对路径
define('DIR_ROOT', dirname(__FILE__) . DS);
//用户数据
define('DIR_DATA', DIR_ROOT . 'content');
//库路径
define('DIR_LIB', DIR_ROOT . 'includes');

//////////////////
//目录设定
//////////////////
//备份目录
define('DIR_BACKUP', DIR_DATA . DS . 'backup');
//缓冲目录
define('DIR_CACHE', DIR_DATA . DS . 'cache');
//日志目录
define('DIR_LOG', DIR_DATA . DS . 'log');
//上传文件目录
define('DIR_UPLOAD', DIR_DATA . DS . 'uploads');

//////////////////
//上传文件设定
//////////////////
//允许的文件类型
define('UPLOAD_TYPE', 'jpg,png,gif,wmp,zip,rar,pdf,txt,mp3,mp4,avi,mpeg');
//拒绝的文件类型
define('UPLOAD_BAN_TYPE', 'exe,bat,sh,php,html,htm,msi');
//允许的图片文件类型
define('UPLOAD_IMG_TYPE', 'jpg,png');
//允许的个人简历文件类型
define('UPLOAD_CANDADITES_TYPE', 'doc,docx');
//是否开启总白名单
define('UPLOAD_TYPE_ON', false);
//是否开启黑名单
define('UPLOAD_BAN_TYPE_ON', true);
//是否开启图片白名单
define('UPLOAD_IMG_TYPE_ON', true);
//最大文件大小 (KB)
define('UPLOAD_SIZE_MAX', 51200);
//如果图片超出尺寸是否自动压缩图片
define('UPLOAD_IMG_SIZE_P_ON', true);
//图片最大尺寸
define('UPLOAD_IMG_SIZE_W', 3000);
define('UPLOAD_IMG_SIZE_H', 3000);
//是否直接跳转到文件下载，否则通过脚本下载
define('UPLOAD_DOWN_PHP', true);

//////////////////
//缓冲器
//////////////////
//缓冲器开关
define('CACHE_ON', true);
//失效时间长度 ( 秒 )
define('CACHE_LIMIT_TIME', 86400);

//////////////////
//在线应聘
//////////////////
//在线应聘开关
define('CANDADITES_ON', true);
//提交应聘时间间隔 (s)
define('CANDADITES_TIME_LIMIT', 86400);

//////////////////
//购物
//////////////////
//在线购物开关
define('ORDER_ON', true);
//提交订单时间间隔 (s)
define('ORDER_TIME_LIMIT', 60);

//////////////////
//评论
//////////////////
//评论开关
define('COMMENT_ON', true);
//提交评论时间间隔 (s)
define('COMMENT_TIME_LIMIT', 60);
//评论审核开关
define('COMMENT_AUDIT_ON', true);

//////////////////
//其他设定
//////////////////
//URL
define('WEB_URL', 'http://localhost/');
//Debug模式开关
define('DEBUG_ON', true);
//网站开关，超级开关，关闭后后台也无法使用
define('WEB_ON', true);
//默认访客用户 (该用户不能被删除，且被用于上传和其他系统创建任务)
define('VISITOR_USER', 1);
//默认访客用户组，作用同上 (如果需要自定义，请勿赋予任何权限)
define('VISITOR_USER_GROUP', 1);

/**
 * 定义时区
 */
date_default_timezone_set('PRC');

/**
 * 数据库定义
 */
//PDO-DSN eg: mysql:host=localhost;dbname=databasename;charset=utf8
$dbDSN = 'mysql:host=localhost;dbname=diningbio;charset=utf8';
//数据库用户名
$dbUser = 'admin';
//数据库密码
$dbPasswd = 'admin';
//是否持久化连接
$dbPersistent = true;
//连接编码
$dbEncoding = 'UTF8';

/**
 * 引用必备库
 */
//错误处理模块
require(DIR_LIB . DS . 'core-error.php');

//头信息类
require(DIR_LIB . DS . 'core-header.php');

//缓冲器
require(DIR_LIB . DS . 'core-cache.php');
$coreCache = new CoreCache(DIR_CACHE, CACHE_LIMIT_TIME, CACHE_ON);

//Session处理器
require(DIR_LIB . DS . 'core-session.php');
$coreSession = new CoreSession();

//访客提交处理器
require(DIR_LIB . DS . 'plug-vistor-post.php');
$plugVistorPost = new PlugVistorPost($coreSession);

//数据库
require(DIR_LIB . DS . 'core-db.php');
$coreDB = new CoreDB($dbDSN, $dbPasswd, $dbUser, $dbPersistent, $dbEncoding);

//文件操作类
require(DIR_LIB . DS . 'core-file.php');
CoreFile::$ds = DS;

//配置操作类
require(DIR_LIB . DS . 'sys-config.php');
$sysConfig = new SysConfig($coreDB);

//IP类
require(DIR_LIB . DS . 'core-ip.php');
$ipConfigs = $sysConfig->load(array('IP_BAN_ON', 'IP_BAN_LIST', 'IP_WHITE_ON', 'IP_WHITE_LIST'));
$coreIP = new CoreIP($ipConfigs['IP_BAN_LIST'], $ipConfigs['IP_WHITE_LIST']);
if ($ipConfigs['IP_BAN_ON'] == '1') {
    if ($coreIP->isBan() == true) {
        die();
    }
}
if ($ipConfigs['IP_WHITE_ON'] == '1') {
    if ($coreIP->isWhite() == false) {
        die();
    }
}

//日志类
require(DIR_LIB . DS . 'sys-log.php');
$sysLog = new sysLog($coreDB, $coreIP->ip, DIR_LOG);
?>
