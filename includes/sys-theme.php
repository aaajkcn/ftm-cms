<?php

/**
 * 主题操作类
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package sys
 */
class SysTheme {

    /**
     * 数据表名称
     * @var string 
     */
    private $tableName;

    /**
     * 数据库句柄
     * @var CoreDB 
     */
    private $db;

    /**
     * 日志句柄
     * @var SysLog 
     */
    private $log;

    /**
     * 配置句柄
     * @var SysConfig
     */
    private $config;

    /**
     * 当前选择配置名称
     * @var string 
     */
    private $configSelectedName = 'THEME_SELECTED';

    /**
     * 初始化
     * @param CoreDB $db 数据库句柄
     * @param SysLog $log 日志句柄
     * @param SysConfig $config 配置句柄
     */
    public function __construct(&$db, &$log, $config) {
        $this->db = $db;
        $this->log = $log;
        $this->config = $config;
        $this->tableName = $this->db->tables['theme'];
        $this->config = $config;
    }

    /**
     * 获取当前主题
     * @return string
     */
    public function getNow($infoBool = false) {
        $themeID = $this->config->load($this->configSelectedName);
        $res = $this->get($themeID);
        if ($res) {
            if ($infoBool == true) {
                return $res;
            } else {
                return $res['te_name'];
            }
        }
        return false;
    }

    /**
     * 获取主题信息
     * @param int $target 目标ID
     * @return array 数据数组
     */
    public function get($target) {
        $sql = 'SELECT `id`,`te_name`,`te_title`,`te_description` FROM `' . $this->tableName . '` WHERE `id` = :id';
        $attrs = array(':id' => array($target, PDO::PARAM_INT));
        return $this->db->prepareAttr($sql, $attrs, 1, PDO::FETCH_ASSOC);
    }

    /**
     * 获取主题列表
     * @return array 主题列表数据数组
     */
    public function getList() {
        $sql = 'SELECT `id`,`te_name`,`te_title`,`te_description` FROM `' . $this->tableName . '` ORDER BY `id` ASC';
        return $this->db->prepareAttr($sql, null, 3, PDO::FETCH_ASSOC);
    }

    /**
     * 添加新的主题包
     * @param string $themeZip 压缩包路径
     * @return boolean 是否成功
     */
    public function add($themeZip) {
        $this->addLog('Add theme failed.');
        return false;
    }

    /**
     * 选择主题
     * @param int $themeID 主题ID
     * @return boolean 是否成功
     */
    public function select($themeID) {
        $this->addLog('Select theme,by ID : ' . $themeID);
        return $this->config->save($this->configSelectedName, $themeID);
    }

    /**
     * 还原默认主题
     * @return boolean 是否成功
     */
    public function restore() {
        $this->addLog('Restore theme.');
        return $this->config->restore($this->configSelectedName);
    }

    /**
     * 添加日志
     * @param string $message 日志消息
     */
    private function addLog($message) {
        $this->log->add($message);
    }

}

?>
