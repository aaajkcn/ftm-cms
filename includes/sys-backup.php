<?php

/**
 * 备份操作类
 * <p>需要额外扩展 : CoreFile</p>
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package sys
 */
class SysBackup {

    /**
     * 数据库句柄
     * @var CoreDB 
     */
    private $db;

    /**
     * 路径分隔符
     * @var string 
     */
    private $ds;

    /**
     * 临时处理目录
     * @var string 
     */
    private $lsDir;

    /**
     * 备份目录
     * @var string 
     */
    private $backupDir;

    /**
     * 记录数 ( 必须先是用getlist获取后才能使用 )
     * @var int 
     */
    public $row;

    /**
     * 初始化
     * @param CoreDB $db 数据库句柄
     * @param string $lsDir 临时目录
     * @param string $backupDir 备份目录
     * @param string $ds 路径分隔符
     */
    public function __construct(&$db, $lsDir, $backupDir, $ds = '/') {
        $this->db = $db;
        $this->lsDir = $lsDir;
        $this->backupDir = $backupDir;
        $this->ds = $ds;
    }

    /**
     * 获取备份文件列表
     * @param int $page 页数
     * @param int $max 页长
     * @return array 列表数据，如果找不到则返回null
     */
    public function getList($page = 1, $max = 10) {
        $search = $this->backupDir . $this->ds . '*.zip';
        $fileList = CoreFile::searchDir($search);
        if ($fileList) {
            $this->row = count($fileList);
            krsort($fileList);
            $step = ($page - 1) * $max;
            $fileListSlice = array_slice($fileList, $step, $max);
            foreach ($fileListSlice as $k => $v) {
                $vName = basename($v);
                $vTimeArr = getdate(substr($vName, 0, -4));
                $vTime = $vTimeArr['year'] . '-' . $vTimeArr['mon'] . '-' . $vTimeArr['mday'] . ' ' . $vTimeArr['hours'] . ':' . $vTimeArr['minutes'] . ':' . $vTimeArr['seconds'];
                $vSize = ceil(filesize($v) / 1024) . ' KB';
                $fileListSlice[$k] = array($vName, $vTime, $vSize);
            }
            return $fileListSlice;
        }
        return null;
    }

    /**
     * 备份数据库
     * @param array $dirs 文件数据 eg: array('content/files','content/logs',...)
     * @return string 备份文件路径
     */
    public function backup($dirs) {
        //建立临时目录
        $lsDir = $this->lsDir;
        $lsDataDir = $lsDir . $this->ds . 'content';
        if (CoreFile::newDir($lsDir) == true) {
            if (CoreFile::newDir($lsDataDir) == false) {
                CoreFile::deleteDir($lsDir);
                return false;
            }
            //将数据文件拷贝到指定目录
            foreach ($dirs as $dirV) {
                $vBaseName = basename($dirV);
                if (CoreFile::copyDir($dirV, $lsDataDir . $this->ds . $vBaseName) == false) {
                    CoreFile::deleteDir($lsDir);
                    return false;
                }
            }
            //建立SQL目录
            $lsSQLDir = $lsDir . $this->ds . 'sql';
            if (CoreFile::newDir($lsSQLDir) == false) {
                CoreFile::deleteDir($lsDir);
                return false;
            }
            //遍历数据表
            $tables = $this->db->tables;
            foreach ($tables as $tableV) {
                $fields = null;
                //建立表目录
                $lsSQLTableDir = $lsSQLDir . $this->ds . $tableV;
                if (CoreFile::newDir($lsSQLTableDir) == false) {
                    CoreFile::deleteDir($lsDir);
                    return false;
                }
                //构建sql文件名
                $lsSQLRowFile = $lsSQLTableDir . $this->ds . $tableV;
                //遍历所有记录
                $step = 0;
                $max = 0;
                $sql = 'SELECT * FROM `' . $tableV . '` LIMIT ';
                //获取当前表字段
                $sqlField = 'SELECT DISTINCT COLUMN_NAME as name from information_schema.columns where table_name=\'' . $tableV . '\'';
                $resField = $this->db->prepareAttr($sqlField, null, 3, PDO::FETCH_ASSOC);
                if ($resField) {
                    foreach ($resField as $v) {
                        $fields[] = $v['name'];
                    }
                }
                //计算表内记录数，以判定步长
                $sqlMax = 'SELECT AVG( LENGTH(`' . implode('`))+AVG(LENGTH(`', $fields) . '`)) as al FROM `' . $tableV . '`';
                $resMax = $this->db->prepareAttr($sqlMax, null, 2, 0);
                if ($resMax > 50000) {
                    $max = 1;
                } elseif ($resMax > 20000) {
                    $max = 10;
                } elseif ($resMax > 5000) {
                    $max = 20;
                } else {
                    $max = 50;
                }
                //构建基本插入语句部分
                $sqlInsert = 'INSERT INTO `' . $tableV . '`(`' . implode('`,`', $fields) . '`) VALUES';
                //必须确保存在内容，才可以进行数据读出
                $res = null;
                if ($max > 1 && $fields) {
                    do {
                        //读取数据
                        $lsSQL = $sql . $step * $max . ',' . $max;
                        $res = $this->db->prepareAttr($lsSQL, null, 3, PDO::FETCH_ASSOC);
                        if ($res) {
                            $content = '';
                            //如果存在数据，则构建SQL插入语句
                            foreach ($res as $resV) {
                                $content .= '(';
                                foreach ($resV as $resVV) {
                                    if ($resVV == null) {
                                        $content .= 'NULL,';
                                    } elseif (is_int($resVV) == true) {
                                        $content .= $resVV . ',';
                                    } else {
                                        $trans = array("'" => "\'", "\\'" => "\'");
                                        $resVV = strtr($resVV, $trans);
                                        $content .= '\'' . $resVV . '\',';
                                    }
                                }
                                $content = substr($content, 0, -1);
                                $content .= '),';
                            }
                            $content = substr($content, 0, -1) . ';';
                            $content = $sqlInsert . $content;
                            //保存文件
                            $lsSQLRowFileV = $lsSQLRowFile . '_' . $step . '.sql';
                            if (CoreFile::saveFile($lsSQLRowFileV, $content, true) == false) {
                                CoreFile::deleteDir($lsDir);
                                return false;
                            }
                        }
                        //进一步
                        $step++;
                    } while ($res);
                }
            }
        }
        //生成压缩包
        $backupFile = $this->backupDir . $this->ds . time() . '.zip';
        if (CoreFile::createZip($lsDir, $backupFile) == false) {
            if (CoreFile::isFile($backupFile) == true) {
                CoreFile::deleteFile($backupFile);
            }
            CoreFile::deleteDir($lsDir);
            return false;
        }
        //删除临时文件夹
        CoreFile::deleteDir($lsDir);
        return true;
    }

    /**
     * 还原数据库
     * @param string $backup 备份文件
     * @param array $dirs 文件数据数组 eg: array('content/files','content/logs',...)
     * @return boolean 是否成功
     */
    public function restore($backup, $dirs) {
        //解压数据
        $lsDir = $this->lsDir;
        if (CoreFile::isFile($backup) == false) {
            return false;
        }
        if (CoreFile::extractZip($backup, $lsDir) == false) {
            return false;
        }
        //重新定位临时目录
        $search = $lsDir . CoreFile::$ds . '*';
        $searchDirs = CoreFile::searchDir($search, GLOB_ONLYDIR);
        if ($searchDirs) {
            $lsDir = $searchDirs[0];
        }
        //清空并复制文件数据
        foreach ($dirs as $v) {
            $vName = basename($v);
            $bPath = $lsDir . $this->ds . 'content' . $this->ds . $vName;
            if (CoreFile::isDir($bPath) == true) {
                if (CoreFile::deleteDir($v) == true) {
                    if (CoreFile::copyDir($bPath, $v) == false) {
                        return false;
                    }
                }
            }
        }
        //遍历表，清空并插入数据
        $tables = $this->db->tables;
        foreach ($tables as $tableV) {
            $tableVDir = $lsDir . $this->ds . 'sql' . $this->ds . $tableV;
            if (CoreFile::isDir($tableVDir) == true) {
                $sqlTrash = 'TRUNCATE ' . $tableV;
                if ($this->db->exec($sqlTrash) !== false) {
                    //查询所有相关文件
                    $sqlFileList = CoreFile::searchDir($tableVDir . $this->ds . '*.sql');
                    if ($sqlFileList) {
                        foreach ($sqlFileList as $sqlFileV) {
                            $sqlContent = CoreFile::loadFile($sqlFileV);
                            if ($sqlContent) {
                                if ($this->db->exec($sqlContent) === false) {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }
        //删除临时文件
        CoreFile::deleteDir($lsDir);
        return true;
    }

}

?>
