<?php

/**
 * 访客提交处理器
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package plug
 */
class PlugVistorPost {

    /**
     * 会话句柄
     * @var CoreSession 
     */
    private $session;

    /**
     * 会话标识名称
     * @var string 
     */
    private $sessionName = 'VISTOR-POST-RAND';

    /**
     * 初始化
     * @param CoreSession $session 会话句柄
     */
    public function __construct($session) {
        $this->session = $session;
    }

    /**
     * 获取随机值
     * @return string 值
     */
    public function get() {
        $rand = substr(sha1(rand(1, 9999999)), 10, 10);
        $this->saveSession($rand);
        return $rand;
    }

    /**
     * 确定是否匹配
     * <p>判断完成后即失效。</p>
     * @param string $rand 用户提交的值
     * @return boolean 值是否匹配
     */
    public function isRand($rand) {
        $sessionRand = $this->getSession();
        if ($sessionRand === $rand) {
            $this->saveSession('');
            return true;
        }
        return false;
    }

    /**
     * 保存会话值
     * @param string $value 值
     */
    private function saveSession($value) {
        $this->session->save($this->sessionName, $value);
    }

    /**
     * 获取会话值
     * @return string 值
     */
    private function getSession() {
        return $this->session->get($this->sessionName);
    }

}

?>
