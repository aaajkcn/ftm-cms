<?php

/**
 * 上传文件操作类
 * <p>需要用到额外扩展 : CoreHeader , CoreFile , WEB_URL</p>
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package sys
 */
class SysUpload {

    /**
     * post句柄
     * @var SysPost 
     */
    private $post;

    /**
     * 日志句柄
     * @var SysLog
     */
    private $log;

    /**
     * 文件数据路径
     * @var string 
     */
    private $uploadDir;

    /**
     * 用户ID
     * @var int 
     */
    private $userID;

    /**
     * 初始化
     * @param SysPost $post post句柄
     * @param SysLog $log 日志句柄
     * @param string $uploadDir 文件数据目录
     * @param int $userID 用户ID
     */
    public function __construct(&$post, &$log, $uploadDir, $userID) {
        $this->post = $post;
        $this->log = $log;
        $this->uploadDir = $uploadDir;
        $this->userID = $userID;
    }

    /**
     * 显示图片
     * @param int $id 文件ID
     * @param int $width 最大宽度
     * @param int $height 最大高度
     * @param string $passwd 访问密码
     * @param boolean $phpBool 是否通过脚本显示，否则直接跳到文件
     */
    public function showImage($id, $width = null, $height = null, $passwd = null, $phpBool = false) {
        $res = $this->post->get($id, $passwd, true);
        if ($res && $res['post_type'] == 'file' && ($res['post_meta'] == 'image/jpeg' || $res['post_meta'] == 'image/png')) {
            if ($phpBool == true) {
                $src = $this->getSrc($res['post_url']);
                $img = null;
                CoreHeader::toImg('jpeg');
                switch ($res['post_meta']) {
                    case 'image/jpeg':
                        $img = imagecreatefromjpeg($src);
                        imagejpeg($img);
                        break;
                    case 'image/png':
                        $img = imagecreatefrompng($src);
                        imagepng($img);
                        break;
                }
            } else {
                $url = $this->getURL($res['post_url']);
                CoreHeader::toURL($url);
            }
        }
    }
    
    /**
     * 判断是否为图片文件
     * @param string $meta POST-META或文件META
     * @return boolean 是否为允许图片
     */
    public function isImageType($meta) {
        switch ($meta) {
            case 'image/jpeg':
                return true;
                break;
            case 'image/png':
                return true;
                break;
        }
        return false;
    }

    /**
     * 下载文件
     * @param int $id 文件ID
     * @param string $passwd 访问密码
     * @param boolean $phpBool 是否通过脚本访问，否则直接跳到文件
     */
    public function downloadFile($id, $passwd = null, $phpBool = false) {
        $id = (int) $id;
        $res = $this->post->get($id, $passwd, true);
        if ($res) {
            if ($phpBool == true) {
                $fileSrc = $this->getSrc($res['post_url']);
                if (CoreFile::isFile($fileSrc) == true) {
                    $fileSize = filesize($fileSrc);
                    $fileName = $res['post_title'];
                    if (!$res['post_title']) {
                        $fileName = basename($fileSrc);
                    }
                    CoreHeader::downloadFile($fileSize, $fileName, $fileSrc);
                }
            } else {
                $url = $this->getURL($res['post_url']);
                CoreHeader::toURL($url);
            }
        }
    }

    /**
     * 上传新的文件
     * <p>可以是已上传服务器的文件，如果是，则直接指定其路径即可。</p>
     * @param $_FILES[] $file 上传文件
     * @param int $parent 上一级ID
     * @param boolean $imgBool 是否为图片
     * @return int 新的ID，如果失败则返回0
     */
    public function upload($file, $parent = 0, $imgBool = false) {
        $fileName = '';
        $fileTitle = '';
        $fileTmp = '';
        $fileSize = 0;
        $fileMeta = '';
        if (isset($file['tmp_name']) == true && is_uploaded_file($file['tmp_name']) == true) {
            $fileName = $file['name'];
            $fileTitle = $file['name'];
            $fileTmp = $file['tmp_name'];
            $fileMeta = $file['type'];
            $fileSize = $file['size'];
        } else {
            if (is_string($file) == true) {
                $fileName = $file;
                $fileTitle = basename($file);
                $fileTmp = $file;
                $fileMeta = mime_content_type($file);
                $fileSize = filesize($file);
            } else {
                return 0;
            }
        }
        $dateYm = date('Ym');
        $dateD = date('d');
        $dateHis = date('His');
        $dir = $this->createDir($dateYm, $dateD);
        if ($dir && $this->checkUploadSize($fileSize) == true) {
            $fileSha1 = sha1_file($fileTmp);
            $fileParts = pathinfo($fileName);
            $fileParts['extension'] = strtolower($fileParts['extension']);
            if ($fileParts && $this->checkUploadType($fileParts['extension']) == true) {
                $fileType = $fileParts['extension'];
                if ($imgBool == true) {
                    //$fileMeta = 'image/jpg';
                    //$fileType = 'jpg';
                }
                $fileSrc = $dir . CoreFile::$ds . $dateYm . $dateD . $dateHis . '_' . $fileSha1 . '.' . $fileType;
                $fileSrcValue = $dateYm . CoreFile::$ds . $dateD . CoreFile::$ds . $dateYm . $dateD . $dateHis . '_' . $fileSha1 . '.' . $fileType;
                if ($imgBool == true && $this->checkUploadImageType($fileParts['extension']) == true) {
                    $maxWidth = $this->getUploadImageWidth();
                    $maxHeight = $this->getUploadImageHeight();
                    if ($this->saveImageScale($fileTmp, $fileSrc, $maxWidth, $maxHeight, $fileParts['extension']) == false) {
                        return 0;
                    }
                } else {
                    if (CoreFile::moveUpload($fileTmp, $fileSrc) == false) {
                        return 0;
                    }
                }
                $id = $this->post->add($this->userID, $fileTitle, '', $fileSrcValue, 'public', $parent, 0, null, $this->post->type[1], $fileMeta);
                if ($id > 0) {
                    $this->addLog('Upload file success , by id : ' . $id . ' , src : ' . $fileSrc);
                }
                return $id;
            }
        }
        return 0;
    }

    /**
     * 删除文件
     * @param int $target 目标ID
     * @param string $passwd 访问密码
     * @return boolean 是否成功
     */
    public function del($target, $passwd = null) {
        $res = $this->post->get($target, $passwd);
        if ($res) {
            $src = $this->getSrc($res['post_url']);
            if (CoreFile::isFile($src) == true) {
                if (CoreFile::deleteFile($src) == false) {
                    return false;
                }
                $this->addLog('Delete upload file , by id : ' . $res['id'] . ' , src : ' . $src);
            }
            return $this->post->del($target);
        }
        return false;
    }

    /**
     * 获取文件路径
     * @param string $url post-url
     * @return string 文件路径
     */
    private function getSrc($url) {
        return $this->uploadDir . CoreFile::$ds . $url;
    }

    /**
     * 保存并压缩图像
     * @param string $src 源文件路径
     * @param string $dest 目标文件路径
     * @param int $maxWidth 最大宽度
     * @param int $maxHeight 最大高度
     * @param string $type 文件类型
     * @return boolean 是否成功
     */
    private function saveImageScale($src, $dest, $maxWidth, $maxHeight, $type) {
        //建立图像句柄
        $img;
        switch ($type) {
            case 'jpg':
                $img = imagecreatefromjpeg($src);
                break;
            case 'png':
                $img = imagecreatefrompng($src);
                break;
            default:
                return false;
        }
        if ($img) {
            //获取大小
            $width = imagesx($img);
            $height = imagesy($img);
            //判断是否超出限制
            if (($width >= $maxWidth || $height >= $maxHeight) && $this->getUploadImageScaleOn() == true) {
                //计算压缩比
                $p = 0;
                $newWidth = $width;
                $newHeight = $height;
                if ($width > $height) {
                    $p = $width - $maxWidth;
                } else {
                    $p = $height - $maxHeight;
                }
                $newWidth = $width - $p;
                $newHeight = $height - $p;
                //转换图像
                $img2 = imagecreatetruecolor($newWidth, $newHeight);
                imagecopyresized($img, $img2, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
                $img = $img2;
            }
            //保存文件
            switch ($type) {
                case 'jpg':
                    return imagejpeg($img, $dest);
                    break;
                case 'png':
                    return imagepng($img, $dest);
                    break;
                default:
                    return false;
            }
        }
        return false;
    }

    /**
     * 获取文件URL
     * @param string $src 文件路径
     * @return string URL
     */
    private function getURL($src) {
        return WEB_URL . '/content/uploads/' . $src;
    }

    /**
     * 检查上传文件是否超出大小
     * @param int $size 上传文件大小
     * @return boolean 是否通过
     */
    private function checkUploadSize($size) {
        $max = UPLOAD_SIZE_MAX * 1024;
        if ($size <= $max && $size >= 1) {
            return true;
        }
        return false;
    }

    /**
     * 检查上传文件类型是否允许
     * @param string $type 上传文件类型
     * @return boolean 是否允许
     */
    private function checkUploadType($type) {
        $typeWhiteList = $this->getUploadType();
        $typeBanList = $this->getUploadBanType();
        $typeWhiteOn = $this->getUploadTypeOn();
        $typeBanOn = $this->getUploadBanTypeOn();
        if ((($typeWhiteOn == true && in_array($type, $typeWhiteList) == true) || $typeWhiteOn == false) && (($typeBanOn == true && in_array($type, $typeBanList) == false) || $typeBanOn == false)) {
            return true;
        }
        return false;
    }

    /**
     * 检查上传图片文件类型是否允许
     * @param string $type 上传文件类型
     * @return boolean 是否允许
     */
    private function checkUploadImageType($type) {
        $typeList = $this->getUploadImageType();
        $typeOn = $this->getUploadImageTypeOn();
        if ($typeOn == false || ($typeOn == true && in_array($type, $typeList) == true)) {
            return true;
        }
        return false;
    }

    /**
     * 获取是否压缩图片
     * @return boolean
     */
    private function getUploadImageScaleOn() {
        return UPLOAD_IMG_SIZE_P_ON;
    }

    /**
     * 获取允许上传的图片图片宽度
     * @return int 宽度(px)
     */
    private function getUploadImageWidth() {
        return UPLOAD_IMG_SIZE_W;
    }

    /**
     * 获取允许上传的图片图片宽度
     * @return int 宽度(px)
     */
    private function getUploadImageHeight() {
        return UPLOAD_IMG_SIZE_H;
    }

    /**
     * 获取允许上传的图片文件类型
     * @return array 类型数组
     */
    private function getUploadImageType() {
        return explode(',', UPLOAD_IMG_TYPE);
    }

    /**
     * 获取允许上传的文件类型
     * @return array 类型数组
     */
    private function getUploadType() {
        return explode(',', UPLOAD_TYPE);
    }

    /**
     * 获取上传黑名单
     * @return array 类型数组
     */
    private function getUploadBanType() {
        return explode(',', UPLOAD_BAN_TYPE);
    }

    /**
     * 获取类型白名单开关
     * @return boolean 是否开启
     */
    private function getUploadTypeOn() {
        return UPLOAD_TYPE_ON;
    }

    /**
     * 获取类型黑名单开关
     * @return boolean 是否开启
     */
    private function getUploadBanTypeOn() {
        return UPLOAD_BAN_TYPE_ON;
    }

    /**
     * 获取图片白名单开关
     * @return boolean 是否开启
     */
    private function getUploadImageTypeOn() {
        return UPLOAD_IMG_TYPE_ON;
    }

    /**
     * 创建目录
     * @param int $dateYm 年月
     * @param int $dateD 日
     * @return string 目录路径
     */
    private function createDir($dateYm, $dateD) {
        $dir = $this->uploadDir . CoreFile::$ds . $dateYm . CoreFile::$ds . $dateD;
        if (CoreFile::newDir($dir) == true) {
            return $dir;
        }
        return '';
    }

    /**
     * 执行日志
     * @param string $message 日志消息
     */
    private function addLog($message) {
        $this->log->add($message);
    }

}

?>
