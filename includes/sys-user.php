<?php

/**
 * 用户处理类
 * @author fotomxq <fotomxq.me>
 * @version 2
 * @package sys
 */
class SysUser {

    /**
     * 用户表名称
     * @var string 
     */
    private $tableNameUser;

    /**
     * 用户组表名称
     * @var string 
     */
    private $tableNameGroup;

    /**
     * 数据库句柄
     * @var CoreDB 
     */
    private $db;

    /**
     * 登陆Session变量名称
     * @var string 
     */
    private $loginSessionName = 'login';

    /**
     * 登陆Cookie变量名称
     * @var string 
     */
    private $loginCookieName = 'login';

    /**
     * Session操作句柄
     * @var CoreSession
     */
    private $session;

    /**
     * 日志操作句柄
     * @var SysLog
     */
    private $log;

    /**
     * 用户字段
     * @var array 
     */
    private $userFieldsArr = array('id', 'user_group', 'user_username', 'user_password', 'user_name', 'user_email', 'user_lasttime', 'user_ip', 'user_remember', 'user_session');

    /**
     * 用户组字段
     * @var array 
     */
    private $groupFieldsArr = array('id', 'group_name', 'group_power');

    /**
     * 用户全部字段
     * @var string 
     */
    private $userFields;

    /**
     * 用户组全部字段
     * @var string
     */
    private $groupFields;

    /**
     * 登录超时(秒)
     * @var int 
     */
    private $loginTime;

    /**
     * cookie过期时间
     * @var int
     */
    private $cookieTime;

    /**
     * IP地址
     * @var string 
     */
    private $ip;

    /**
     * 当前用户权限列
     * @var array 
     */
    private $loggedUserPowers;

    /**
     * 当前登录用户组
     * @var array 
     */
    private $loggedUserGroup;

    /**
     * 权限
     * @var array 
     */
    public $powers = array(
        'VISITORS' => '访问后台',
        'USER-SELF' => '个人中心',
        'USER' => '用户管理',
        'USER-GROUP' => '用户组管理',
        'LOG' => '日志',
        'BACKUP' => '备份数据库',
        'RESTORE' => '还原数据库',
        'BASIC' => '基本设置',
        'NEWS' => '新闻管理',
        'PRO' => '产品管理',
        'SORT' => '分类管理',
        'FILE' => '文件管理',
        'ORDER' => '订单管理',
        'ORDER-CHECK' => '订单审核',
        'CANDIDATES' => '招聘信息管理',
        'CANDIDATES-CHECK' => '应聘审核',
        'COMMENT' => '评论管理',
        'STATISTICS' => '站点统计'
    );

    /**
     * 初始化
     * @param CoreDB $db 数据库句柄
     * @param CoreSession $session Session句柄
     * @param SysLog $log 日志句柄
     * @param string $ip IP地址
     * @param int $loginTime 登录超时时间(秒)
     * @param int $cookieTime cookie过期时间
     */
    public function __construct(&$db, &$session, &$log, $ip, $loginTime = 1500, $cookieTime = 2592000) {
        $this->db = $db;
        $this->tableNameUser = $db->tables['user'];
        $this->tableNameGroup = $db->tables['userg'];
        $this->session = $session;
        $this->log = $log;
        $this->ip = $ip;
        $this->loginTime = $loginTime;
        //cookie过期时间
        $this->cookieTime = time() + $cookieTime;
        $this->userFields = '`' . implode('`,`', $this->userFieldsArr) . '`';
        $this->groupFields = '`' . implode('`,`', $this->groupFieldsArr) . '`';
    }

    /**
     * 登陆
     * @param string $user 用户名
     * @param string $passwd 密码明文
     * @param boolean $remember 是否记住登陆
     * @return boolean 是否登陆成功
     */
    public function login($user, $passwd, $remember = false) {
        if ($this->logged() == true) {
            return true;
        } else {
            if ($this->checkUserName($user) == true && $this->checkUserPasswd($passwd) == true) {
                $sql = 'SELECT ' . $this->userFields . ' FROM `' . $this->tableNameUser . '` WHERE `user_username` = :user';
                $attrs = array(':user' => array($user, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT));
                $res = $this->doSQL($sql, $attrs, 1, PDO::FETCH_ASSOC);
                if ($res) {
                    $passwdEncrypt = $this->getEncrypt($passwd);
                    //如果密码正确
                    if ($res['user_password'] === $passwdEncrypt) {
                        $this->setLoginSession($res['id']);
                        $this->updateLoginTime($res['id']);
                        $this->updateLoginSession($res['id']);
                        //如果需要记住我
                        if ($remember == true) {
                            $this->setLoginCookie($res['id']);
                            $this->setRemember($res['id'], 1);
                        }
                        $this->addLog('User Login success,by ID : ' . $res['id']);
                        return true;
                    }
                }
            }
            return false;
        }
    }

    /**
     * 是否已登陆
     * @return boolean|array 是否已经登陆，如果登录则返回用户信息
     */
    public function logged() {
        $sessionUserID = (int) $this->getLoginSession();
        $res = null;
        $this->loggedUserPowers = null;
        $sessionID = $this->getSessionID();
        if ($sessionUserID > 0) {
            $res = $this->getUser($sessionUserID);
            if ($res['user_ip'] != $this->ip || $res['user_session'] != $sessionID) {
                $res = null;
            }
        } else {
            //判断是否记住我
            $cookieUserID = (int) $this->getLoginCookie();
            if ($cookieUserID > 0) {
                $res = $this->getUser($cookieUserID);
                if ($res) {
                    if ($res['user_remember'] > 0 && $res['user_ip'] == $this->ip && $res['user_session'] == $sessionID) {
                        $this->setLoginSession($res['id']);
                        $this->updateLoginSession($res['id']);
                        $this->addLog('User Logged,by ID : ' . $res['id']);
                    }
                }
            }
        }
        if ($res) {
            $this->updateLoginTime($res['id']);
            if ($this->checkLoginTime() == true) {
                $this->loggedUserGroup = $this->getGroup((int) $res['user_group']);
                if ($this->loggedUserGroup) {
                    if ($this->loggedUserGroup['group_power']) {
                        $this->loggedUserPowers = explode(',', $this->loggedUserGroup['group_power']);
                    }
                    return $res;
                }
            }
        }
        return false;
    }

    /**
     * 判断用户是否具备权限
     * @param string $power 权限标识
     * @return boolean 是否具备权限
     */
    public function checkPower($power) {
        if ($this->loggedUserPowers && is_array($this->loggedUserPowers) == true) {
            return in_array($power, $this->loggedUserPowers);
        }
        return false;
    }

    /**
     * 退出登陆
     */
    public function logout() {
        $userID = $this->getLoginSession();
        if ($userID > 0) {
            $this->setRemember($userID, 0);
            $this->setLoginSession(0);
            $this->setLoginCookie(0);
        }
    }

    /**
     * 获取用户列表
     * @param string $where 条件语句
     * @param array $attrs 过滤数据数组
     * @param int $page 页数
     * @param int $max 页长
     * @param int $order 排序字段键值
     * @param boolean $desc 是否倒叙
     * @return array 数据数组
     */
    public function getUserList($where = '1', $attrs = null, $page = 1, $max = 10, $order = 0, $desc = true) {
        $orderField = isset($this->userFieldsArr[$order]) == true ? $this->userFieldsArr[$order] : $this->userFieldsArr[0];
        $descStr = $desc == true ? 'DESC' : 'ASC';
        $sql = 'SELECT ' . $this->userFields . ' FROM `' . $this->tableNameUser . '` WHERE ' . $where . ' ORDER BY ' . $orderField . ' ' . $descStr . ' LIMIT ' . ($page - 1) * $max . ',' . $max;
        return $this->doSQL($sql, $attrs, 3, PDO::FETCH_ASSOC);
    }

    /**
     * 获取用户记录数
     * @param string $where 条件
     * @param array $attrs 条件数据数组
     * @return int 记录数
     */
    public function getUserListRow($where = '1', $attrs = null) {
        if (!$where) {
            $where = '1';
        }
        $sql = 'SELECT count(id) FROM `' . $this->tableNameUser . '` WHERE ' . $where;
        return $this->doSQL($sql, $attrs, 2, 0);
    }

    /**
     * 获取用户
     * @param mixed $target 用户ID或用户名
     * @return boolean|array 用户数据数组，失败返回false
     */
    public function getUser($target) {
        if ($target) {
            $sql = 'SELECT ' . $this->userFields . ' FROM `' . $this->tableNameUser . '` WHERE ';
            $attrs;
            if (is_int($target) == true) {
                $sql .= $this->tableNameUser . '.id = :id';
                $attrs = array(':id' => array($target, PDO::PARAM_INT));
            } else {
                $sql .= $this->tableNameUser . '.user_username = :username';
                $attrs = array(':username' => array($target, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT));
            }
            return $this->doSQL($sql, $attrs, 1, PDO::FETCH_ASSOC);
        }
        return false;
    }

    /**
     * 获取用户组列表
     * @param string $where 条件语句
     * @param array $attrs 过滤数据
     * @param int $page 页数
     * @param int $max 页长
     * @param int $order 排序字段键值
     * @param boolean $desc 是否倒叙
     * @return array 数据数组
     */
    public function getGroupList($where = '1', $attrs = null, $page = 1, $max = 10, $order = 0, $desc = true) {
        $orderField = isset($this->groupFieldsArr[$order]) == true ? $this->groupFieldsArr[$order] : $this->groupFieldsArr[0];
        $descStr = $desc == true ? 'DESC' : 'ASC';
        $sql = 'SELECT ' . $this->groupFields . ' FROM `' . $this->tableNameGroup . '` WHERE ' . $where . ' ORDER BY ' . $orderField . ' ' . $descStr . ' LIMIT ' . ($page - 1) * $max . ',' . $max;
        return $this->doSQL($sql, $attrs, 3, PDO::FETCH_ASSOC);
    }

    /**
     * 获取用户组记录数
     * @param string $where 条件
     * @param array $attrs 条件数据组合
     * @return int 记录数
     */
    public function getGroupListRow($where = '1', $attrs = null) {
        if (!$where) {
            $where = '1';
        }
        $sql = 'SELECT count(id) FROM `' . $this->tableNameGroup . '` WHERE ' . $where;
        return $this->doSQL($sql, $attrs, 2, 0);
    }

    /**
     * 获取用户组
     * @param mixed $target 用户Id或用户名称
     * @return array|boolean 用户组数据数组，失败返回false
     */
    public function getGroup($target) {
        if ($target) {
            $sql = 'SELECT ' . $this->groupFields . ' FROM `' . $this->tableNameGroup . '` WHERE ';
            $attrs;
            if (is_int($target) == true) {
                $sql .= '`id` = :id';
                $attrs = array(':id' => array($target, PDO::PARAM_INT));
            } else {
                $sql .= '`group_name` = :name';
                $attrs = array(':name' => array($target, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT));
            }
            return $this->doSQL($sql, $attrs, 1, PDO::FETCH_ASSOC);
        }
        return false;
    }

    /**
     * 添加用户
     * @param int $group 用户组ID
     * @param string $user 用户名
     * @param string $passwd 密码
     * @param string $name 名字
     * @param string $email 邮箱
     * @return int 用户ID，失败则返回0
     */
    public function addUser($group, $user, $passwd, $name, $email) {
        if ($this->checkUserName($user) == true && $this->checkUserNameOnly($user) == true && $this->checkUserPasswd($passwd) == true && $this->checkName($name) == true && $this->checkEmail($email) == true && $this->getGroup($group)) {
            $sql = 'INSERT INTO `' . $this->tableNameUser . '`(' . $this->userFields . ') VALUES(NULL,:group,:user,:passwd,:name,:email,NOW(),:ip,0,NULL)';
            $passwdEncrypt = $this->getEncrypt($passwd);
            $attrs = array(':group' => array($group, PDO::PARAM_INT), ':user' => array($user, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT), ':passwd' => array($passwdEncrypt, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT), ':name' => array($name, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT), ':email' => array($email, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT), ':ip' => array($this->ip, PDO::PARAM_STR));
            $res = $this->doSQL($sql, $attrs, 4);
            if ($res > 0) {
                $this->addLog('Add user success,by ID : ' . $res);
                return $res;
            } else {
                $this->addLog('Add user failed.');
            }
        }
        return 0;
    }

    /**
     * 添加用户组
     * <p>注意，用户组名称不能冲突。</p>
     * @param string $name 用户组名称
     * @param array $power 权限数组
     * @return int 用户组ID，如果失败返回0
     */
    public function addGroup($name, $power) {
        if (!$this->getGroup($name) && $this->checkName($name) == true) {
            $sql = 'INSERT INTO `' . $this->tableNameGroup . '`(' . $this->groupFields . ') VALUES(NULL,:name,:power)';
            $power = $this->getPower($power);
            $attrs = array(':name' => array($name, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT), ':power' => array($power, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT));
            $res = $this->doSQL($sql, $attrs, 4);
            if ($res > 0) {
                $this->addLog('Add user group success,by ID : ' . $res);
                return $res;
            } else {
                $this->addLog('Add user group failed.');
            }
        }
        return 0;
    }

    /**
     * 编辑用户
     * @param int $id 用户ID
     * @param string $user 用户名
     * @param string $passwd 密码，可以不是明文
     * @param string $name 名字
     * @param string $email 邮箱
     * @return boolean 是否成功
     */
    public function editUser($id, $user, $passwd, $name, $email) {
        if ($this->checkUserName($user) == true && $this->checkName($name) == true && $this->checkEmail($email) == true) {
            $id = (int) $id;
            $userRes = $this->getUser($id);
            if ($userRes) {
                if ($userRes['user_username'] != $user && $this->checkUserNameOnly($user) == false) {
                    return false;
                }
            } else {
                return false;
            }
            $sql = 'UPDATE `' . $this->tableNameUser . '` SET `user_username` = :user,`user_password` = :passwd,`user_name` = :name,`user_email` = :email WHERE `id` = :id';
            //如果密码不是明文，则直接赋值
            $passwdEncrypt = '';
            if (strlen($passwd) == 40 || $passwd == null || $passwd == '') {
                if ($userRes) {
                    $passwdEncrypt = $userRes['user_password'];
                } else {
                    return false;
                }
            } else {
                if ($this->checkUserPasswd($passwd) == true) {
                    $passwdEncrypt = $this->getEncrypt($passwd);
                } else {
                    return false;
                }
            }
            $attrs = array(':id' => array($id, PDO::PARAM_INT), ':user' => array($user, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT), ':passwd' => array($passwdEncrypt, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT), ':name' => array($name, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT), ':email' => array($email, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT));
            $res = $this->doSQL($sql, $attrs);
            if ($res == true) {
                $this->addLog('Edit user success,by ID : ' . $id);
                return true;
            } else {
                $this->addLog('Edit user failed,by ID : ' . $id);
            }
        }
        return false;
    }

    /**
     * 编辑用户组
     * @param int $id 用户组ID
     * @param string $name 用户组名称
     * @param array $power 权限数组
     * @return boolean 是否成功
     */
    public function editGroup($id, $name, $power) {
        if ($this->checkName($name) == true) {
            $sql = 'UPDATE `' . $this->tableNameGroup . '` SET `group_name` = :name,`group_power` = :power WHERE `id` = :id';
            $power = $this->getPower($power);
            $attrs = array(':id' => array($id, PDO::PARAM_INT), ':name' => array($name, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT), ':power' => array($power, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT));
            $res = $this->doSQL($sql, $attrs);
            if ($res == true) {
                $this->addLog('Edit user group success,by ID : ' . $id);
                return true;
            } else {
                $this->addLog('Edit user group failed,by ID : ' . $id);
            }
        }
        return false;
    }

    /**
     * 检查用户是否具备权限
     * @param int $id 用户ID
     * @param string $power 权限
     * @return boolean 是否具备权限
     */
    public function checkUserPower($id, $power) {
        $userRes = $this->getUser($id);
        if ($userRes) {
            $groupRes = $this->getGroup($userRes['id']);
            if ($groupRes) {
                $powerArr = explode(',', $groupRes['group_power']);
                if (in_array($power, $powerArr) == true) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 删除用户
     * @param int $id 用户ID
     * @return boolean 是否成功
     */
    public function delUser($id) {
        $sql = 'DELETE FROM `' . $this->tableNameUser . '` WHERE `id` = :id';
        $attrs = array(':id' => array($id, PDO::PARAM_INT));
        if ($this->doSQL($sql, $attrs) == true) {
            $this->addLog('Delete user success,by ID : ' . $id);
            return true;
        }
        $this->addLog('Delete user failed,by ID : ' . $id);
        return false;
    }

    /**
     * 删除用户组
     * @param int $id 用户组ID
     * @return boolean 是否成功
     */
    public function delGroup($id) {
        $sql = 'DELETE FROM `' . $this->tableNameGroup . '` WHERE `id` = :id';
        $attrs = array(':id' => array($id, PDO::PARAM_INT));
        if ($this->doSQL($sql, $attrs) == true) {
            $this->addLog('Delete user group success,by ID : ' . $id);
            return true;
        }
        $this->addLog('Delete user group failed,by ID : ' . $id);
        return false;
    }

    /**
     * 更新用户登录Ip和时间
     * @param int $target 目标ID
     * @return boolean 是否成功
     */
    private function updateLoginTime($target) {
        $sql = 'UPDATE `' . $this->tableNameUser . '` SET `user_ip` = :ip,`user_lasttime` = NOW() WHERE `id` = :id';
        $attrs = array(':ip' => array($this->ip, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT), ':id' => array($target, PDO::PARAM_INT));
        return $this->doSQL($sql, $attrs);
    }

    /**
     * 更新用户登陆session_id
     * @param int $id 用户ID
     * @return boolean 是否成功
     */
    private function updateLoginSession($id) {
        $sessionID = $this->getSessionID();
        $sql = 'UPDATE `' . $this->tableNameUser . '` SET `user_session` = :session WHERE `id` = :id';
        $attrs = array(':session' => array($sessionID, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT), ':id' => array($id, PDO::PARAM_INT));
        return $this->doSQL($sql, $attrs);
    }

    /**
     * 获取session_id
     * @return string session_id值
     */
    private function getSessionID() {
        return session_id();
    }

    /**
     * 检查并更新登录时间
     * @return boolean 是否通过检查
     */
    private function checkLoginTime() {
        $loginTime = $this->session->get('login-time');
        if ($loginTime > 0) {
            $expireTime = $loginTime + $this->loginTime;
            $nowTime = time();
            if ($nowTime <= $expireTime) {
                $this->session->save('login-time', $nowTime);
                return true;
            }
        } else {
            $this->session->save('login-time', time());
            return true;
        }
        $this->session->clear('login-time');
        return false;
    }

    /**
     * 获取权限字符串
     * @param array $powerArr 权限数组
     * @return string 权限字符串
     */
    private function getPower($powerArr) {
        if ($powerArr) {
            $powers;
            $powersList = array_keys($this->powers);
            foreach ($powerArr as $v) {
                if (in_array($v, $powersList) == true) {
                    $powers[] = $v;
                }
            }
            $power = implode(',', $powers);
            return $power;
        }
        return '';
    }

    /**
     * 设定记住我
     * @param int $id 用户Id
     * @param int $remember 是否记住我 eg: 1|0
     * @return boolean
     */
    private function setRemember($id, $remember = 1) {
        $sql = 'UPDATE `' . $this->tableNameUser . '` SET `user_remember` = :remember WHERE `id` = :id';
        $attrs = array(':remember' => array($remember, PDO::PARAM_INT), ':id' => array($id, PDO::PARAM_INT));
        return $this->doSQL($sql, $attrs);
    }

    /**
     * 获取cookie值
     * @return string 用户ID加密值
     */
    private function getLoginCookie() {
        if (isset($_COOKIE[$this->loginCookieName]) == true) {
            return $_COOKIE[$this->loginCookieName];
        }
        return '';
    }

    /**
     * 设定cookie值
     * @param string $value 值
     */
    private function setLoginCookie($value) {
        setcookie($this->loginCookieName, $value, $this->cookieTime);
    }

    /**
     * 返回当前登陆状态
     * @return int 用户ID
     */
    private function getLoginSession() {
        return $this->session->get($this->loginSessionName);
    }

    /**
     * 设定Session登陆用户ID
     * @param int $userID 用户ID
     */
    private function setLoginSession($userID) {
        $this->session->save($this->loginSessionName, $userID);
    }

    /**
     * 检查用户名
     * @param string $user 用户名
     * @return boolean 是否正确
     */
    private function checkUserName($user) {
        if (filter_var($user, FILTER_SANITIZE_STRING) != false && strlen($user) <= 30) {
            return true;
        }
        return false;
    }

    /**
     * 检查用户名是否唯一
     * @param string $user 用户名
     * @return boolean 是否唯一
     */
    private function checkUserNameOnly($user) {
        $res = $this->getUser($user);
        if (!$res) {
            return true;
        }
        return false;
    }

    /**
     * 检查密码
     * @param string $passwd 密码原文
     * @return boolean 是否正确
     */
    private function checkUserPasswd($passwd) {
        if (filter_var($passwd, FILTER_SANITIZE_STRING) != false && strlen($passwd) <= 30) {
            return true;
        }
        return false;
    }

    /**
     * 检查邮箱
     * @param string $email 邮箱
     * @return boolean 是否正确
     */
    private function checkEmail($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * 检查用户名称
     * @param string $name 用户名称
     * @return boolean 是否正确
     */
    private function checkName($name) {
        if (filter_var($name, FILTER_SANITIZE_STRING) != false && strlen($name) < 300) {
            return true;
        }
        return false;
    }

    /**
     * 获取SHA1值
     * @param string $str 字符串
     * @return string SHA1值
     */
    public function getEncrypt($str) {
        return sha1($str);
    }

    /**
     * 执行SQL封装
     * @param string $sql SQL
     * @param array $attrs 筛选组合数组
     * @param int $resType 返回方式
     * @param int $resFetch 返回类型
     * @return mixed 混合返回结果
     */
    private function doSQL($sql, $attrs = null, $resType = 0, $resFetch = null) {
        return $this->db->prepareAttr($sql, $attrs, $resType, $resFetch);
    }

    /**
     * 添加一条日志
     * @param string $message 日志消息
     */
    private function addLog($message) {
        $this->log->add($message);
    }

}

?>
