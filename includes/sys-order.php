<?php

/**
 * 订单操作类
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package sys
 */
class SysOrder {

    /**
     * 数据表名称
     * @var string 
     */
    private $tableName;

    /**
     * 数据库句柄
     * @var CoreDB 
     */
    private $db;

    /**
     * 日志句柄
     * @var SysLog 
     */
    private $log;

    /**
     * 会话句柄
     * @var CoreSession 
     */
    private $session;

    /**
     * 产品句柄
     * @var SysProduct 
     */
    private $sysProduct;

    /**
     * 购物车会话标记
     * @var string 
     */
    private $buySessionName = 'ORDER-BUY';

    /**
     * 字段列
     * @var array 
     */
    private $fields = array('id', 'order_parent', 'product_id', 'order_ip', 'order_price', 'order_date', 'order_status', 'order_name', 'order_contact', 'order_note');

    /**
     * 状态列
     * @var array 
     */
    public $status = array('wait', 'finish', 'trash');

    /**
     * 初始化
     * @param CoreDB $db 数据库句柄
     * @param SysLog $log 日志句柄
     * @param CoreSession $session 会话句柄
     * @param SysProduct $sysProduct 产品句柄
     */
    public function __construct(&$db, &$log, &$session, &$sysProduct) {
        $this->db = $db;
        $this->log = $log;
        $this->session = $session;
        $this->sysProduct = $sysProduct;
        $this->tableName = $db->tables['order'];
    }

    /**
     * 获取列表
     * @param string $where 条件语句
     * @param array $attrs 过滤参数
     * @param int $page 页数
     * @param int $max 页长
     * @param int $sort 排序字段键值
     * @param boolean $desc 是否倒叙
     * @return array 数据数组
     */
    public function getList($where = '1', $attrs = null, $page = 1, $max = 10, $sort = 0, $desc = false) {
        $sortField = isset($this->fields[$sort]) == true ? $this->fields[$sort] : $this->fields[0];
        $descStr = $desc == true ? 'DESC' : 'ASC';
        $sql = 'SELECT `id`,`order_parent`,`product_id`,`order_ip`,`order_price`,`order_date`,`order_status`,`order_name`,`order_contact`,`order_note` FROM `' . $this->tableName . '` WHERE ' . $where . ' ORDER BY `' . $sortField . '` ' . $descStr . ' LIMIT ' . ($page - 1) * $max . ',' . $max;
        return $this->doSQL($sql, $attrs, 3, PDO::FETCH_ASSOC);
    }

    /**
     * 获取记录数
     * @param string $where 条件语句
     * @param array $attrs 过滤参数
     * @return int 记录数
     */
    public function getListRow($where = '1', $attrs = null) {
        $sql = 'SELECT COUNT(`id`) FROM `' . $this->tableName . '` WHERE ' . $where;
        return $this->doSQL($sql, $attrs, 2, 0);
    }

    /**
     * 获取订单信息
     * @param int $target 目标ID
     * @return array 数据数组
     */
    public function get($target) {
        $target = (int) $target;
        $sql = 'SELECT `id`,`order_parent`,`product_id`,`order_ip`,`order_price`,`order_date`,`order_status`,`order_name`,`order_contact`,`order_note` FROM `' . $this->tableName . '` WHERE `id` = :id';
        $attrs = array(':id' => array($target, PDO::PARAM_INT));
        return $this->doSQL($sql, $attrs, 1, PDO::FETCH_ASSOC);
    }

    /**
     * 获取订单子集信息
     * @param int $target 目标ID
     * @return array 数据数组
     */
    public function getInfos($target) {
        $target = (int) $target;
        $sql = 'SELECT `id`,`order_parent`,`product_id`,`order_ip`,`order_price`,`order_date`,`order_status`,`order_name`,`order_contact`,`order_note` FROM `' . $this->tableName . '` WHERE `order_parent` = :id';
        $attrs = array(':id' => array($target, PDO::PARAM_INT));
        return $this->doSQL($sql, $attrs, 1, PDO::FETCH_ASSOC);
    }

    /**
     * 将产品ID添加到购物车
     * @param int $productID 产品ID
     */
    public function addBuy($productID) {
        $productRes = $this->getProduct($productID);
        if ($productRes) {
            $buys = $this->session->get($this->buySessionName);
            if ($buys) {
                $buys .= ',' . $productID;
            } else {
                $buys = $productID;
            }
            $this->session->save($this->buySessionName, $buys);
        }
    }

    /**
     * 获取购物车产品列
     * @return null|array 数据数组，无内容则返回null
     */
    public function getBuy() {
        $res = $this->getBuySession();
        if ($res) {
            return explode(',', $res);
        }
        return null;
    }

    /**
     * 清空购物车
     * @param int $productID 指定清理产品ID
     */
    public function clearBuy($productID = null) {
        if ($name) {
            $productID = (int) $productID;
            $buys = $this->getBuy();
            $key = array_search($productID, $buys);
            if ($key > 0) {
                unset($buys[$key]);
                if ($buys) {
                    sort($buys);
                    $buyStr = implode(',', $buys);
                    $this->saveBuySession($buyStr);
                }
            }
        } else {
            $this->saveBuySession('');
        }
    }

    /**
     * 生成订单
     * <p>根据购物车内容构建订单。</p>
     * @param string $ip IP地址
     * @param string $name 名字
     * @param string $contact 联系方式
     * @param string $note 备注
     * @return boolean 是否成功
     */
    public function order($ip, $name, $contact, $note) {
        $buyStr = $this->getBuySession();
        if ($buyStr) {
            $buyList = explode(',', $buyStr);
            $productList = null;
            //检查产品是否存在，不存在则清空订单，返回失败
            foreach ($buyList as $v) {
                $vProductRes = $this->getProduct($v);
                if (!$vProductRes) {
                    $this->clearBuy();
                    return false;
                }
                $productList[] = $vProductRes;
            }
            //计算订单总额
            $priceAll = $this->sysProduct->getPrice($buyList);
            //生成订单主体
            $sql = 'INSERT INTO `' . $this->tableName . '`(`id`,`order_parent`,`product_id`,`order_ip`,`order_price`,`order_date`,`order_status`,`order_name`,`order_contact`,`order_note`) VALUES(NULL,:parent,:product,:ip,:price,NOW(),:status,:name,:contact,:note)';
            $attrs = array(
                ':parent' => array(0, PDO::PARAM_INT),
                ':product' => array(0, PDO::PARAM_INT),
                ':ip' => array($ip, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT),
                ':price' => array($priceAll, PDO::PARAM_INT),
                ':status' => array($this->status[0], PDO::PARAM_STR),
                ':name' => array($name, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT),
                ':contact' => array($contact, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT),
                ':note' => array($note, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT)
            );
            $resID = $this->doSQL($sql, $attrs, 4);
            if ($resID > 0) {
                //生成子集
                foreach ($productList as $v) {
                    $attrs[':parent'] = array($resID, PDO::PARAM_INT);
                    $attrs[':product'] = array($v['id'], PDO::PARAM_INT);
                    $attrs[':price'] = array($v['pt_price'], PDO::PARAM_INT);
                    $attrs[':status'] = array(NULL, PDO::PARAM_NULL);
                    $attrs[':name'] = array($v['pt_name'], PDO::PARAM_STR);
                    $attrs[':contact'] = array(NULL, PDO::PARAM_NULL);
                    $attrs[':note'] = array($v['pt_description'], PDO::PARAM_STR);
                    $res = $this->doSQL($sql, $attrs, 4);
                    if ($res < 1) {
                        $this->addLog('Add order failed , by ID : ', $resID);
                        return false;
                    }
                }
                //完成，返回订单ID
                $this->addLog('Add order success , by ID : ' . $resID);
                return $resID;
            } else {
                $this->addLog('Add order failed');
            }
        }
        return false;
    }

    /**
     * 修改订单状态
     * @param int|array $target 目标ID或ID列
     * @param string $status 状态
     * @return boolean 是否成功
     */
    public function changeStatus($target, $status) {
        if (is_array($target) == true) {
            foreach ($target as $v) {
                if ($this->editStatus($v, $status) == false) {
                    $this->addLog('Change order status failed , by ID : ' . $v . ' , status : ' . $status);
                    return false;
                }
            }
            $this->addLog('Change order status success , by ID list , status : ' . $status);
            return true;
        } else {
            $res = $this->editStatus($target, $status);
            if ($res == true) {
                $this->addLog('Change order status success , by ID : ' . $target . ' , status : ' . $status);
            } else {
                $this->addLog('Change order status failed , by ID : ' . $target . ' , status : ' . $status);
            }
            return $res;
        }
    }

    /**
     * 删除订单
     * @param int $target 目标ID
     * @return boolean 是否成功
     */
    public function del($target) {
        $target = (int) $target;
        $sql = 'DELETE FROM `' . $this->tableName . '` WHERE `id` = :id or `order_parent` = :id';
        $attrs = array(':id' => array($target, PDO::PARAM_INT));
        $res = $this->doSQL($sql, $attrs);
        if ($res == true) {
            $this->addLog('Delete order success , by ID : ' . $target);
        } else {
            $this->addLog('Delete order failed , by ID : ' . $target);
        }
        return $res;
    }

    /**
     * 删除产品下的所有订单
     * @param int $productID 产品ID
     * @return boolean 是否成功
     */
    public function delProductAll($productID) {
        $sql = 'DELETE FROM `' . $this->tableName . '` WHERE `product_id` = :id';
        $attrs = array(':id' => array($productID, PDO::PARAM_INT));
        $res = $this->doSQL($sql, $attrs);
        if ($res == true) {
            $this->addLog('Delete product all order success , by product ID : ' . $productID);
        } else {
            $this->addLog('Delete product all order failed , by product ID : ' . $productID);
        }
        return $res;
    }

    /**
     * 修改订单状态
     * @param int $target 目标ID
     * @param string $status 状态
     * @return boolean 是否成功
     */
    private function editStatus($target, $status) {
        $target = (int) $target;
        $sql = 'UPDATE `' . $this->tableName . '` SET `order_status` = :status WHERE `id` = :id';
        $attrs = array(':id' => array($target, PDO::PARAM_INT), ':status' => array($status, PDO::PARAM_STR));
        return $this->doSQL($sql, $attrs);
    }

    /**
     * 遍历插入PDO数据
     * @param string $sql SQL语句
     * @param array $attrs 数据数组 eg:array(':id'=>array('value','PDO::PARAM_INT'),...)
     * @param int $resType 返回类型 0-boolean 1-fetch 2-fetchColumn 3-fetchAll 4-lastID
     * @param int $resFetch PDO-FETCH类型，如果返回fetchColumn则为列偏移值
     * @return boolean|PDOStatement 成功则返回PDOStatement句柄，失败返回false
     */
    private function doSQL($sql, $attrs = null, $resType = 0, $resFetch = null) {
        return $this->db->prepareAttr($sql, $attrs, $resType, $resFetch);
    }

    /**
     * 添加日志
     * @param string $message 消息
     */
    private function addLog($message) {
        $this->log->add($message);
    }

    /**
     * 获取产品信息
     * @param int $target 产品ID
     * @return array 产品数据数组
     */
    private function getProduct($target) {
        return $this->sysProduct->get($target);
    }

    /**
     * 获取购物车数据
     * @return string 购物车数据 eg: 1,5,10...
     */
    private function getBuySession() {
        return $this->session->get($this->buySessionName);
    }

    /**
     * 保存购物车数据
     * @param string $value 购物车数据
     */
    private function saveBuySession($value) {
        $this->session->save($this->buySessionName, $value);
    }

}

?>
