<?php

/**
 * 访问控制器
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package sys
 */
class SysViews {

    /**
     * 数据表名称
     * @var string 
     */
    private $tableName;

    /**
     * 数据库句柄
     * @var CoreDB 
     */
    private $db;

    /**
     * 日志句柄
     * @var SysLog 
     */
    private $log;

    /**
     * 字段列
     * @var array 
     */
    private $fields = array('id', 'v_ip', 'v_url', 'v_date');

    /**
     * 初始化
     * @param CoreDB $db 数据库句柄
     * @param SysLog $log 日志句柄
     */
    public function __construct(&$db, &$log) {
        $this->db = $db;
        $this->log = $log;
        $this->tableName = $db->tables['views'];
    }

    /**
     * 获取列表
     * @param string $where 条件语句
     * @param array $attrs 过滤参数
     * @param int $page 页数
     * @param int $max 页长
     * @param int $sort 排序字段键值
     * @param boolean $desc 是否倒叙
     * @param boolean $groupBool 是否去除重复
     * @return array 数据数组
     */
    public function getList($where = '1', $attrs = null, $page = 1, $max = 10, $sort = 0, $desc = true, $groupBool = false) {
        $sortField = isset($this->fields[$sort]) == true ? $this->fields[$sort] : $this->fields[0];
        $descStr = $desc == true ? 'DESC' : 'ASC';
        $groupStr = $groupBool == true ? ' GROUP BY v_ip ' : '';
        $sql = 'SELECT `id`,`v_ip`,`v_url`,`v_date` FROM `' . $this->tableName . '` WHERE ' . $where . $groupStr . ' ORDER BY ' . $sortField . ' ' . $descStr . ' LIMIT ' . ($page - 1) * $max . ',' . $max;;
        return $this->doSQL($sql, $attrs, 3, PDO::FETCH_ASSOC);
    }

    /**
     * 获取记录数
     * @param string $where 条件语句
     * @param array $attrs 过滤参数
     * @param boolean $groupBool 是否去除重复
     * @return int 记录数
     */
    public function getListRow($where = '1', $attrs = null, $groupBool = false) {
        $groupStr = $groupBool == true ? ' GROUP BY `v_ip`' : '';
        $sql = 'SELECT COUNT(`id`) FROM `' . $this->tableName . '` WHERE ' . $where . $groupStr;
        return $this->doSQL($sql, $attrs, 2, 0);
    }

    /**
     * 获取页面访问排行
     * @param string $where 条件语句
     * @param array $attrs 过滤参数
     * @return array 数据数组
     */
    public function getPageTop($where = '1', $attrs = null, $max = 10) {
        $sql = 'SELECT COUNT(`id`) as `num`,`v_url` FROM `' . $this->tableName . '` WHERE ' . $where . ' GROUP BY v_url ORDER BY COUNT(`id`) DESC LIMIT 0,' . $max;
        return $this->doSQL($sql, $attrs, 3, PDO::FETCH_ASSOC);
    }

    /**
     * 清空表
     * @return boolean 是否成功
     */
    public function clear() {
        $sql = 'TRUNCATE ' . $this->tableName;
        return $this->doSQL($sql);
    }

    /**
     * 添加记录
     * @param string $ip IP地址
     * @return int 记录ID，失败则返回0
     */
    public function add($ip) {
        $nowDate = date('Y-m-d');
        $pageURL = $this->getPageURL();
        $sqlSelect = 'SELECT `id` FROM `' . $this->tableName . '` WHERE `v_ip` = :ip and `v_date` LIKE \'' . $nowDate . '%\' and `v_url` = :url';
        $attrsSelect = array(
            ':ip' => array($ip, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT),
            ':url' => array($pageURL, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT)
        );
        $res = $this->doSQL($sqlSelect, $attrsSelect, 2, 0);
        if (!$res) {
            $sqlInsert = 'INSERT INTO `' . $this->tableName . '`(`id`,`v_ip`,`v_url`,`v_date`) VALUES(NULL,:ip,:url,NOW())';
            $attrsInsert = array(
                ':ip' => array($ip, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT),
                ':url' => array($pageURL, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT)
            );
            return $this->doSQL($sqlInsert, $attrsInsert, 4);
        }
        return false;
    }

    /**
     * 获取当前页面URL
     * @return string URL
     */
    private function getPageURL() {
        $url = $_SERVER['REQUEST_URI'];
        if (strpos($url, "?") !== false)
            $url = reset(explode("?", $url));
        return $url;
    }

    /**
     * 添加日志
     * @param string $message 日志消息
     */
    private function addLog($message) {
        $this->log->add($message);
    }

    /**
     * 遍历插入PDO数据
     * @param string $sql SQL语句
     * @param array $attrs 数据数组 eg:array(':id'=>array('value','PDO::PARAM_INT'),...)
     * @param int $resType 返回类型 0-boolean 1-fetch 2-fetchColumn 3-fetchAll 4-lastID
     * @param int $resFetch PDO-FETCH类型，如果返回fetchColumn则为列偏移值
     * @return boolean|PDOStatement 成功则返回PDOStatement句柄，失败返回false
     */
    private function doSQL($sql, $attrs = null, $resType = 0, $resFetch = null) {
        return $this->db->prepareAttr($sql, $attrs, $resType, $resFetch);
    }

}

?>
