<?php

/**
 * 标签操作类
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package sys
 */
class SysTag {

    /**
     * 标签表名称
     * @var string 
     */
    private $tableNameTag;

    /**
     * 标签值表名称
     * @var string 
     */
    private $tableNameTagValue;

    /**
     * 数据库句柄
     * @var CoreDB 
     */
    private $db;

    /**
     * 日志句柄
     * @var SysLog 
     */
    private $log;

    public function __construct(&$db, &$log) {
        $this->db = $db;
        $this->log = $log;
    }

    public function getTags($postID) {
        $sql = 'SELECT ';
    }

    public function searchTag($tagName) {
        
    }

    public function getList($where = '1', $attrs = null, $page = 1, $max = 5, $sort = 0, $desc = true) {
        
    }

    /**
     * 添加日志
     * @param string $message 日志消息
     */
    private function addLog($message) {
        $this->log->add($message);
    }

}

?>
