<?php

/**
 * 产品操作类
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package sys
 */
class SysProduct {

    /**
     * 数据表
     * @var string 
     */
    private $tableName;

    /**
     * 数据库句柄
     * @var CoreDB 
     */
    private $db;

    /**
     * 日志句柄
     * @var SysLog 
     */
    private $log;

    /**
     * 初始化
     * @param CoreDB $db 数据库句柄
     * @param SysLog $log 日志句柄
     */
    public function __construct(&$db, &$log) {
        $this->db = $db;
        $this->log = $log;
        $this->tableName = $db->tables['product'];
    }

    /**
     * 获取指定post所有产品
     * @param int $postID POST-ID
     * @return array 产品数据数组
     */
    public function getALL($postID) {
        $postID = (int) $postID;
        $sql = 'SELECT `id`,`post_id`,`pt_price`,`pt_name`,`pt_description` FROM `' . $this->tableName . '` WHERE `post_id` = :id ORDER BY `id` ASC';
        $attrs = array(':id' => array($postID, PDO::PARAM_INT));
        return $this->doSQL($sql, $attrs, 3, PDO::FETCH_ASSOC);
    }

    /**
     * 获取产品信息
     * @param int $target 目标ID
     * @return array 产品信息数组
     */
    public function get($target) {
        $target = (int) $target;
        $sql = 'SELECT `id`,`post_id`,`pt_price`,`pt_name`,`pt_description` FROM `' . $this->tableName . '` WHERE `id` = :id';
        $attrs = array(':id' => array($target, PDO::PARAM_INT));
        return $this->doSQL($sql, $attrs, 1, PDO::FETCH_ASSOC);
    }
    
    /**
     * 计算产品金额
     * @param int|array $target 目标ID或ID列
     * @return int 总额
     */
    public function getPrice($target) {
        if (is_array($target) == true) {
            $sql = 'SELECT SUM(`pt_price`) FROM `' . $this->tableName . '` WHERE';
            $where = '';
            $attrs = null;
            foreach ($target as $k => $v) {
                $where .= ' or `id` = :id' . $k;
                $attrs[':id' . $k] = array($v, PDO::PARAM_INT);
            }
            $where = substr($where, 4);
            $sql .= $where;
            return $this->doSQL($sql, $attrs, 2, 0);
        } else {
            $sql = 'SELECT `pt_price` FROM `' . $this->tableName . '` WHERE `id` = :id';
            $attrs = array(':id' => array($target, PDO::PARAM_INT));
            return $this->doSQL($sql, $attrs, 2, 0);
        }
    }

    /**
     * 设定产品数据
     * @param int $postID 目标POST-ID
     * @param array $products 产品数据数组 eg: array(array('name'=>'','price'=>5,'des'=>'...'),...)
     * @return boolean
     */
    public function setProducts($postID, $products) {
        $productList = $this->getALL($postID);
        if ($productList) {
            if ($this->delAll($postID) == false) {
                return false;
            }
        }
        if ($products) {
            $sqlInsert = 'INSERT INTO `' . $this->tableName . '`(`id`,`post_id`,`pt_price`,`pt_name`,`pt_description`) VALUES(NULL,:post,:price,:name,:des)';
            $attrsInsert = array(':post' => array($postID, PDO::PARAM_INT));
            foreach ($products as $v) {
                $attrsInsert[':price'] = array($v['price'], PDO::PARAM_INT);
                $attrsInsert[':name'] = array($v['name'], PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT);
                $attrsInsert[':des'] = array($v['des'], PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT);
                $res = $this->doSQL($sqlInsert, $attrsInsert, 4);
                if ($res < 1) {
                    $this->addLog('Add product failed , by post ID : ' . $postID . ' , name : ' . $v['name'] . ' , price : ' . $v['price'] . ' , description : ' . $v['des']);
                    return false;
                }
            }
            $this->addLog('Add product success , by post ID : ' . $postID);
            return true;
        } else {
            $this->addLog('Add product success , by post ID : ' . $postID);
            return true;
        }
        $this->addLog('Add product failed , by post ID : ' . $postID);
        return false;
    }

    /**
     * 删除产品
     * @param int $id 目标ID
     * @return boolean 是否成功
     */
    public function del($id) {
        $sql = 'DELETE FROM `' . $this->tableName . '` WHERE `id` = :id';
        $attrs = array(':id' => array($id, PDO::PARAM_INT));
        $res = $this->doSQL($sql, $attrs);
        if ($res == true) {
            $this->addLog('Delete product success , by ID : ' . $id);
        } else {
            $this->addLog('Delete product failed , by ID : ' . $id);
        }
        return $res;
    }

    /**
     * 删除指定post下所有产品
     * @param int $postID POST-ID
     * @return boolean 是否成功
     */
    public function delAll($postID) {
        $sql = 'DELETE FROM `' . $this->tableName . '` WHERE `post_id` = :id';
        $attrs = array(':id' => array($postID, PDO::PARAM_INT));
        $res = $this->doSQL($sql, $attrs);
        if ($res == true) {
            $this->addLog('Delete post all product success , by post ID : ' . $postID);
        } else {
            $this->addLog('Delete post all product failed , by post ID : ' . $postID);
        }
        return $res;
    }

    /**
     * 遍历插入PDO数据
     * @param string $sql SQL语句
     * @param array $attrs 数据数组 eg:array(':id'=>array('value','PDO::PARAM_INT'),...)
     * @param int $resType 返回类型 0-boolean 1-fetch 2-fetchColumn 3-fetchAll 4-lastID
     * @param int $resFetch PDO-FETCH类型，如果返回fetchColumn则为列偏移值
     * @return boolean|PDOStatement 成功则返回PDOStatement句柄，失败返回false
     */
    private function doSQL($sql, $attrs = null, $resType = 0, $resFetch = null) {
        return $this->db->prepareAttr($sql, $attrs, $resType, $resFetch);
    }

    /**
     * 添加日志
     * @param string $message 日志消息
     */
    private function addLog($message) {
        $this->log->add($message);
    }

}

?>
