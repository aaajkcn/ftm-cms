{include file="header.tpl" includePageCSS="text"}
<body>
    {include file="menu.tpl" webTitle=$webTitle menu=$menu}
    <div class="container container-fix">
        {include file="carousel.tpl" slideShowList=$slideShowList}
        <hr>
        {if $textRes['post_title']}
        <div class="row">
            <div class="span3">
                {if $textList}
                <h3>栏目最新信息</h3>
                <small>来自 {$sortRes['post_title']} 栏目的最新信息</small>
                <hr>
                {foreach item=textValue from=$textList}
                <div class="row text-row">
                    <div class="span3">
                        <h5><a href="text.php?id={$textValue['id']}" target="_self"><i class="icon-align-justify"></i>&nbsp;{$textValue['post_title']}</a>&nbsp;<small>{$textValue['post_date']|truncate:10:''}</small></h5>
                    </div>
                </div>
                {/foreach}
                {/if}
                <div class="row">&nbsp;</div>
                <div class="row">&nbsp;</div>
                <h3>公司最新信息</h3>
                <small>来自全公司和产品的最新信息</small>
                <hr>
                {foreach item=textValue from=$textTopList}
                <div class="row text-row">
                    <div class="span3">
                        <h5><a href="text.php?id={$textValue['id']}" target="_self"><i class="icon-align-justify"></i>&nbsp;{$textValue['post_title']}</a>&nbsp;<small>{$textValue['post_date']|truncate:10:''}</small></h5>
                    </div>
                </div>
                {/foreach}
            </div>
            <div class="span9">
                {if $messageKey eq 1}
                <div class="text-center">
                    <h2>已经添加到购物车！</h2>
                </div>
                {else if $messageKey eq 2}
                <div class="text-center">
                    <h2>无法添加到购物车，可能是系统繁忙，请稍后再试！</h2>
                </div>
                {/if}
                <div class="text-center">
                    <h2>{$textRes['post_title']}</h2>
                </div>
                <div class="text-right">
                    <small>{$textRes['post_date']} [ <a href="#comment" target="_self">查看评论</a> ]</small>
                </div>
                <div class="row">&nbsp;</div>
                {if $productList}
                <hr>
                <table class="table table-bordered table-hover product-list">
                    <thead>
                        <tr>
                            <th><i class="icon-bookmark"></i>&nbsp;产品名称</th>
                            <th><i class="icon-briefcase"></i>&nbsp;描述</th>
                            <th><i class="icon-tag"></i>&nbsp;价格</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach item=productValue from=$productList}
                        <tr>
                            <td>{$productValue['pt_name']}</td>
                            <td>{$productValue['pt_description']}</td>
                            <td>{$productValue['pt_price']} 元</td>
                            <td><a href="action.php?action=buy&product={$productValue['id']}&text={$textRes['id']}&rand={$postRand}" target="_self" class="btn btn-success"><i class="icon-shopping-cart icon-white"></i>&nbsp;购买</a></td>
                        </tr>
                        {/foreach}
                    </tbody>
                </table>
                <hr>
                {/if}
                <div class="text-text">
                {$textRes['post_content']}
                </div>
            </div>
        </div>
        <hr>
        <div class="row" id="comment">
            <div class="span3"></div>
            <div class="span9">
                <h3>评论</h3>
            </div>
        </div>
        <div class="row">
            <div class="span3"></div>
            <div class="span9">
                {if $commentListRow eq 0}
                <p>等待你来评论！</p>
                {else}
                {foreach item=commentValue from=$commentList}
                <div class="media comment-child">
                  <a class="pull-left" href="#">
                    <img class="media-object" data-src="holder.js/64x64" src="http://chart.googleapis.com/chart?cht=qr&chs=150x150&choe=UTF-8&chld=H&chl={$commentValue['ct_name']}">
                  </a>
                  <div class="media-body">
                    <h4 class="media-heading">{$commentValue['ct_name']}</h4>
                    <small>{$commentValue['ct_date']} - <a href="mailto:{$commentValue['ct_email']}" target="_blank">{$commentValue['ct_email']}</a> - <a href="{$commentValue['ct_url']}" target="_blank">{$commentValue['ct_url']}</a></small>
                    <div class="media">
                        {$commentValue['ct_content']}
                    </div>
                  </div>
                </div>
                {/foreach}
                {/if}
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="span3"></div>
            <div class="span9">
                <h3>添加评论</h3>
                {if $messageKey eq 3}
                <div class="text-center">
                    <h2>添加评论成功，请等待管理员审核。</h2>
                </div>
                {else if $messageKey eq 4}
                <div class="text-center">
                    <h2>无法添加评论，可能是系统繁忙，请稍后重试！</h2>
                </div>
                {/if}
            </div>
        </div>
        <div class="row">
            <div class="span3"></div>
            <div class="span9">
                <div class="well">
                    <form class="form-horizontal" action="action.php?action=comment&text={$textRes['id']}&rand={$postRand}" method="post">
                        <div class="control-group">
                            <label class="control-label" for="inputName">姓名</label>
                            <div class="controls">
                                <input type="text" name="name" id="inputName" placeholder="姓名 (必填)">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="inputEmail">邮箱</label>
                            <div class="controls">
                                <input type="text" name="email" id="inputEmail" placeholder="邮箱@邮箱.com (必填)">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="inputURL">网站</label>
                            <div class="controls">
                                <input type="text" name="url" id="inputURL" placeholder="网站.com (必填)">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="inputContent">内容</label>
                            <div class="controls">
                                <textarea rows="5" class="textarea span5" name="content" placeholder="评论内容 (字数必须少于200字) (必填)"></textarea>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="controls">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> 发表</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {/if}
        <div id="pull"></div>
    </div>
    {include file="footer.tpl" webURL=$webURL}
  </body>
</html>