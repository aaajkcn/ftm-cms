{include file="header.tpl"}
<body>
    {include file="menu.tpl" webTitle=$webTitle menu=$menu}
    <div class="container container-fix">
        {$license}
        <div class="push-fix"></div>
        <div id="pull"></div>
    </div>
    {include file="footer.tpl" webURL=$webURL}
  </body>
</html>