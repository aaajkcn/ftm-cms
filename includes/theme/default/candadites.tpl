{include file="header.tpl"}
<body>
    {include file="menu.tpl" webTitle=$webTitle menu=$menu}
    <div class="container container-fix">
        {include file="carousel.tpl" slideShowList=$slideShowList}
        <hr>
        {if $postAdd eq 1}
        <div class="row">
            <div class="span11">
                <h1>提交成功</h1>
                <p>我们已经受理您的简历，请耐心等待处理结果。</p>
            </div>
        </div>
        <div class="push-fix"></div>
        {else if $postAdd eq 2}
        <div class="row">
            <div class="span11">
                <h1>提交失败</h1>
                <p>您提交的频率太快了，请稍等解锁后提交。</p>
            </div>
        </div>
        <div class="push-fix"></div>
        {else}
        <div class="row">
            <div class="span11">
                <h1>提交您的简历</h1>
                <p>由于我们的处理能力有限，您在{$limitTimeStr}内只能提交一次应聘信息，请谨慎填写相关信息。</p>
            </div>
        </div>
        <div class="row">
            <div class="span11 well well-large">
                {if $candaditesAddOn eq 1}
                <form class="form-horizontal" action="action.php?action=candadites&rand={$postRand}" method="post">
                    <div class="control-group">
                        <label class="control-label" for="inputName">姓名</label>
                        <div class="controls">
                            <div class="input-prepend input-append">
                                <span class="add-on"><i class="icon-user"></i></span>
                                <input type="text" name="name" id="inputName" placeholder="姓名" class="span5">
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputContact">联系电话</label>
                        <div class="controls">
                            <div class="input-prepend input-append">
                                <span class="add-on"><i class="icon-bullhorn"></i></span>
                                <input type="text" name="contact" id="inputContact" placeholder="联系电话" class="span5">
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputResume">个人介绍</label>
                        <div class="controls">
                            <div class="input-prepend input-append">
                                <span class="add-on"><i class="icon-thumbs-up"></i></span>
                                <textarea rows="8" name="resume" class="span5" placeholder="个人介绍...请勿超过3000字..."></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="control-group text-center">
                        <div class="controls">
                            <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> 提交简历</button>
                        </div>
                    </div>
                </form>
                {else}
                <p>对不起，您之前已经提交过一次简历了。请在您上次提交{$limitTimeStr}后再提交新的简历。</p>
                <div class="push-fix"></div>
                {/if}
            </div>
        </div>
        {/if}
        <div id="pull"></div>
    </div>
    {include file="footer.tpl" webURL=$webURL}
  </body>
</html>