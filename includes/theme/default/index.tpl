{include file="header.tpl" includePageCSS="index"}
<body>
    {include file="menu.tpl" webTitle=$webTitle menu=$menu}
    {if $slideShowList}
    <div id="myCarousel" class="carousel slide">
      <div class="carousel-inner">
        {foreach key=slideK item=slideV from=$slideShowList}
        <div class="item{if $slideK eq 0} active{/if}">
          <img src="image.php?id={$slideV['image']}" alt="">
          <div class="container">
            <div class="carousel-caption">
              <h1>{$slideV['post_title']}</h1>
              <p class="lead">{$slideV['post_content']}</p>
              <a class="btn btn-large btn-primary" href="{$slideV['post_url']}">了解更多</a>
            </div>
          </div>
        </div>
        {/foreach}
      </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
    </div><!-- /.carousel -->
    {/if}
    <div class="container marketing">
      <!-- Three columns of text below the carousel -->
      <div class="row product-list">
        {foreach key=productK item=productV from=$indexProductList}
        <div class="span4">
          <img class="img-circle product-list-img" data-src="holder.js/140x140" src="image.php?id={$productV['image']}">
          <h2>{$productV['post_title']}</h2>
          <p>{$productV['post_content']}</p>
          <p><a class="btn" href="{$productV['post_url']}">了解 &raquo;</a></p>
        </div><!-- /.span4 -->
        {/foreach}
      </div><!-- /.row -->
      <!-- START THE FEATURETTES -->
      <hr>
      <div class="featurette">
        <img class="featurette-image pull-right">
        <h2 class="featurette-heading">{$webTitle} <span class="muted">简介</span></h2>
        <p class="lead company-introduction">{$indexTextM}</p>
      </div>
      <hr>
      {if $indexCooperationList}
      <div class="row poo-list">
          <marquee behavior="scroll" scrollamount="3" direction="left" height="145">
                {foreach key=pooK item=pooV from=$indexCooperationList}<a href="{$pooV['post_url']}" target="_blank" title="{$pooV['post_title']}"><img class="img-circle poo-list-img" data-src="holder.js/140x140" src="image.php?id={$pooV['image']}" alt="{$pooV['post_title']}"></a>{/foreach}
           </marquee>
      </div>
      {/if}
      <div id="push"></div>
    </div><!-- /.container -->
    {include file="footer.tpl" webURL=$webURL includePageJS="index"}
  </body>
</html>
