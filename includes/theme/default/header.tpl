<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>{$webTitle} - 首页</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{$webTitle}">
    <meta name="author" content="{$webTitle}">
    <!-- Le styles -->
    <link href="includes/assets/bootstrap.css" rel="stylesheet">
    <link href="includes/assets/bootstrap-responsive.css" rel="stylesheet">
    <link href="includes/theme/default/assets/glob.css" rel="stylesheet">
    {if $includePageCSS}
    <link href="includes/theme/default/assets/{$includePageCSS}.css" rel="stylesheet">
    {/if}
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="assets/html5shiv.js"></script>
        <![endif]-->
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="includes/assets/logo-144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="includes/assets/logo-114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="includes/assets/logo-72.png">
        <link rel="apple-touch-icon-precomposed" href="includes/assets/logo-57.png">
        <link rel="shortcut icon" href="includes/assets/favicon.ico">
        <!-- ta.qq -->
        <script type="text/javascript" src="http://tajs.qq.com/stats?sId=26334886" charset="UTF-8"></script>
  </head>