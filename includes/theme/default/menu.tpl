    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse">
          <div class="navbar-inner">
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
              <a class="brand" href="index.php"><image src="includes/assets/logo-32.png" class="menu-logo" />&nbsp;{$webTitle}</a>
            <div class="nav-collapse collapse">
              <ul class="nav" id="menu">
                <li><a href="index.php">首页</a></li>
                {section name=menuName loop=$menu}
                {if $menu[menuName].child}
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">{$menu[menuName]['post_title']} <b class="caret"></b></a>
                    <ul class="dropdown-menu menu-child">
                        {section name=childName loop=$menu[menuName].child}
                        <li><a href="sort.php?id={$menu[menuName].child[childName].id}">{$menu[menuName].child[childName].post_title}</a></li>
                        {/section}
                    </ul>
                </li>
                {else}
                <li><a href="sort.php?id={$menu[menuName]['id']}">{$menu[menuName]['post_title']}</a></li>
                {/if}
                {/section}
              </ul>
              <input type="text" id="search" name="q" placeholder="搜索" class="navbar-search pull-right input-medium" data-action="http://cn.bing.com/search?q=site%3A{$webURL}">
            </div><!--/.nav-collapse -->
          </div><!-- /.navbar-inner -->
        </div><!-- /.navbar -->

      </div> <!-- /.container -->
    </div><!-- /.navbar-wrapper -->