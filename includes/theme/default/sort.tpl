{include file="header.tpl" includePageCSS='sort'}
<body>
    {include file="menu.tpl" webTitle=$webTitle menu=$menu}
    <div class="container container-fix">
        {include file="carousel.tpl" slideShowList=$slideShowList}
        <hr>
        {if $showType eq 'none'}
        <div class="row">
            <div class="span12">
                <h1>没有任何内容</h1>
                <p>该栏目下暂时没有录入信息，给您造成的不便我们深表歉意，请选择其他内容浏览。</p>
            </div>
        </div>
        <div class="push-fix"></div>
        {else if $showType eq 'sort'}
        <div class="row">
            <div class="span3">
                <h3>栏目最新信息</h3>
                <small>来自 {$sortRes['post_title']} 栏目的最新信息</small>
                <hr>
                {foreach item=textValue from=$textList}
                <div class="row text-row">
                    <div class="span3">
                        <h5><a href="text.php?id={$textValue['id']}" target="_self"><i class="icon-align-justify"></i>&nbsp;{$textValue['post_title']}</a>&nbsp;<small>{$textValue['post_date']|truncate:10:''}</small></h5>
                    </div>
                </div>
                {/foreach}
                <div class="row">&nbsp;</div>
                <div class="row">&nbsp;</div>
                <h3>公司最新信息</h3>
                <small>来自全公司和产品的最新信息</small>
                <hr>
                {foreach item=textValue from=$textTopList}
                <div class="row text-row">
                    <div class="span3">
                        <h5><a href="text.php?id={$textValue['id']}" target="_self"><i class="icon-align-justify"></i>&nbsp;{$textValue['post_title']}</a>&nbsp;<small>{$textValue['post_date']|truncate:10:''}</small></h5>
                    </div>
                </div>
                {/foreach}
            </div>
            <div class="span9">
                <h3>{$sortRes['post_title']}</h3>
                <table class="table table-hover sort-row">
                    <tbody>
                        {foreach item=sortValue from=$sortList}
                        <tr>
                            <td>&nbsp;&nbsp;<i class="icon-tags"></i>&nbsp;<a href="sort.php?id={$sortValue['id']}" target="_self">{$sortValue['post_title']}</a></td>
                            <td>&nbsp;&nbsp;<small>{$sortValue['post_content']}</small></td>
                        </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
        <hr>
        <div class="row text-center">
            {$pageStr}
        </div>
        {else if $showType eq 'text'}
        <div class="row">
            <div class="span3">
                <h3>公司最新信息</h3>
                <small>来自全公司和产品的最新信息</small>
                <hr>
                {foreach item=textValue from=$textTopList}
                <div class="row text-row">
                    <div class="span3">
                        <h5><a href="text.php?id={$textValue['id']}" target="_self"><i class="icon-align-justify"></i>&nbsp;{$textValue['post_title']}</a>&nbsp;<small>{$textValue['post_date']|truncate:10:''}</small></h5>
                    </div>
                </div>
                {/foreach}
            </div>
            <div class="span9">
                <h3>{$sortRes['post_title']}</h3>
                <table class="table table-hover sort-row">
                    <tbody>
                        {foreach item=textValue from=$textList}
                        <tr>
                            <td>&nbsp;&nbsp;<i class="icon-align-justify"></i>&nbsp;&nbsp;<a href="text.php?id={$textValue['id']}" target="_self">{$textValue['post_title']}</a></td>
                            <td>&nbsp;&nbsp;<small>{$textValue['post_date']}</small></td>
                        </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
        <hr>
        <div class="row text-center">
            <div class="span12">{$pageStr}</div>
        </div>
        {/if}
        <div id="pull"></div>
    </div>
    {include file="footer.tpl" webURL=$webURL}
  </body>
</html>