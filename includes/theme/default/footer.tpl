      <div id="footer">
        <div class="container">
            <p class="muted credit">
                {$webFooterStr} , <a href="license.php">Business License</a> .
                Code & Desgin by <a href="http://fotomxq.me">Liuzilu</a> . &nbsp;&nbsp;&nbsp;
                <a href="shop.php" target="_self"><i class="icon-shopping-cart"></i>&nbsp;在线订单</a>&nbsp;&nbsp;
                <a href="candadites.php" target="_self"><i class="icon-briefcase"></i>&nbsp;在线应聘</a>
            </p>
        </div>
      </div>
        <script src="includes/assets/jquery.js"></script>
        <script src="includes/assets/bootstrap.js"></script>
        <script src="includes/assets/holder.js"></script>
        <script src="includes/theme/default/assets/glob.js"></script>
        {if $includePageJS}
        <script src="includes/theme/default/assets/{$includePageJS}.js"></script>
        {/if}
