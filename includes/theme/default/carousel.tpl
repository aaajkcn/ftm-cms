{if $slideShowList}
<div id="myCarousel" class="carousel slide">
    <ol class="carousel-indicators">
        {foreach key=slideK item=slideV from=$slideShowList}
        <li data-target="#myCarousel" data-slide-to="0"{if $slideK eq 0} class="active"{/if}></li>
        {/foreach}
    </ol>
    <div class="carousel-inner">
    {foreach key=slideK item=slideV from=$slideShowList}
    <div class="{if $slideK eq 0}active {/if} item">
        <img src="image.php?id={$slideV['image']}" alt="">
        <div class="carousel-caption">
            <h4>{$slideV['post_title']}</h4>
            <p>{$slideV['post_content']}</p>
        </div>
    </div>
    {/foreach}
    </div>
    <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
    <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
</div>
{/if}