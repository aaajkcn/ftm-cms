!function($) {
    $(function() {
        //主菜单栏显示
        $('#menu li').hover(function() {
            $(this).button("toggle");
            $(this).addClass('open');
        }, function() {
            $(this).button("toggle");
            $(this).removeClass('open');
        });
        //search
        $('#search').keydown(function(event){
            if(event.which == 13){
                window.location.href = $(this).attr('data-action')+' '+$(this).val();
            }
        });
    })
}(window.jQuery);