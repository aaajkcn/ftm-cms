{include file="header.tpl"}
<body>
    {include file="menu.tpl" webTitle=$webTitle menu=$menu}
    <div class="container container-fix">
        {include file="carousel.tpl" slideShowList=$slideShowList}
        <hr>
        <div class="row">
            <div class="span1"></div>
            <div class="span11">
                <h2>购物车清单</h2>
            </div>
        </div>
        {if $messageKey eq 1}
        <div class="row">
            <div class="span1"></div>
            <div class="span11">
                <h3>订单已经成功提交到公司！</h3>
                <p>我们会尽快处理您的交易请求！如果您需要了解更多产品或其他细节，请联系我们。</p>
            </div>
        </div>
        {else if  $messageKey eq 2}
        {/if}
        {if $buyList}
        <div class="row">
            <div class="span1"></div>
            <div class="span11">
                    <table class="table table-bordered table-hover product-list">
                        <thead>
                            <tr>
                                <th><i class="icon-bookmark"></i>&nbsp;产品名称</th>
                                <th><i class="icon-briefcase"></i>&nbsp;描述</th>
                                <th><i class="icon-tag"></i>&nbsp;价格</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=productValue from=$buyList}
                            <tr>
                                <td>{$productValue['pt_name']}</td>
                                <td>{$productValue['pt_description']}</td>
                                <td>{$productValue['pt_price']} 元</td>
                                <td><a href="action.php?action=clear-buy&product={$productValue['id']}&rand={$postRand}" target="_self" class="btn btn-warning"><i class="icon-trash icon-white"></i>&nbsp;删除</a></td>
                            </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>总金额</th>
                                <th></th>
                                <th>{$buyPrice} 元</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="span1"></div>
            <div class="span11">
                <form class="form-horizontal" action="action.php?action=order&rand={$postRand}" method="post">
                    <div class="control-group">
                        <label class="control-label" for="inputName">联系人姓名</label>
                        <div class="controls">
                            <input type="text" name="name" id="inputName" placeholder="联系人姓名 (必填)">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputContact">联系电话</label>
                        <div class="controls">
                            <input type="text" name="contact" id="inputContact" placeholder="联系电话 (必填)">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputNote">地址和备注信息</label>
                        <div class="controls">
                            <textarea rows="5" class="textarea span5" name="note" placeholder="地址和备注信息 (字数必须少于500字) (必填)"></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <button type="submit" href="#" class="btn btn-primary btn-large"><i class="icon-tags icon-white"></i>&nbsp;结算</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        {else}
        <div class="row">
            <div class="span1"></div>
            <div class="span11">
                <p>请先购买产品，然后再进入当前页面结算。</p>
            </div>
        </div>
        {/if}
        <div class="push-fix"></div>
        <div id="pull"></div>
    </div>
    {include file="footer.tpl" webURL=$webURL}
  </body>
</html>