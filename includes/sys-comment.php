<?php

/**
 * 评论操作类
 * @author fotomxq <fotomxq.me>
 * @version 2
 * @package sys
 */
class SysComment {

    /**
     * 数据表名称
     * @var string 
     */
    private $tableName;

    /**
     * 数据库句柄
     * @var CoreDB 
     */
    private $db;

    /**
     * 日志句柄
     * @var SysLog 
     */
    private $log;

    /**
     * 字段列
     * @var array 
     */
    private $fields = array('id', 'post_id', 'ct_status', 'ct_parent', 'ct_date', 'ct_ip', 'ct_name', 'ct_email', 'ct_url', 'ct_content');

    /**
     * 状态
     * @var array 
     */
    public $status = array('audit', 'public', 'trash');

    /**
     * 初始化
     * @param CoreDB $db 数据库句柄
     * @param SysLog $log 日志句柄
     */
    public function __construct(&$db, &$log) {
        $this->db = $db;
        $this->log = $log;
        $this->tableName = $db->tables['comment'];
    }

    /**
     * 获取列表
     * @param string $where 条件语句
     * @param array $attrs 过滤参数
     * @param int $page 页数
     * @param int $max 页长
     * @param int $sort 排序字段键值
     * @param boolean $desc 是否倒叙
     * @return array 数据数组
     */
    public function getList($where = '1', $attrs = null, $page = 1, $max = 10, $sort = 0, $desc = false) {
        $sortField = isset($this->fields[$sort]) == true ? $this->fields[$sort] : $this->fields[0];
        $descStr = $desc == true ? 'DESC' : 'ASC';
        $sql = 'SELECT `id`,`post_id`,`ct_status`,`ct_parent`,`ct_date`,`ct_ip`,`ct_name`,`ct_email`,`ct_url`,`ct_content` FROM `' . $this->tableName . '` WHERE ' . $where . ' ORDER BY `' . $sortField . '` ' . $descStr . ' LIMIT ' . ($page - 1) * $max . ',' . $max;
        return $this->doSQL($sql, $attrs, 3, PDO::FETCH_ASSOC);
    }

    /**
     * 获取记录数
     * @param string $where 条件语句
     * @param array $attrs 过滤参数
     * @return int 记录数
     */
    public function getListRow($where = '1', $attrs = null) {
        $sql = 'SELECT count(`id`) FROM `' . $this->tableName . '` WHERE ' . $where;
        return $this->doSQL($sql, $attrs, 2, 0);
    }

    /**
     * 获取评论
     * @param int $target 目标ID
     * @return array 数据数组
     */
    public function get($target) {
        $target = (int) $target;
        $sql = 'SELECT `id`,`post_id`,`ct_status`,`ct_parent`,`ct_date`,`ct_ip`,`ct_name`,`ct_email`,`ct_url`,`ct_content` FROM `' . $this->tableName . '` WHERE `id` = :id';
        $attrs = array(':id' => array($target, PDO::PARAM_INT));
        return $this->doSQL($sql, $attrs, 1, PDO::FETCH_ASSOC);
    }

    /**
     * 添加评论
     * @param int $postID POST-ID
     * @param int $parent 回复ID
     * @param string $ip Ip地址
     * @param string $name 名字
     * @param string $email 邮箱
     * @param string $url URL
     * @param string $content 内容
     * @param boolean $audit 审核开关
     * @return int 添加ID，失败则返回0
     */
    public function add($postID, $parent, $ip, $name, $email, $url, $content, $audit = true) {
        if ($parent == 0 || $this->get($parent)) {
            $status = $audit == true ? $this->status[0] : $this->status[1];
            $sql = 'INSERT INTO `' . $this->tableName . '`(`id`,`post_id`,`ct_status`,`ct_parent`,`ct_date`,`ct_ip`,`ct_name`,`ct_email`,`ct_url`,`ct_content`) VALUES(NULL,:post,\'' . $status . '\',:parent,NOW(),:ip,:name,:email,:url,:content)';
            $attrs = array(
                ':post' => array($postID, PDO::PARAM_INT),
                ':parent' => array($parent, PDO::PARAM_INT),
                ':ip' => array($ip, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT),
                ':name' => array($name, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT),
                ':email' => array($email, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT),
                ':url' => array($url, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT),
                ':content' => array($content, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT)
            );
            $res = $this->doSQL($sql, $attrs, 4);
            if ($res > 0) {
                $this->addLog('Add comment success , by ID : ' . $res);
            } else {
                $this->addLog('Add comment failed.');
            }
            return $res;
        }
        return 0;
    }

    /**
     * 修改评论状态
     * @param int|array $target 目标ID或ID列
     * @param string $status 状态
     * @return boolean
     */
    public function changeStatus($target, $status) {
        if (is_array($target) == true) {
            foreach ($target as $v) {
                if ($this->editStatus($v, $status) == false) {
                    $this->addLog('Change comment status failed , by ID : ' . $v . ' , status : ' . $status);
                    return false;
                }
            }
            $this->addLog('Change comment status success , by ID list , status : ' . $status);
            return true;
        } else {
            $res = $this->editStatus($target, $status);
            if ($res == true) {
                $this->addLog('Change comment status success , by ID : ' . $target . ' , status : ' . $status);
            } else {
                $this->addLog('Change comment status failed , by ID : ' . $target . ' , status : ' . $status);
            }
            return $res;
        }
    }

    /**
     * 删除评论
     * @param int $target 目标ID
     * @return boolean 是否成功
     */
    public function del($target) {
        $target = (int) $target;
        if ($this->delParent($target) == true) {
            $sql = 'DELETE FROM `' . $this->tableName . '` WHERE `id` = :id';
            $attrs = array(':id' => array($target, PDO::PARAM_INT));
            $res = $this->doSQL($sql, $attrs);
            if ($res == true) {
                $this->addLog('Delete comment success , by ID : ' . $target);
            } else {
                $this->addLog('Delete comment failed , by ID : ' . $target);
            }
            return $res;
        }
        return false;
    }

    /**
     * 删除post下所有评论
     * @param int $postID POST-ID
     * @return boolean 是否成功
     */
    public function delAll($postID) {
        $sql = 'DELETE FROM `' . $this->tableName . '` WHERE `post_id` = :id';
        $attrs = array(':id' => array($postID, PDO::PARAM_INT));
        $res = $this->doSQL($sql, $attrs);
        if ($res == true) {
            $this->addLog('Delete post all comment success , by post ID : ' . $postID);
        } else {
            $this->addLog('Delete post all comment failed , by post ID : ' . $postID);
        }
        return $res;
    }

    /**
     * 删除子评论
     * @param int $target 目标ID
     * @return boolean 是否成功
     */
    private function delParent($target) {
        $sql = 'SELECT `id` FROM `' . $this->tableName . '` WHERE `ct_parent` = :id';
        $attrs = array(':id' => array($target, PDO::PARAM_INT));
        $res = $this->doSQL($sql, $attrs, 3, PDO::FETCH_ASSOC);
        if ($res) {
            foreach ($res as $v) {
                if ($this->delParent($v['id']) == false) {
                    return false;
                }
            }
            $sqlDelete = 'DELETE FROM `' . $this->tableName . '` WHERE `ct_parent` = :id';
            $attrsDelete = array(':id' => array($target, PDO::PARAM_INT));
            return $this->doSQL($sqlDelete, $attrsDelete);
        } else {
            return true;
        }
    }

    /**
     * 修改评论状态
     * @param int $id 目标ID
     * @param string $status 状态
     * @return boolean 是否成功
     */
    private function editStatus($id, $status) {
        $id = (int) $id;
        $sql = 'UPDATE `' . $this->tableName . '` SET `ct_status` = :status WHERE `id` = :id';
        $attrs = array(':id' => array($id, PDO::PARAM_INT), ':status' => array($status, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT));
        return $this->doSQL($sql, $attrs);
    }

    /**
     * 添加日志
     * @param string $message 消息
     */
    private function addLog($message) {
        $this->log->add($message);
    }

    /**
     * 遍历插入PDO数据
     * @param string $sql SQL语句
     * @param array $attrs 数据数组 eg:array(':id'=>array('value','PDO::PARAM_INT'),...)
     * @param int $resType 返回类型 0-boolean 1-fetch 2-fetchColumn 3-fetchAll 4-lastID
     * @param int $resFetch PDO-FETCH类型，如果返回fetchColumn则为列偏移值
     * @return boolean|PDOStatement 成功则返回PDOStatement句柄，失败返回false
     */
    private function doSQL($sql, $attrs = null, $resType = 0, $resFetch = null) {
        return $this->db->prepareAttr($sql, $attrs, $resType, $resFetch);
    }

}

?>
