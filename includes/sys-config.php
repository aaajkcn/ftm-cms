<?php

/**
 * 配置操作类
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package sys
 */
class SysConfig {

    /**
     * 数据库句柄
     * @var CoreDB
     */
    private $db;

    /**
     * 表名称
     * @var string 
     */
    private $tableNameConfig;

    /**
     * 内部配置缓冲
     * @var array 
     */
    private $vals;

    /**
     * 初始化
     * @param CoreDB $db 数据库句柄
     */
    public function __construct(&$db) {
        $this->db = $db;
        $this->tableNameConfig = $db->tables['config'];
    }

    /**
     * 获取配置
     * @param array|string|int $names 单一或多个配置名称 eg: 'config1' | array('c1','c2'...) | 1 | array(1,2,5)
     * @return string 配置值
     */
    public function load($names) {
        if (is_array($names) == true) {
            $vals;
            foreach ($names as $v) {
                if (isset($this->vals[$v]) == true) {
                    $vals[$v] = $this->vals[$v];
                } else {
                    $vals[$v] = $this->loadConfigValue($v);
                    $this->vals[$v] = $vals[$v];
                }
            }
            return $vals;
        } else {
            if (isset($this->vals[$names]) == true) {
                return $this->vals[$names];
            } else {
                $this->vals[$names] = $this->loadConfigValue($names);
                return $this->vals[$names];
            }
        }
    }

    /**
     * 保存配置
     * @param array $vals 新的配置数组 eg: array('config1'=>'value1',...)
     * @return boolean 是否成功
     */
    public function save($vals) {
        try {
            $this->db->beginTransaction();
            $sql = 'UPDATE `' . $this->tableNameConfig . '` SET';
            foreach ($vals as $k => $v) {
                $where = '';
                $key = '';
                $attrs;
                if (is_int($v) == true) {
                    $key = ':id' . $k;
                    $where = ' `id` = ' . $key . 'w';
                    $attrs[$key] = array($v, PDO::PARAM_INT);
                } else {
                    $key = ':name' . $k;
                    $where = ' `cg_name` = ' . $key . 'w';
                    $attrs[$key] = array($v, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT);
                }
                $sql .= ' `cg_value` = ' . $key . ' WHERE' . $where;
                $attrs[$key . 'w'] = array($k, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT);
                $this->db->prepareAttr($sql, $attrs);
            }
            return $this->db->commit();
        } catch (PDOException $e) {
            $this->db->rollBack();
            return false;
        }
    }

    /**
     * 还原配置
     * @param string|int|array $names 单个或多个配置，且细分为配置名称或Id
     * @return boolean
     */
    public function restore($names = '') {
        if ($names == '') {
            $sql = 'UPDATE `' . $this->tableNameConfig . '` SET `cg_value` = `cg_default`';
            return $this->db->exec($sql) !== false ? true : false;
        } else {
            $sql = 'UPDATE `' . $this->tableNameConfig . '` SET `cg_value` = `cg_default` WHERE';
            $where = '';
            $attrs;
            if (is_string($names) == true || is_int($names) == true) {
                if (is_string($names) == true) {
                    $where = '`cg_name` = :name';
                    $attrs = array(':name' => array($names, PDO::PARAM_STR));
                } else {
                    $where = '`id` = :id';
                    $attrs = array(':id' => array($names, PDO::PARAM_INT));
                }
                $sql .= ' ' . $where;
                return $this->db->prepareAttr($sql, $attrs);
            } else {
                foreach ($names as $k => $v) {
                    $key = '';
                    if (is_int($v) == true) {
                        $key = ':id' . $k;
                        $where .= ' OR `id` = ' . $key;
                    } else {
                        $key = ':name' . $k;
                        $where .= ' OR `cg_name` = ' . $key;
                    }
                    $attrs[$key] = array($v, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT);
                }
                $where = substr($where, 4);
                $sql .= ' ' . $where;
                return $this->db->prepareAttr($sql, $attrs);
            }
        }
    }

    /**
     * 读取配置
     * @param string $name 名称
     * @param boolean $onlyValue 是否只读配置值
     * @return string 配置值
     */
    private function loadConfigValue($name, $onlyValue = true) {
        $sql = 'SELECT `id`,`cg_name`,`cg_value`,`cg_default` FROM `' . $this->tableNameConfig . '` WHERE ';
        $attrs;
        if (is_int($name) == true) {
            $sql .= '`id` = :id';
            $attrs[':id'] = array($name, PDO::PARAM_INT);
        } else {
            $sql .= '`cg_name` = :name';
            $attrs[':name'] = array($name, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT);
        }
        if ($onlyValue == true) {
            return $this->db->prepareAttr($sql, $attrs, 2, 2);
        } else {
            return $this->db->prepareAttr($sql, $attrs, 1, PDO::FETCH_ASSOC);
        }
    }

}

?>
