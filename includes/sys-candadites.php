<?php

/**
 * 应聘处理器
 * <p>额外扩展 : CoreHeader</p>
 * @author fotomxq <fotomxq.me>
 * @version 1 
 * @package sys
 */
class SysCandadites {

    /**
     * 数据表名称
     * @var string 
     */
    private $tableName;

    /**
     * 数据库句柄
     * @var CoreDB 
     */
    private $db;

    /**
     * 日志句柄
     * @var SysLog 
     */
    private $log;

    /**
     * 字段列
     * @var array 
     */
    private $fields = array('id', 'cs_resume', 'cs_contact', 'cs_name', 'cs_date', 'cs_status', 'cs_ip');

    /**
     * 求职请求最短时间 (防止重复和频繁提交)
     * @var int 
     */
    private $limitTime;

    /**
     * 状态
     * @var array
     */
    public $status = array('audit', 'pass', 'veto');

    /**
     * 初始化
     * @param CoreDB $db 数据库句柄
     * @param SysLog $log 日志句柄
     * @param int $limitTime 求职请求最短时间 (防止重复和频繁提交)
     */
    public function __construct(&$db, &$log, $limitTime) {
        $this->db = $db;
        $this->log = $log;
        $this->tableName = $db->tables['candadites'];
        $this->limitTime = $limitTime;
    }

    /**
     * 获取列表
     * @param string $where 条件语句
     * @param array $attrs 过滤参数
     * @param int $page 页数
     * @param int $max 页长
     * @param int $sort 排序字段键值
     * @param boolean $desc 是否倒叙
     * @return array 数据数组，如果不存在则返回null
     */
    public function getList($where = '1', $attrs = null, $page = 1, $max = 10, $sort = 0, $desc = false) {
        $sortField = isset($this->fields[$sort]) == true ? $this->fields[$sort] : $this->fields[0];
        $descStr = $desc == true ? 'DESC' : 'ASC';
        $sql = 'SELECT `id`,`cs_resume`,`cs_contact`,`cs_name`,`cs_date`,`cs_status`,`cs_ip` FROM `' . $this->tableName . '` WHERE ' . $where . ' ORDER BY ' . $sortField . ' ' . $descStr . ' LIMIT ' . ($page - 1) * $max . ',' . $max;
        return $this->doSQL($sql, $attrs, 3, PDO::FETCH_ASSOC);
    }

    /**
     * 返回记录数
     * @param string $where 条件语句
     * @param array $attrs 过滤参数
     * @return int 记录数
     */
    public function getListRow($where = '1', $attrs = null) {
        $sql = 'SELECT COUNT(`id`) FROM `' . $this->tableName . '` WHERE ' . $where;
        return $this->doSQL($sql, $attrs, 2, 0);
    }

    /**
     * 获取记录
     * @param int $target 目标ID
     * @return array 记录数据数组
     */
    public function get($target) {
        $id = (int) $target;
        $sql = 'SELECT `id`,`cs_resume`,`cs_contact`,`cs_name`,`cs_date`,`cs_status`,`cs_ip` FROM `' . $this->tableName . '` WHERE `id` = :id';
        $attrs = array(':id' => array($id, PDO::PARAM_INT));
        return $this->doSQL($sql, $attrs, 1, PDO::FETCH_ASSOC);
    }

    /**
     * 添加求职
     * @param string $resume 简历内容
     * @param string $contact 联系方式
     * @param string $name 姓名
     * @param string $ip IP地址
     * @return int 新的ID，失败则返回0
     */
    public function add($resume, $contact, $name, $ip) {
        if ($this->checkAdd($ip) == true) {
            $sql = 'INSERT INTO `' . $this->tableName . '`(`id`,`cs_resume`,`cs_contact`,`cs_name`,`cs_date`,`cs_status`,`cs_ip`) VALUES(NULL,:resume,:contact,:name,NOW(),\'' . $this->status[0] . '\',:ip)';
            $attrs = array(
                ':resume' => array($resume, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT),
                ':contact' => array($contact, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT),
                ':name' => array($name, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT),
                ':ip' => array($ip, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT)
            );
            $res = $this->doSQL($sql, $attrs, 4);
            if ($res > 0) {
                $this->addLog('Add candadites success , by ID : ' . $res);
            } else {
                $this->addLog('Add candadites failed.');
            }
            return $res;
        }
        return 0;
    }

    /**
     * 检查是否可添加
     * @param string $ip IP地址
     * @return boolean 是否可以添加
     */
    public function checkAdd($ip) {
        $limitTimeSince = time() - $this->limitTime;
        $limitDate = date('Y-m-d H:i:s', $limitTimeSince);
        $sqlSelect = 'SELECT `cs_date` FROM `' . $this->tableName . '` WHERE `cs_ip` = :ip and `cs_date` > :date ORDER BY `id` DESC';
        $attrsSelect = array(':ip' => array($ip, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT), ':date' => array($limitDate, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT));
        $resSelect = $this->doSQL($sqlSelect, $attrsSelect, 2, 0);
        if (!$resSelect) {
            return true;
        }
        return false;
    }

    /**
     * 修改求职状态
     * @param int $id 求职ID
     * @param string $status 状态
     * @return boolean 是否成功
     */
    public function changeStatus($id, $status) {
        $sql = 'UPDATE `' . $this->tableName . '` SET `cs_status` = :status WHERE `id` = :id';
        $attrs = array(':id' => array($id, PDO::PARAM_INT), ':status' => array($status, PDO::PARAM_STR));
        $res = $this->doSQL($sql, $attrs);
        if ($res == true) {
            $this->addLog('Change candadites status to ' . $status . ' success , by ID : ' . $id);
        } else {
            $this->addLog('Change candadites status to ' . $status . ' failed , by ID : ' . $id);
        }
        return $res;
    }

    /**
     * 删除求职
     * @param int $id 求职ID
     * @return boolean 是否成功
     */
    public function del($id) {
        $res = $this->get($id);
        if ($res) {
            $sql = 'DELETE FROM `' . $this->tableName . '` WHERE `id` = :id';
            $attrs = array(':id' => array($id, PDO::PARAM_INT));
            $res = $this->doSQL($sql, $attrs);
            if ($res == true) {
                $this->addLog('Delete candadites success , by ID : ' . $id);
            } else {
                $this->addLog('Delete candadites failed , by ID : ' . $id);
            }
            return $res;
        }
        return false;
    }

    /**
     * 添加日志
     * @param string $message 日志消息
     */
    private function addLog($message) {
        $this->log->add($message);
    }

    /**
     * 遍历插入PDO数据
     * @param string $sql SQL语句
     * @param array $attrs 数据数组 eg:array(':id'=>array('value','PDO::PARAM_INT'),...)
     * @param int $resType 返回类型 0-boolean 1-fetch 2-fetchColumn 3-fetchAll 4-lastID
     * @param int $resFetch PDO-FETCH类型，如果返回fetchColumn则为列偏移值
     * @return boolean|PDOStatement 成功则返回PDOStatement句柄，失败返回false
     */
    private function doSQL($sql, $attrs = null, $resType = 0, $resFetch = null) {
        return $this->db->prepareAttr($sql, $attrs, $resType, $resFetch);
    }

}

?>
