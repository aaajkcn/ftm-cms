<?php

/**
 * 语言操作
 * <p>需要额外的扩展 : CoreFile</p>
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package sys
 */
class SysLanguage {

    /**
     * 语言类型
     * @var string 
     */
    private $language;

    /**
     * 读出的语言数据
     * @var array 
     */
    private $languageStrArr;

    /**
     * 默认的语言
     * @var string 
     */
    private $default;
    
    /**
     * 可用语言包列表
     * @var array 
     */
    private $languageList;

    /**
     * 初始化
     * @param string $language 语言
     * @param string $default 默认语言，如果找不到内容则参考默认语言包
     */
    public function __construct($language, $default = 'sc') {
        $this->load($language);
        $this->default = $default;
        $this->getList();
    }

    /**
     * 获取语言
     * @param string $name 标识
     * @return string 语言字符串
     */
    public function getStr($name) {
        if (isset($this->languageStrArr[$name]) == true) {
            return $this->languageStrArr[$name];
        }
        return '';
    }
    
    /**
     * 获取语言包列表
     * @return array
     */
    public function getLanguageList(){
        return $this->languageList;
    }

    /**
     * 读取语言包
     * @param string $language 语言
     * @return boolean 是否成功
     */
    private function load($language) {
        $this->language = $language;
        return false;
    }
    
    private function getList(){
        
    }

}

?>
