<?php

/**
 * 用户提交post核心类
 * @author fotomxq <fotomxq.me>
 * @version 2
 * @package sys
 */
class SysPost {

    /**
     * 数据表名称
     * @var string 
     */
    private $tableName;

    /**
     * 数据库句柄
     * @var CoreDB 
     */
    private $db;

    /**
     * 日志句柄 
     * @var SysLog 
     */
    private $log;

    /**
     * 字段数组
     * @var array 
     */
    private $fieldArr = array('id', 'post_parent', 'post_title', 'post_order', 'post_url', 'post_date', 'post_modified', 'post_password', 'post_content', 'post_status', 'post_user', 'post_type', 'post_view', 'post_meta');

    /**
     * 所有字段
     * @var type 
     */
    private $fields;

    /**
     * 记录状态列表
     * <p>public - 公开</p>
     * <p>private - 私人</p>
     * <p>trash - 回收站</p>
     * <p>draft - 草稿</p>
     * <p>audit - 等待审核</p>
     * @var array 
     */
    public $status = array('public', 'private', 'trash', 'draft', 'audit');

    /**
     * 注册的类型
     * <p>sort - 分类 ; file - 文件 ; text - 文本 ; comment - 评论 ; product - 产品信息 ; job - 应聘信息 ; index-slideshow - 首页幻灯片 ; index-prolist - 首页产品目录 ; index-cooperation - 首页赞助商</p>
     * @var array 
     */
    public $type = array('sort', 'file', 'text', 'comment', 'product', 'job', 'index-slideshow', 'index-prolist', 'index-cooperation');

    /**
     * 初始化
     * @param CoreDB $db 数据库句柄
     * @param SysLog $log 日志句柄
     */
    public function __construct(&$db, &$log) {
        $this->db = $db;
        $this->tableName = $db->tables['post'];
        $this->log = $log;
        $this->fields = '`' . implode('`,`', $this->fieldArr) . '`';
    }

    /**
     * 获取列表
     * @param string $where 条件语句
     * @param array $attrs 过滤参数
     * @param int $page 页数
     * @param int $max 页长
     * @param int $sort 排序字段键值
     * @param boolean $desc 是否倒叙
     * @return array 列表数据数组
     */
    public function getList($where = '1', $attrs = null, $page = 1, $max = 10, $sort = 3, $desc = true, $contentBool = false) {
        $sortField = isset($this->fieldArr[$sort]) == true ? $this->fieldArr[$sort] : $this->fieldArr[0];
        $descStr = $desc == true ? 'DESC' : 'ASC';
        $page = (int) $page;
        $max = (int) $max;
        $contentStr = $contentBool == true ? ',`post_content`' : '';
        $sql = 'SELECT `id`,`post_parent`,`post_title`,`post_order`,`post_url`,`post_date`,`post_modified`,`post_password`' . $contentStr . ',`post_status`,`post_user`,`post_type`,`post_view`,`post_meta` FROM `' . $this->tableName . '` WHERE ' . $where . ' ORDER BY ' . $sortField . ' ' . $descStr . ' LIMIT ' . ($page - 1) * $max . ',' . $max;
        return $this->doSQL($sql, $attrs, 3, PDO::FETCH_ASSOC);
    }

    /**
     * 获取记录数
     * @param string $where 条件语句
     * @param array $attrs 过滤参数
     * @return int 记录数
     */
    public function getListRow($where = '1', $attrs = null) {
        $sql = 'SELECT count(`id`) FROM `' . $this->tableName . '` WHERE ' . $where;
        return $this->doSQL($sql, $attrs, 2, 0);
    }

    /**
     * 获取数据
     * <p>只能获取公开数据。</p>
     * <p>如果密码提交为null，则忽略该密码。</p>
     * @param int $target ID
     * @param string $passwd 密码，如果存在则必须提交，否则无法获取数据
     * @param boolean $viewBool 是否计数访问次数
     * @return array 数据数组
     */
    public function get($target, $passwd = null, $viewBool = false) {
        $passwdStr = $passwd === null ? ' or 1' : '';
        $sql = 'SELECT ' . $this->fields . ' FROM `' . $this->tableName . '` WHERE (`post_password` = :passwd or `post_password` is null or `post_password` = \'\'' . $passwdStr . ') and `id` = :id and `post_status` = \'public\'';
        $attrs = array(':passwd' => array($passwd, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT), ':id' => array($target, PDO::PARAM_INT));
        if ($viewBool == true) {
            $this->editView($target);
        }
        return $this->doSQL($sql, $attrs, 1, PDO::FETCH_ASSOC);
    }

    /**
     * 添加
     * @param int $userID 用户ID
     * @param string $title 标题
     * @param string $content 内容
     * @param string $url URL
     * @param string $status 状态
     * @param int $parent 上级ID
     * @param int $order 排序
     * @param string $passwd 密码
     * @param string $type 类型
     * @param string $meta 媒体
     * @return int 插入的ID，失败则返回0
     */
    public function add($userID, $title, $content = '', $url = null, $status = 'public', $parent = 0, $order = 0, $passwd = null, $type = 'text', $meta = 'text/html') {
        $sql = 'INSERT INTO `' . $this->tableName . '`(' . $this->fields . ') VALUES(NULL,:parent,:title,:order,:url,NOW(),NULL,:passwd,:content,:status,:user,:type,0,:meta)';
        $attrs = array(
            ':parent' => array($parent, PDO::PARAM_INT),
            ':title' => array($title, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT),
            ':order' => array($order, PDO::PARAM_INT),
            ':url' => array($url, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT | PDO::PARAM_NULL),
            ':passwd' => array($passwd, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT | PDO::PARAM_NULL),
            ':content' => array($content, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT),
            ':status' => array($status, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT),
            ':user' => array($userID, PDO::PARAM_INT),
            ':type' => array($type, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT),
            ':meta' => array($meta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT)
        );
        $resID = $this->doSQL($sql, $attrs, 4);
        if ($resID > 0) {
            $this->addLog('Add post success,by ID : ' . $resID);
            $this->updateOrder();
            return $resID;
        } else {
            $this->addLog('Add post failed.');
        }
        return 0;
    }

    /**
     * 编辑
     * @param int $target 目标ID
     * @param int $userID 用户ID
     * @param string $title 标题
     * @param string $content 内容
     * @param string $url URL
     * @param string $status 状态
     * @param int $parent 上一级ID
     * @param int $order 排序
     * @param string $passwd 密码
     * @return boolean 是否成功
     */
    public function edit($target, $userID, $title, $content = '', $url = null, $status = 'public', $parent = 0, $order = 0, $passwd = null) {
        $sql = 'UPDATE `' . $this->tableName . '` SET `post_title` = :title,`post_content` = :content,`post_url` = :url,`post_status` = :status,`post_parent` = :parent,`post_order` = :order,`post_password` = :passwd WHERE `id` = :id and `post_user` = :user';
        $attrs = array(
            ':parent' => array($parent, PDO::PARAM_INT),
            ':title' => array($title, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT),
            ':order' => array($order, PDO::PARAM_INT),
            ':url' => array($url, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT | PDO::PARAM_NULL),
            ':passwd' => array($passwd, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT | PDO::PARAM_NULL),
            ':content' => array($content, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT),
            ':status' => array($status, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT),
            ':user' => array($userID, PDO::PARAM_INT),
            ':id' => array($target, PDO::PARAM_INT)
        );
        $res = $this->doSQL($sql, $attrs);
        if ($res == true) {
            $this->addLog('Edit post success,by ID : ' . $target);
            return true;
        } else {
            $this->addLog('Edit post failed,by ID : ' . (int) $target);
        }
        return false;
    }

    /**
     * 增加访问数
     * @param int $target ID
     * @return boolean 是否成功
     */
    public function editView($target) {
        $sql = 'UPDATE `' . $this->tableName . '` SET `post_view` = post_view + 1 WHERE `id` = :id';
        $attrs = array(':id' => array($target, PDO::PARAM_INT));
        $res = $this->doSQL($sql, $attrs);
        if ($res == true) {
            //$this->addLog('Edit post view success,by ID : ' . $target);
            return true;
        } else {
            $this->addLog('Edit post view failed,by ID : ' . (int) $target);
        }
        return false;
    }

    /**
     * 彻底删除
     * <p>建议在此之前使用edit标记status为trash，然后再进行该操作。</p>
     * <p>将递归删除所有子元素。</p>
     * @param int $target 目标ID
     * @return boolean 是否成功
     */
    public function del($target) {
        $target = (int) $target;
        if ($this->delParent($target) == true) {
            $sql = 'DELETE FROM `' . $this->tableName . '` WHERE `id` = :id';
            $attrs = array(':id' => array($target, PDO::PARAM_INT));
            $res = $this->doSQL($sql, $attrs);
            if ($res == true) {
                $this->addLog('Delete post success,by ID : ' . $target);
                return true;
            } else {
                $this->addLog('Delete post failed,by ID : ' . (int) $target);
            }
        }
        return false;
    }

    /**
     * 更新排序数据
     * @return boolean 是否成功
     */
    private function updateOrder() {
        $sqlOrder = 'UPDATE `' . $this->tableName . '` SET `post_order` = `id` WHERE `post_order` = 0 or `post_order` is null';
        return $this->db->exec($sqlOrder);
    }

    /**
     * 递归删除子集
     * @param int $target 目标ID
     * @return boolean 是否成功
     */
    private function delParent($target) {
        $sql = 'SELECT `id` FROM `' . $this->tableName . '` WHERE `post_parent` = :id';
        $attrs = array(':id' => array($target, PDO::PARAM_INT));
        $res = $this->doSQL($sql, $attrs, 3, PDO::FETCH_ASSOC);
        if ($res) {
            foreach ($res as $v) {
                if ($this->delParent($v['id']) == false) {
                    return false;
                }
            }
            $sqlDelete = 'DELETE FROM `' . $this->tableName . '` WHERE `post_parent` = :id';
            $sqlAttes = array(':id' => array($target, PDO::PARAM_INT));
            return $this->doSQL($sqlDelete, $sqlAttes);
        } else {
            return true;
        }
    }

    /**
     * 执行SQL语句
     * @param string $sql SQL语句
     * @param array $attrs 数据数组 eg:array(':id'=>array('value','PDO::PARAM_INT'),...)
     * @param int $resType 返回类型 0-boolean 1-fetch 2-fetchColumn 3-fetchAll 4-lastID
     * @param int $resFetch PDO-FETCH类型，如果返回fetchColumn则为列偏移值
     * @return boolean|PDOStatement 成功则返回PDOStatement句柄，失败返回false
     */
    private function doSQL($sql, $attrs = null, $resType = 0, $resFetch = null) {
        return $this->db->prepareAttr($sql, $attrs, $resType, $resFetch);
    }

    /**
     * 添加一条日志
     * @param string $message 日志内容
     */
    private function addLog($message) {
        $this->log->add($message);
    }

}

?>
