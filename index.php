<?php

/**
 * 首页
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package page
 */
require('page.php');

$wherePostMeta = '`post_meta` = \'image/jpeg\' or `post_meta` = \'image/png\'';

//产品目录数据
$productListwhere = '`post_status` = \'public\' and `post_type` = \'' . $sysPost->type[7] . '\'';
$productList = $sysPost->getList($productListwhere, null, 1, 9999, 0, false, true);
if ($productList) {
    foreach ($productList as $k => $v) {
        $where = '`post_status` = \'public\' and `post_type` = \'file\' and (' . $wherePostMeta . ') and `post_parent` = :id';
        $attrs = array(':id' => array($v['id'], PDO::PARAM_INT));
        $image = $sysPost->getList($where, $attrs);
        if ($image) {
            $productList[$k]['image'] = $image[0]['id'];
        } else {
            $productList[$k]['image'] = 0;
        }
    }
}

//滚动条列
$cooperationListWhere = '`post_status` = \'public\' and `post_type` = \'' . $sysPost->type[8] . '\'';
$cooperationList = $sysPost->getList($cooperationListWhere, null, 1, 9999, 0, false, true);
if ($cooperationList) {
    foreach ($cooperationList as $k => $v) {
        $where = '`post_status` = \'public\' and `post_type` = \'file\' and (' . $wherePostMeta . ') and `post_parent` = :id';
        $attrs = array(':id' => array($v['id'], PDO::PARAM_INT));
        $image = $sysPost->getList($where, $attrs);
        if ($image) {
            $cooperationList[$k]['image'] = $image[0]['id'];
        } else {
            $cooperationList[$k]['image'] = 0;
        }
    }
}

//注册变量
$smarty->assign('indexProductList', $productList);
$smarty->assign('indexCooperationList', $cooperationList);
$smarty->assign('indexTextM', $sysConfig->load('INDEX_TEXT_M'));

//输出页面
$smarty->display('index.tpl');
?>