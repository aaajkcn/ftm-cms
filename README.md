==FTM-CMS框架体系==
<p>该主体为分支最后一次框架修改更新内容，其配置文件和数据文件可能有大量变动。</p>
<p>© <a href="fotomxq.me" target="_blank">Liuzilu</a><br/></p>
<p>使用Bootstrap、JQuery第三方内容。</p>

==安装说明==
<p>1、需要构建PHP/Mysql服务器运行环境；</p>
<p>2、创建数据库并执行install/install.sql文件；</p>
<p>3、修改config.php配置文件，PDO数据库连接名称部分。</p>