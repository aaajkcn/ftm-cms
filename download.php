<?php

/**
 * 下载文件
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package page
 */
require_once('config.php');

//引入相关包
require(DIR_LIB . DS . 'sys-post.php');
require(DIR_LIB . DS . 'sys-upload.php');
$sysPost = new SysPost($coreDB, $sysLog);
$sysUpload = new SysUpload($sysPost, $sysLog, DIR_UPLOAD, $userRes['id']);
if (isset($_GET['id']) == true) {
    $id = (int) $_GET['id'];
    $passwd = isset($_GET['passwd']) == true ? $_GET['passwd'] : '';
    $sysUpload->downloadFile($id, $passwd, UPLOAD_DOWN_PHP);
}
?>
