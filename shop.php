<?php

/**
 * 购物车
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package page
 */
require('page.php');

//强制不生成缓冲
$smarty->caching = false;

//引入并声明product类
require(DIR_LIB . DS . 'sys-product.php');
$sysProduct = new SysProduct($coreDB, $sysLog);

//引入并声明order类
require(DIR_LIB . DS . 'sys-order.php');
$sysOrder = new SysOrder($coreDB, $sysLog, $coreSession, $sysProduct);

//获取当前购买清单
$buyList = null;
$buyPrice = 0;
$buys = $sysOrder->getBuy();
if ($buys) {
    foreach ($buys as $v) {
        $vRes = $sysProduct->get($v);
        if ($vRes) {
            $buyList[] = $vRes;
            $buyPrice = $buyPrice + $vRes['pt_price'];
        }
    }
}

//订单状态
$messageKey = 0;
if (isset($_GET['msg']) == true) {
    if ($_GET['msg'] == 'order-success') {
        $messageKey = 1;
    } else {
        $messageKey = 2;
    }
}

//注册变量
$smarty->assign('buyList', $buyList);
$smarty->assign('buyPrice', $buyPrice);
$smarty->assign('messageKey', $messageKey);

//输出页面
$smarty->display('shop.tpl');
?>
